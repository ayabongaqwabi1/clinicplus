
module.exports = {
	siteMetadata: {
		title: 'ClinicPlus',
		author: 'Midas Touch Technologies',
		imageUrl: 'https://www.clinicplusbooking.co.za/favicon-32x32.png',
		description: 'Medical examinations for your employees',
		keywords: `Clinic, Bookings, Occupational Health`,
		twitter: '',
		github: ``,
		medium: '',
		gatsby: '',
		bulma: '',
		siteUrl: `https://www.clinicplusbooking.co.za`
	},
	plugins: [
		'gatsby-plugin-react-helmet',
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`
			}
		},
		'gatsby-transformer-sharp',
		'gatsby-plugin-sharp',
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: 'Clinicplus Booking',
				short_name: 'Clinicplus',
				start_url: '/',
				background_color: '#2980b9',
				theme_color: '#2980b9',
				display: 'standalone',
				icon: 'src/images/logo.png',
				orientation: 'portrait'
			}
		},
		`gatsby-plugin-sass`,
		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
				trackingId: 'UA-XXXXXXXX-X',
				// Setting this parameter is optional (requried for some countries such as Germany)
				anonymize: true
			}
		},
		`gatsby-plugin-transition-link`,
		`gatsby-plugin-sitemap`
		// this (optional) plugin enables Progressive Web App + Offline functionality
		// To learn more, visit: https://gatsby.app/offline
		// 'gatsby-plugin-offline',
	]
};
