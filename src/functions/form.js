import React, { Fragment } from "react";
import * as R from "ramda";
import TextField from "@material-ui/core/TextField";
import config from "../config";
import Password from "../components/form/fields/password";
import { navigate } from "gatsby";
import Layout from "../components/layout";
import Banner from "../components/banner";
import CheckboxGroup from "../components/form/fields/checkboxGroup";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import { connect } from "react-redux";
import { answerQuestions, answerQuestion } from "../state/actions/answers";
import fromEntries from "object.fromentries";
import NDA from './nda';

const freeTextValidator = (value) => {
  if (R.isEmpty(value) || R.isNil(value)) {
    return {
      isValid: false,
      errorMsg: "Field cannot be empty",
      value: "",
    };
  }
  return {
    isValid: true,
    errorMsg: undefined,
    value,
  };
};

const emailValidator = (value) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const isValid = re.test(String(value).toLowerCase());
  if (!freeTextValidator(value).isValid) {
    return freeTextValidator(value);
  }
  if (!isValid) {
    return {
      isValid,
      errorMsg: "Email address is not correct format",
      value,
    };
  }
  return {
    isValid,
    errorMsg: undefined,
    value,
  };
};

const validators = {
  "free-text": freeTextValidator,
  email: emailValidator,
};

const componentTypes = {
  "checkbox-group": CheckboxGroup,
  text: TextField,
  password: Password,
  button: (config) => (
    <button className="button is-success is-medium">{config.label}</button>
  ),
};

const getFormComponent = (type) => {
  if (!R.has(type)(componentTypes)) {
    throw new Error(`${type}: component tyype not found`);
  }
  return componentTypes[type];
};

export const getQuestionConfig = (qId) => R.path(["questions", qId], config);

export const getFormQuestions = (formId) => {
  const questionIds = R.path(["forms", formId, "questions"], config);
  return questionIds.map((qId) =>
    R.assoc("questionid", qId, getQuestionConfig(qId))
  );
};

export const getFormById = (formId) => {
  return R.path(["forms", formId], config);
};

export const getFormButton = (formId) =>
  R.path(["forms", formId, "button"], config);

export const createFormElement = (config, answerQuestion, answer) => {
  const { type, questionid } = config;
  const onChange = (e) => {
    const value = e.target.value;
    answerQuestion(questionid, value);
  };
  const Component = getFormComponent(type);
  const { isValid, errorMsg, value } = answer;
  const error = isValid === false;
  return (
    <div className="component-margin" key={questionid}>
      <Component
        error={error}
        helperText={errorMsg}
        onChange={onChange}
        value={value}
        {...config}
      />
    </div>
  );
};

export const buildElement = (config, onChange) => {
  const { type } = config;
  const Component = getFormComponent(type);
  return (
    <Component className="component-margin" onChange={onChange} {...config} />
  );
};

const generateSplashButton = (
  goToNextPage,
  next,
  disabled,
  splash,
  buttonConfig
) => {
  const { label } = buttonConfig;
  return (
    <Fragment>
      {!disabled && (
        <AniLink
          cover
          direction=""
          duration={3}
          className="btn btn-custom w-lg btn-red"
          bg={`
            url(${splash})
            center / cover
            no-repeat
            fixed
            padding-box
            content-box
            #d4d8dd
          `}
          onClick={goToNextPage}
          to={next}
        >
          {label}
        </AniLink>
      )}
      {disabled && (
        <button
          onClick={goToNextPage}
          disabled
          className="btn btn-custom w-lg btn-red disabled-btn"
        >
          <span>{label}</span>
        </button>
      )}
    </Fragment>
  );
};

const generateNextbutton = (runFormActions, next, disabled, buttonConfig) => {
  return (
    <button
      onClick={(e) => {
        e.preventDefault();
        runFormActions();
        navigate(next);
      }}
      className="btn btn-custom w-lg btn-red"
      disabled={disabled}
    >
      <span>{buttonConfig.label}</span>
    </button>
  );
};

export class FormConstructor extends React.Component {
  constructor() {
    super();
    this.answerQuestion = this.answerQuestion.bind(this);
    this.state = {};
  }

  componentWillMount() {
    const { formId, state } = this.props;
    const defaultValidation = {
      isValid: undefined,
      errorMsg: undefined,
      value: "",
    };
    const getAppStateValidation = (qId) => {
      const answer = state.answers[qId];
      return R.isNil(answer) || R.isEmpty(answer) ? defaultValidation : answer;
    };
    const formQuestions = getFormQuestions(formId);
    const initialState = R.mergeAll(
      formQuestions.map((q) =>
        fromEntries([
          [q.questionid, getAppStateValidation(q.questionid)],
        ])
      )
    );
    this.setState(initialState);
  }

  answerQuestion(questionId, value) {
    const validatorId = config.questions[questionId].validator;
    const validate = validators[validatorId];
    const defaultValidation = {
      isValid: true,
      errorMsg: undefined,
      value,
    };
    const validation = R.is(Function, validate)
      ? validate(value)
      : defaultValidation;
    const updatedState = R.assoc(questionId, validation, this.state);
    this.setState(updatedState);
  }

  render() {
    const { formId } = this.props;
    const formQuestions = getFormQuestions(formId);
    const formElements = formQuestions.map((q) => {
      const { questionid } = q;
      const answer = this.state[questionid];
      return createFormElement(q, this.answerQuestion, answer);
    });
    const formButton = getFormButton(formId);
    const formConfig = getFormById(formId);
    const { splash } = formButton;
    const next = this.props.nextPage  ? this.props.nextPage : formConfig.next;
    const withSpash = formButton
      ? !R.isNil(formButton.splash) && !R.isEmpty(formButton.splash)
      : false;
    const equalsTrue = R.equals(true);
    const validations = R.values(this.state).map((a) => {
      if (R.isNil(a.isValid)) {
        return false;
      }
      return a.isValid;
    });
    const disabled = !R.all(equalsTrue)(validations);
    const goToNextPage = () => this.props.answerQuestions(this.state);
    return (
      <div className={`form-container-${formId}`} style={{ overflowY:"auto"}}>
        <form noValidate autoComplete="off">
          {formElements}
          <br />
        </form>
        {formId === "prebooking" && 
          <div style={{display:'flex', justifyContent:'center', marginBottom:"15px"}}>
            <NDA answerNDA={this.props.answerNDA}/>
            <br />
          </div>}
        
        <div className="form-button-container">
          {formButton &&
              withSpash &&
              generateSplashButton(
                goToNextPage,
                next,
                disabled,
                splash,
                formButton
              )}
            {formButton &&
              !withSpash &&
              generateNextbutton(goToNextPage, next, disabled, formButton)}
        </div>
      </div >
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  answerQuestions: (answers) => dispatch(answerQuestions(answers)),
  answerNDA: (answer) => dispatch(answerQuestion('ndaUrl', answer)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) =>
  Object.assign({}, stateProps, dispatchProps, ownProps);

export const Form = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(FormConstructor);

export const generateFormPage = (formId, noDeets) => {
  const formConfig = getFormById(formId);
  const { title, description } = formConfig;
  return (
    <Layout>
      <div className="container center">
        <div className="user-option-box">
          <Banner title={title} subtitle={description} />
          <div className={`${noDeets ? '' :'company-deets-form'} ${formId}-gen-form`}>
            <Form formId={formId} />
          </div>
        </div>
      </div>
    </Layout>
  );
};
