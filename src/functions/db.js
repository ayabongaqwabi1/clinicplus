import axios from "axios";
import { GET_SERVER } from "../config/server.js";
import jwt from "json-web-token";
import { GATSBY_ACCESS_TOKEN_SECRET } from "../config/access";

export const saveAppointmentEditToDb = (appointment) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    appointment
  );
  return axios.post(`${GET_SERVER()}/api/appointment/edit`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const addAppointmentToDb = (appointment) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    appointment
  );
  return axios.post(`${GET_SERVER()}/api/booking`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const addAppointmentToUser = (appointment) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    appointment
  );
  return axios.post(`${GET_SERVER()}/api/existing/booking`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const addCustomerToDb = (customer) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    customer
  );
  return axios.post(`${GET_SERVER()}/api/customer/add`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const saveCustomerEditToDb = (customer) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    customer
  );
  return axios.post(`${GET_SERVER()}/api/customer/edit`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const saveAccountEditToDb = (account) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    account
  );
  return axios.post(`${GET_SERVER()}/api/account/edit`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const saveUserEditToDb = (user) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    user
  );
  return axios.post(`${GET_SERVER()}/api/auth/user/edit`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const addUserToDb = (user) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    user
  );
  return axios.post(`${GET_SERVER()}/api/auth/user/add`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const refreshDbData = (user) => {
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    user
  );
  return axios.post(`${GET_SERVER()}/api/auth/refresh`, data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};
