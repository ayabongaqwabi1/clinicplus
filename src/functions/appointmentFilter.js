import moment from "moment";

export const getTodaysAppointments = (appointments) =>
  appointments.filter((ap) => {
    const todayDate = new moment(new Date());
    const appointmentDate = new moment(ap.date, "DD/MM/YYYY");
    return (
      todayDate.diff(appointmentDate, "hours") > 0 &&
      todayDate.diff(appointmentDate, "hours") < 23
    );
  });

export const getTomorrowsAppointments = (appointments) =>
  appointments.filter((ap) => {
    const todayDate = new moment(new Date());
    const appointmentDate = new moment(ap.date, "DD/MM/YYYY");
    return (
      todayDate.diff(appointmentDate, "hours") < 0 &&
      todayDate.diff(appointmentDate, "hours") > -23
    );
  });

export const getAppointmentsOnThisWeek = (appointments) =>
  appointments.filter((ap) => {
    const appointmentDate = new moment(ap.date, "DD/MM/YYYY");
    const start = moment()
      .day(1)
      .startOf("day");
    const end = moment()
      .day(5)
      .startOf("day");
    const isInWeek =
      appointmentDate.diff(end, "days") >= -4 &&
      appointmentDate.diff(end, "days") <= 0 &&
      appointmentDate.diff(start, "days") >= 0 &&
      appointmentDate.diff(start, "days") <= 4;
    return isInWeek;
  });

export const getAppointmentsOnLastWeek = (appointments) =>
  appointments.filter((ap) => {
    const todayDate = new moment(new Date());
    const appointmentDate = new moment(ap.date, "DD/MM/YYYY");
    return appointmentDate.week() === todayDate.week() - 1;
  });

export const getAppointmentsOnThisMonth = (appointments) =>
  appointments.filter((ap) => {
    const todayDate = new moment(new Date());
    const appointmentDate = new moment(ap.date, "DD/MM/YYYY");
    return appointmentDate.month() === todayDate.month();
  });

export const getAppointmentsOnThisYear = (appointments) =>
  appointments.filter((ap) => {
    const todayDate = new moment(new Date());
    const appointmentDate = new moment(ap.date, "DD/MM/YYYY");
    return appointmentDate.year() === todayDate.year();
  });
