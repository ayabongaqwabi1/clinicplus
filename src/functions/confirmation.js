import { keys, assoc, isNil, values, mergeAll, append, pipe } from "ramda";
import axios from "axios";
import { GET_SERVER } from "../config/server.js";
import jwt from "json-web-token";
import { GATSBY_ACCESS_TOKEN_SECRET } from "../config/access";

const isValidateable = (item) => !isNil(item.value);

const extractAnswers = (rawAnswers) => {
  const answerKeys = keys(rawAnswers);
  const answers = answerKeys.map((key) => {
    if (isValidateable(rawAnswers[key])) {
      return assoc(key, rawAnswers[key].value, {});
    } else {
      return {};
    }
  });
  const services = { services: values(rawAnswers.services) };
  const employees = values(rawAnswers.employees).map((employeeObject) => ({
    name: employeeObject.questions.employeeInitialsAndSurname.value,
    id: employeeObject.questions.employeeIdNumber.value,
    occupation: employeeObject.questions.employeeOccupation.value,
    jobfileUrl: employeeObject.jobfileUrl,
    medicalServices: employeeObject.employeeMedicalService.selected,
  }));
  const extendedAnswers = pipe(
    append(services),
    append({ employees }),
    append({ ndaUrl: rawAnswers.ndaUrl }),
    mergeAll
  )(answers);
  
  return extendedAnswers;
};

export default (answers) => {
  const cleanAnswers = extractAnswers(answers);
  const data = jwt.encode(
    GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
    cleanAnswers
  );
  axios
    .post(`${GET_SERVER()}/api/booking`, data, {
      headers: {
        "Content-Type": "application/json",
      },
    })
};
