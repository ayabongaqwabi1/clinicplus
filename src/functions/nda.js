import React, { Component } from 'react';
import loading from '../images/loading2.gif';
import axios from 'axios';
import PageButton from '../components/buttons/pageButton';

class NDA extends Component{
    constructor(){
        super();
        this.state = { selectedFile:'', isUploading: '' }
        this.onFileChange = this.onFileChange.bind(this);
        this.onFileUpload = this.onFileUpload.bind(this);
      }
    
      onFileChange = event => { 
        this.setState({ selectedFile: event.target.files[0] }); 
      }; 
    
      onFileUpload = (qId) => { 
        return ()=>{ 
          this.setState({isUploading: true})
          const url = "https://api.cloudinary.com/v1_1/clinic-plus/raw/upload";
          const formData = new FormData(); 
          formData.append( 
            "file", 
            this.state.selectedFile, 
            this.state.selectedFile.name 
          ); 
          formData.append("upload_preset", "pwdsm6sz");
          axios({
            method: "POST",
            data: formData,
            headers: {'Content-Type': 'multipart/form-data' },
            url
          })
            .then((response) => {
              this.props.answerNDA(response.data.url)
              this.setState({isUploading: false})
            })
            .then((data) => {
            });
        }
      }; 

    render(){
        const { isUploading } = this.state;

        return (
            <div style={{width: "300px"}}>
              <h3>Non Disclosure Agreement</h3>
              <p>Please download, fill and upload NDA</p>
              <a href="https://res.cloudinary.com/clinic-plus/raw/upload/v1603283513/Non-disclosure_agreement_bdkxi8.docx"
                 target="_blank"
                 download="Non disclosure agreement"
              >Download</a>
              <br />
              <p>Upload NDA</p>
              <input type="file" onChange={this.onFileChange} style={{marginBottom:"15px"}}/> 
              <br />
              <PageButton onClick={this.onFileUpload()}> 
                  Upload
              </PageButton>
              <br />
              {isUploading === true && <div style={{marginTop: "15px"}} className=""><img style={{width: "40px", marginTop:"-8px"}} src={loading} alt="loading"/></div>}
              {isUploading === false && <div style={{marginTop: "15px"}} className="">Done Uploading!</div>}
            </div>
        )
    }
}

export default NDA