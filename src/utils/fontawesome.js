import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowCircleRight, faTrash, faArrowCircleLeft, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
library.add(faArrowCircleRight)
library.add(faArrowCircleLeft)
library.add(faPencilAlt)
library.add(faTrash)