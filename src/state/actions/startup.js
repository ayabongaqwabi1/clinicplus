import * as types from "./types";
import * as R from "ramda";
import cookieBakery from "js-cookie";
const queryString = require("query-string");
var short = require("short-uuid");

// Quick start with flickrBase58 format

const newId = () => short.generate();

export const ENV = process.env.NODE_ENV || "development";
export const DOMAIN = "clinic-plus.co.za";
export const IS_DEV = ENV !== "production" && ENV !== "qa";

export const checkIfCookieExists = (cookieKey) =>
  !R.isNil(cookieBakery.get(cookieKey)) &&
  !R.isEmpty(cookieBakery.get(cookieKey));

export const getCookie = (cookieKey) => {
  const cookie = cookieBakery.get(cookieKey);
  return cookie;
};

export const setCookie = (key, value, expires) => {
  const domain = IS_DEV ? "localhost" : DOMAIN;
  cookieBakery.set(key, value, { expires, domain });
};

// export default props => queryString.parse(props.location.search);

export const extractQueryParams = () =>
  queryString.parse(window.location.search);

const INTERACTION_COOKIE = "interactionId";
const INTERACTION_COOKIE_LIFETIME_IN_DAYS = 30;

export const setInteractionId = (interactionId) => {
  const type = types.SET_INTERACTION_ID;
  const payload = interactionId;
  return {
    type,
    payload,
  };
};

export const dispatchNewInteractionId = (dispatch, interactionId) => {
  setCookie(
    INTERACTION_COOKIE,
    interactionId,
    INTERACTION_COOKIE_LIFETIME_IN_DAYS
  );
  dispatch(setInteractionId(interactionId));
};

const handlBadInteractionId = (interactionId) => {
  if (!interactionId) {
    return null;
  }
  if (R.contains("SIMPLY", R.toUpper(interactionId))) {
    return null;
  }
  return interactionId;
};

const startUpActions = (dispatch, state) => {
  // const queryParams = extractQueryParams();
  const cookieInteractionId = handlBadInteractionId(
    getCookie(INTERACTION_COOKIE)
  );
  const newIID = newId();

  const interactionId = cookieInteractionId || state["interactionId"] || newIID;
  //queryParams.interactionId ||

  dispatchNewInteractionId(dispatch, interactionId);
};

export const startup = (dispatch, state) => {
  const type = types.STARTUP;
  const payload = startUpActions(dispatch, state);
  return {
    type,
    payload,
  };
};

const dispatchResetActions = (dispatch) => {
  dispatchNewInteractionId(dispatch, newId());
};

export const reset = () => {
  const type = types.RESET;
  return {
    type,
    payload: dispatchResetActions,
  };
};

const getLocations = (state) => {
  const appointments = state.dashboard.appointments;
  return R.uniq(appointments.map((app) => app.location));
};

export const saveStaff = (staff) => ({
  type: types.SAVE_STAFF,
  payload: staff,
});

export const saveCustomers = (customers) => ({
  type: types.SAVE_CUSTOMERS,
  payload: customers,
});

export const saveAccounts = (accounts) => ({
  type: types.SAVE_ACCOUNTS,
  payload: accounts,
});

const saveLocations = (customers) => ({
  type: types.SAVE_LOCATIONS,
  payload: customers,
});

export const dashboardStartup = (state, dispatch) => {
  const locations = getLocations(state);
  dispatch(saveLocations(locations));
};
