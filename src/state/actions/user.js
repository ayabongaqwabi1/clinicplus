import { ADD_USER, EDIT_USER, DELETE_USER } from "./types";

export const addUser = (ap) => ({
  type: ADD_USER,
  payload: ap,
});

export const editUser = (ap) => ({
  type: EDIT_USER,
  payload: ap,
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  payload: { id },
});
