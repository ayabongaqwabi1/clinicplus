import * as types from "./types";

export default () => ({
  type: types.RESET,
  payload: "",
});
