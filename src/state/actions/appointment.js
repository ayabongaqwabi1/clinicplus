import {
  ADD_APPOINTMENT,
  EDIT_APPOINTMENT,
  DELETE_APPOINTMENT,
  SAVE_APPOINTMENT,
  HYDRATE_APPOINTMENTS,
} from "./types";

export const addAppointment = (ap) => ({
  type: ADD_APPOINTMENT,
  payload: ap,
});

export const editAppointment = (ap) => ({
  type: EDIT_APPOINTMENT,
  payload: ap,
});

export const saveAppointment = (ap) => ({
  type: SAVE_APPOINTMENT,
  payload: ap,
});

export const hydrateAppointments = (aps) => ({
  type: HYDRATE_APPOINTMENTS,
  payload: aps,
});

export const deleteAppointment = (ap) => ({
  type: DELETE_APPOINTMENT,
  payload: ap,
});
