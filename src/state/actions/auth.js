
export const setUserType  = (userType) => ({
    type: "SET_USER_TYPE",
    payload: userType
})

export const saveUser  = (user) => ({
    type: "SET_USER",
    payload: user
})