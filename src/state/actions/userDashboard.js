
export const saveAnswersToUserDashboard  = (answers) => ({
    type: "SAVE_USER_ANSWERS",
    payload: answers
})

export const resetAnswerState  = (answers) => ({
    type: "RESET_ANSWERS_STATE",
    payload: answers
})