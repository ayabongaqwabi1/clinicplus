import * as types from "./types";
import * as R from "ramda";

export const answerQuestionError = (questionId) => {
  const type = types.ANSWER_QUESTION_ERROR;
  const payload = questionId;
  return {
    type,
    payload,
  };
};

export const answerQuestion = R.curry((questionId, answer) => {
  const type = types.ANSWER_QUESTION; //postAnswer(route, meta);
  const meta = { questionId, answer };
  return {
    type,
    payload: meta,
    meta,
  };
});

export const answerQuestions = (answers) => {
  const type = types.ANSWER_QUESTIONS;
  return {
    type,
    payload: answers,
  };
};

export const answerForm = R.curry((formId, questionId, answer) =>
  answerQuestion(questionId, answer)
);
