import { createStore as reduxCreateStore, applyMiddleware } from "redux";

import logger from 'redux-logger'

import rootReducer from './reducers'

const initialState = new Map( )

const createStore = () => reduxCreateStore(rootReducer, initialState, applyMiddleware(logger),)

export default createStore