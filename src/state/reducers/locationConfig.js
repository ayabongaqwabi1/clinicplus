import { append } from 'ramda';
import { ADD_LOCATION } from '../actions/types';
import createActionHandler from './createActionHandler';

const initialState = [
    { 
        name: "Hendrina",
        street: "53 Beukes street",
        color: 'red'
    }, 
    { 
        name: "Nicol Street",
        street: "3 Nicol Street, Witbank",
        color:"yellow"
    }, 
    { 
        name: "Churchill Avenue",
        street: "2 Churchill Ave, Witbank",
        color:"yellow"
    }]

const actionHandlers = {};

const addLocation = (state, action) => append(action.payload, state);

actionHandlers[ADD_LOCATION] = addLocation;

const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);


