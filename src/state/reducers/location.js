import { append } from "ramda";
import { ADD_LOCATION, RESET } from "../actions/types";
import createActionHandler from "./createActionHandler";

const initialState = ["Hendrina", "Nicol Street", "Churchill Avenue"];

const actionHandlers = {};

const addLocation = (state, action) => append(action.payload, state);

const toInitialState = () => initialState;

actionHandlers[RESET] = toInitialState;
actionHandlers[ADD_LOCATION] = addLocation;

const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);
