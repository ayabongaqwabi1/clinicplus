import * as R from "ramda";
import { ANSWER_QUESTION, ANSWER_QUESTIONS } from "../actions/types";
import createActionHandler from "./createActionHandler";

const initialState = {};

const actionHandlers = {};

const toAnswerQuestionState = (state, action) => {
  const { answer, questionId } = action.payload;
  return R.assoc(questionId, answer, state);
};

const toAnswerQuestionsState = (state, action) =>
  R.mergeAll([state, action.payload]);

const toInitialState = () => initialState;

actionHandlers["RESET_ANSWERS_STATE"] = toInitialState;
actionHandlers[ANSWER_QUESTION] = toAnswerQuestionState;
actionHandlers[ANSWER_QUESTIONS] = toAnswerQuestionsState;
const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);
