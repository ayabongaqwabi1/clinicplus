import * as R from "ramda";
import { RESET } from "../actions/types";
import createActionHandler from "./createActionHandler";

const initialState = {
  appointments: [],
  duration: 4,
};

const actionHandlers = {};

const saveUserAnswersAsAppointment = (state, action) => {
  const answers = action.payload;
  let currentAppointments = state.appointments;
  currentAppointments.push(answers);
  const appointmentState = { appointments: currentAppointments };
  return R.mergeAll([state, appointmentState]);
};

const toInitialState = () => initialState;

actionHandlers[RESET] = toInitialState;
actionHandlers["SAVE_USER_ANSWERS"] = saveUserAnswersAsAppointment;
const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);
