import interactionId from './interaction';
import answers from './answers';
import dashboard from './dashboard';
import userDashboard from './userDashboard';
import auth from './auth';
import location from './location';
import locationConfig from './locationConfig';

import { combineReducers } from 'redux'

const rootReducer =
  combineReducers({
    interactionId,
    answers,
    dashboard,
    userDashboard,
    location,
    locationConfig,
    auth,
  })

export default rootReducer;
