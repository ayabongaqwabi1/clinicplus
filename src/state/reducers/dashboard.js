import * as R from "ramda";
import {
  ANSWER_QUESTION,
  ANSWER_QUESTIONS,
  RESET,
  EDIT_APPOINTMENT,
  DELETE_APPOINTMENT,
  SAVE_APPOINTMENT,
  SAVE_CUSTOMERS,
  EDIT_CUSTOMER,
  DELETE_CUSTOMER,
  SAVE_LOCATIONS,
  HYDRATE_APPOINTMENTS,
  ADD_APPOINTMENT,
  ADD_CUSTOMER,
  SAVE_STAFF,
  ADD_USER,
  SAVE_ACCOUNTS,
} from "../actions/types";
import createActionHandler from "./createActionHandler";
import moment from "moment";

const initialState = {
  customers: [],
  locations: [],
  appointments: [],
  staff: [],
  accounts: [],
};

const actionHandlers = {};

const toAnswerQuestionState = (state, action) => {
  const { answer, questionId } = action.payload;
  return R.assoc(questionId, answer, state);
};

const toAnswerQuestionsState = (state, action) =>
  R.mergeAll([state, action.payload]);

const saveAppointment = (state, action) => {
  const appointment = action.payload;
  const { id } = appointment;
  let appointments = state.appointments.filter((ap) => ap.id !== id);
  const editIndex = R.findIndex(R.propEq("id", id))(state.appointments);
  appointments = R.insert(editIndex, appointment, appointments);
  return R.assoc("appointments", appointments, state);
};

const addAppointment = (state, action) => {
  const appointment = action.payload;
  const appointments = R.append(appointment, state.appointments);
  return R.assoc("appointments", appointments, state);
};

const addCustomer = (state, action) => {
  const customer = action.payload;
  const customers = R.append(customer, state.customers);
  return R.assoc("customers", customers, state);
};

const addUser = (state, action) => {
  const user = action.payload;
  const users = R.append(user, state.staff);
  return R.assoc("staff", users, state);
};

const saveCustomer = (state, action) => {
  const customer = action.payload;
  const { id } = customer;
  let customers = state.customers.filter((c) => c.id !== id);
  const editIndex = R.findIndex(R.propEq("id", id))(state.customers);
  customers = R.insert(editIndex, customer, customers);
  return R.assoc("customers", customers, state);
};

const deleteAppointment = (state, action) => {
  const appointment = action.payload;
  const { id } = appointment;
  let appointments = state.appointments.filter((ap) => ap.id !== id);
  return R.assoc("appointments", appointments, state);
};

const deleteCustomer = (state, action) => {
  const customer = action.payload;
  const { id } = customer;
  let customers = state.customers.filter((ap) => ap.id !== id);
  return R.assoc("customers", customers, state);
};

const saveCustomers = (state, action) => {
  return R.assoc("customers", action.payload, state);
};

const saveAccounts = (state, action) => {
  return R.assoc("accounts", action.payload, state);
};

const saveStaff = (state, action) => {
  return R.assoc("staff", action.payload, state);
};

const saveLocations = (state, action) => {
  return R.assoc("locations", action.payload, state);
};

const saveAllAppointments = (state, action) => {
  const simpleAppointments = action.payload.map((ap) => {
    const { date } = ap;
    return moment(new Date(date)).isValid()
      ? R.pipe(R.assoc("date", moment(new Date(date)).format("DD/MM/YYYY")))(ap)
      : R.pipe(R.assoc("date", date))(ap);
  });
  return R.assoc("appointments", simpleAppointments, state);
};

const toInitialState = () => initialState;

actionHandlers[RESET] = toInitialState;
actionHandlers[ANSWER_QUESTION] = toAnswerQuestionState;
actionHandlers[ANSWER_QUESTIONS] = toAnswerQuestionsState;
actionHandlers[ADD_APPOINTMENT] = addAppointment;
actionHandlers[EDIT_APPOINTMENT] = saveAppointment;
actionHandlers[SAVE_APPOINTMENT] = saveAppointment;
actionHandlers[EDIT_CUSTOMER] = saveCustomer;
actionHandlers[ADD_CUSTOMER] = addCustomer;
actionHandlers[DELETE_APPOINTMENT] = deleteAppointment;
actionHandlers[HYDRATE_APPOINTMENTS] = saveAllAppointments;
actionHandlers[DELETE_CUSTOMER] = deleteCustomer;
actionHandlers[ADD_USER] = addUser;
actionHandlers[SAVE_CUSTOMERS] = saveCustomers;
actionHandlers[SAVE_ACCOUNTS] = saveAccounts;
actionHandlers[SAVE_STAFF] = saveStaff;
actionHandlers[SAVE_LOCATIONS] = saveLocations;
const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);
