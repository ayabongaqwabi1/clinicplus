import { RESET } from "../actions/types";
import createActionHandler from "./createActionHandler";

const initialState = { type: "" };

const actionHandlers = {};

const setUserType = (state, action) => ({ type: action.payload });
const setUser = (state, action) => action.payload;

const toInitialState = () => initialState;

actionHandlers[RESET] = toInitialState;

actionHandlers["SET_USER_TYPE"] = setUserType;
actionHandlers["SET_USER"] = setUser;

const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);
