import { SET_INTERACTION_ID } from '../actions/types';
import createActionHandler from './createActionHandler';

const initialState = "";

const actionHandlers = {};

const setInteractionId= (state, action) => action.payload;

actionHandlers[SET_INTERACTION_ID] = setInteractionId;

const handle = createActionHandler(actionHandlers);

export default (state = initialState, action) => handle(state, action);


