import React from "react";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import Layout from "../components/layout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as R from "ramda";
import { v1 as uuidv1 } from "uuid";
import Banner from "../components/banner";
import { getFormQuestions, buildElement } from "../functions/form";
import PageButton from '../components/buttons/pageButton';
import short from "short-uuid";
import { isBrowser } from "react-device-detect";
import { connect } from "react-redux";
import { answerQuestion } from "../state/actions/answers";
import fromEntries from "object.fromentries";
import Helmet from 'react-helmet';
import axios from 'axios';
import loading from '../images/loading2.gif';


const generateFormFromQuestions = (questions, answerQuestion) => (
  <React.Fragment>
    {R.keys(questions).map((key) => {
      const questionConfig = questions[key];
      const { questionid } = questionConfig;
      const element = buildElement(questionConfig, answerQuestion(questionid));
      return element;
    })}
  </React.Fragment>
);

const generateEmployeeCard = (title, openRow, onDelete) => {
  return (
    <p className="unselected">
      {title}
      <div>
        <span className="trash edit" onClick={openRow}>
          <span class="icon is-small">
            <FontAwesomeIcon icon={"pencil-alt"} />
          </span>
        </span>
        <span className="trash" onClick={onDelete}>
          <span class="icon is-small">
            <FontAwesomeIcon icon={"trash"} />
          </span>
        </span>
      </div>
    </p>
  );
};

class EmployeeRow extends React.Component{
  constructor(){
    super();
    this.state = { selectedFile:'', isUploading: '' }
    this.onFileChange = this.onFileChange.bind(this);
    this.onFileUpload = this.onFileUpload.bind(this);
  }

  onFileChange = event => { 
    this.setState({ selectedFile: event.target.files[0] }); 
  }; 

  onFileUpload = (qId) => { 
    return ()=>{ 
      this.setState({isUploading: true})
      const url = "https://api.cloudinary.com/v1_1/clinic-plus/raw/upload";
      const formData = new FormData(); 
      formData.append( 
        "file", 
        this.state.selectedFile, 
        this.state.selectedFile.name 
      ); 
      formData.append("upload_preset", "pwdsm6sz");
      axios({
        method: "POST",
        data: formData,
        headers: {'Content-Type': 'multipart/form-data' },
        url
      })
        .then((response) => {
          this.props.answerQuestion(qId, response.data.url)
          this.setState({isUploading: false})
        })
        .then((data) => {
        });
    }
  }; 

  render(){
    const {
      rowId,
      onDelete,
      openRow,
      closeRow,
      isOpen,
      questions,
      answerQuestion,
      employeeMedicalService,
      title,
    } = this.props;
    const MedicalElement = buildElement(employeeMedicalService, answerQuestion(employeeMedicalService.questionid));
    const activeClass = isOpen ? "is-active" : ""
    const { isUploading } = this.state;
    const EmployeeForm = (
      <div class={`modal ${activeClass}`}>
      <div class="modal-background"></div>
      <div class="modal-card">
          <header class="modal-card-head">
            <p class="modal-card-title">Edit Employee</p>
            <button class="delete" aria-label="close" onClick={()=>closeRow(rowId)}></button>
          </header>
          <section class="modal-card-body">
            <div className="employee-form">
              <div className="questions">
                {generateFormFromQuestions(questions, answerQuestion)}
              </div>
              <div className="medical-service">
                {MedicalElement}
              </div>
              {/* <span className="trash" onClick={onDelete}>
                <span class="icon is-small">
                  <FontAwesomeIcon icon={"trash"} />
                </span>
              </span>  */}
              <h3>Man Job spec</h3>
              <p>Please download, fill and upload employee  Man Job spec file</p>
              <a href="https://res.cloudinary.com/clinic-plus/raw/upload/v1603288115/Man_Job_Spec_ClinicPlus_ysaoiz.xlsx"
                 target="_blank"
                 download="Non disclosure agreement"
              >Download</a>
              <br />
              <p>Upload file</p>
              <input type="file" onChange={this.onFileChange} /> 
              <br />
              <PageButton onClick={this.onFileUpload("jobfileUrl")}> 
                  Upload! 
              </PageButton>
              <br />
              {isUploading === true && <div className="login-loading">Uploading <img style={{width: "40px", marginTop:"-8px"}}src={loading} alt="loading"/></div>}
              {isUploading === false && <div className="login-loading">Done Uploading!</div>}
            </div>
          </section>
          <footer class="modal-card-foot">
            <button className="btn btn-custom" onClick={()=>closeRow(rowId)}>Done</button>
          </footer>
        </div>
      </div>
  
      
    );
    const _answer = questions.employeeInitialsAndSurname.value
    const employeeTitle = R.isEmpty(_answer) || R.isNil(_answer) ? title : _answer;
    const EmployeeCard = generateEmployeeCard(
      employeeTitle,
      () => openRow(rowId),
      onDelete
    );
    const classes = isOpen
      ? `row open-employee-block is-not-collapsed`
      : `row open-employee-block is-collapsed`;
    return (
      <div className={classes}>
        {isOpen === true && EmployeeForm}
        {isOpen === false && EmployeeCard}
      </div>
    );
  }

};
class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      employeeRows: {},
    };
    this.answerQuestion = this.answerQuestion.bind(this);
    this.addServiceRow = this.addServiceRow.bind(this);
    this.deleteServiceRow = this.deleteServiceRow.bind(this);
    this.openRow = this.openRow.bind(this);
    this.closeRow = this.closeRow.bind(this);
  }

  componentWillMount() {
    const questions = getFormQuestions("employee");
    const keyedQuestions = R.mergeAll(
      questions.map((q) => R.assoc(q.questionid, q, {}))
    );
    const employeeMedicalService = keyedQuestions.employeeMedicalService;
    const employeeRows = R.assoc(
      short.generate(),
      { title: "Click to edit 1", questions: R.omit(["employeeMedicalService"], keyedQuestions), isOpen: false, employeeMedicalService },
      {}
    );
    this.setState({ employeeRows });
  }

  answerQuestion(rowId, questionId, event) {
    const { employeeRows } = this.state;
    if (!event.target){
      const updatedRows = R.assocPath(
        [rowId, questionId],
        event,
        employeeRows
      );
      this.setState({ employeeRows: updatedRows });
      this.props.answerQuestion("employees", updatedRows);
    }
    else{
      const answer = event.target.value;
      const updatedRows = R.assocPath(
        [rowId, "questions", questionId, "value"],
        answer,
        employeeRows
      );

      this.setState({ employeeRows: updatedRows });
      this.props.answerQuestion("employees", updatedRows);
    }
  }

  openRow(rowId) {
    const { employeeRows } = this.state;
    const rowKeys = R.keys(employeeRows);
    const updatedRows = rowKeys.map((key) => {
      const row = employeeRows[key];
      if (rowId !== key) {
        const closedRow = R.assoc("isOpen", false, row);
        const entries = new Map([[key, closedRow]]);
        return fromEntries(entries);
      }
      const openedRow = R.assoc("isOpen", true, row);
      const entries = new Map([[key, openedRow]]);
      return fromEntries(entries);
    });

    this.setState({ employeeRows: R.mergeAll(updatedRows) });
  }

  closeRow(rowId) {
    const { employeeRows } = this.state;
    const rowKeys = R.keys(employeeRows);
    const updatedRows = rowKeys.map((key) => {
      const row = employeeRows[key];
      if (rowId !== key) {
        const closedRow = R.assoc("isOpen", false, row);
        const entries = new Map([[key, closedRow]]);
        return fromEntries(entries);
      }
      const closedRow = R.assoc("isOpen", false, row);
      const entries = new Map([[key, closedRow]]);
      return fromEntries(entries);
    });

    this.setState({ employeeRows: R.mergeAll(updatedRows) });
  }

  addServiceRow() {
    const { employeeRows } = this.state;
    const questions = getFormQuestions("employee");
    const keyedQuestions = R.mergeAll(
      questions.map((q) => R.assoc(q.questionid, q, {}))
    );
    const employeeMedicalService = keyedQuestions.employeeMedicalService;
    const closedRows = R.pipe(
      R.keys,
      (keys) => {
        return keys.map((key) => {
          const row = employeeRows[key];
          const rowWithOpenFlag = R.assoc("isOpen", false, row);
          const entries = new Map([[key, rowWithOpenFlag]]);
          return fromEntries(entries);
        });
      },
      (rows) => {
        return rows.map((r) => {
          const rowKey = R.head(R.keys(r));
          const name = r[rowKey].questions["employeeInitialsAndSurname"].value;
          const index = rows.indexOf(r) + 1;
          const title =
            !R.isNil(name) && !R.isEmpty(name) ? name : `Click to edit ${index}`;
          return R.assocPath([rowKey, "title"], title, r);
        });
      },
      R.mergeAll
    )(employeeRows);
    const updatedRows = R.assoc(
      uuidv1(),
      {
        title: `Click to edit ${R.keys(closedRows).length + 1}`,
        questions: R.omit(["employeeMedicalService"], keyedQuestions),
        isOpen: false,
        employeeMedicalService,
      },
      closedRows
    );

    this.setState({ employeeRows: updatedRows });
  }

  deleteServiceRow(rowId) {
    const { employeeRows } = this.state;
    const updatedRows = R.dissoc(rowId, employeeRows);
    this.setState({ employeeRows: updatedRows });
  }

  render() {
    const { employeeRows } = this.state;
    const curriedAnswerQuestion = R.curry((rowId, qId, value) =>
      this.answerQuestion(rowId, qId, value)
    );
    return (
      <Layout showProgress={false} step={1}>
        <Helmet>
          <script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"/>  
        </Helmet>
        <div className="container center">
          <div className="user-option-box">
            <Banner
              title="Employees"
              subtitle="Add details aboput your employee medicals"
            />
            <div className="rows employees-table">
              <div className="row employee-rows">
                <div className="container text-is-centered">
                  <div className="rows">
                    {R.keys(employeeRows).map((rowId) => (
                      <EmployeeRow
                        {...R.prop(rowId, employeeRows)}
                        answerQuestion={curriedAnswerQuestion(rowId)}
                        onDelete={() => this.deleteServiceRow(rowId)}
                        rowId={rowId}
                        openRow={this.openRow}
                        closeRow={this.closeRow}
                        state={this.state}
                      />
                    ))}
                  </div>
                </div>
                
              </div>
              <br />
              <br />
              <div class="row">
                <div className="container text-is-centered">
                  <br />
                  <div className="buttons">
                  <button
                  className="btn btn-custom"
                  onClick={this.addServiceRow}
                >
                  Add Employee
                </button>
                    <AniLink fade duration={0.8} to={"/prebooking"}>
                      <button className="btn btn-custom w-lg">
                        Back
                      </button>
                    </AniLink>
                    <AniLink
                      fade
                      to="/confirmation"
                    >
                      <button className="btn btn-custom w-lg btn-red">
                        <span>Next</span>
                      </button>
                    </AniLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  answerQuestion: (qId, v) => dispatch(answerQuestion(qId, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
