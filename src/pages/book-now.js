import React from "react";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import Layout from "../components/layout";
import Banner from "../components/banner";
import * as R from "ramda";
import Radio from "@material-ui/core/Radio";
import { withStyles } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";
import yellowLocation from "../images/yellowlocation.png";
import redLocation from "../images/redlocation.png";
import { connect } from "react-redux";
import { answerQuestion } from "../state/actions/answers";

const icons = {
  red: redLocation,
  yellow: yellowLocation
}

const RedRadio = withStyles({
  root: {
    color: red[400],
    "&$checked": {
      color: red[600],
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedClinic: "",
    };
    this.setSelectedClinic = this.setSelectedClinic.bind(this);
  }
  componentWillMount() {
    const selectedClinic = this.props.state.answers["selectedClinic"];
    this.setState({ selectedClinic });
  }

  setSelectedClinic(value) {
    const { answerQuestion } = this.props;
    answerQuestion("selectedClinic", { value });
    this.setState({ selectedClinic: value });
  }

  render() {
    const { selectedClinic } = this.state;
    const isDisabled = R.isEmpty(this.state.selectedClinic) || R.isNil(this.state.selectedClinic);
    
    return (
      <Layout showProgress={true} step={0} loadBookingInteraction={true}>
        <div>
          <div class="user-option-box">
            <Banner
              title="Clinics"
              subtitle="Select the clinic you would like to make a booking for"
            />
            <div className="rows user-option-row">
              <div className="row">
                <div class="control">
                  <div class="columns">
                    {this.props.state.location.map(l => {
                      const conf = R.head(this.props.state.locationConfig.filter(i => { return i.name === l}))
                      console.log("conf",conf)
                      const icon = icons[conf.color];
                      return(
                        <div class="column has-text-centered" key={l}>
                          <div
                            class="box color-box"
                            onClick={() => this.setSelectedClinic(l)}
                          >
                            <figure class="image is-128x128">
                              <img src={icon} />
                            </figure>
                          </div>
                          <h3>{l}</h3>
                          <p>{conf.street}</p>
                          <br />
                          <RedRadio
                            onClick={() => this.setSelectedClinic(l)}
                            checked={selectedClinic === l}
                            value="hendrina"
                            name="clinic-choice"
                            inputProps={{ "aria-label": l }}
                          />
                        </div>
                      )
                    })}
                    
                  </div>
                </div>
              </div>
              <br />
              <div class="row">
                <div className="container text-is-centered">
                  <div className="buttons">
                    <AniLink fade to="/registration" duration={0.8}>
                      <button className="btn btn-custom w-lg">
                        Back
                      </button>
                    </AniLink>
                    <AniLink
                      to={isDisabled ? "/book-now" : "/date"}
                      fade
                      duration={0.8}
                      disabled={isDisabled}
                    >
                      <button
                        className="btn btn-custom w-lg btn-red"
                        disabled={isDisabled}
                      >
                        <span>Next</span>
                      </button>
                    </AniLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  answerQuestion: (qId, v) => dispatch(answerQuestion(qId, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
