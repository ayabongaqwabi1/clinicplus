import React from 'react'
import Layout from '../components/layout'

const NotFoundPage = () => (
  <Layout>
    <h1>404 | PAGE NOT FOUND </h1>
    <p>You just hit a route that doesnt exist</p>
  </Layout>
)

export default NotFoundPage
