import React from "react";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import Layout from "../components/layout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import * as R from "ramda";
import Banner from "../components/banner";
import short from "short-uuid";
import config from "../config";
import _ from "lodash";
import { connect } from "react-redux";
import { answerQuestion } from "../state/actions/answers";

const services = config.questions["employeeMedicalService"].options;

const ServiceRow = ({
  onDelete,
  answerQuestion,
  employeeCount,
  serviceType,
}) => {
  return (
    <div className="row service-row">
      <FormControl variant="filled">
        <InputLabel htmlFor="filled-age-native-simple">Service</InputLabel>
        <Select
          native
          value={serviceType}
          onChange={answerQuestion("serviceType")}
          label="Service"
          inputProps={{
            name: "age",
            id: "outlined-age-native-simple",
          }}
        >
          {services.map((s) => (
            <option value={_.camelCase(s)}>{s}</option>
          ))}
        </Select>
      </FormControl>
      <TextField
        id="outlined-number"
        label="Number"
        type="number"
        value={employeeCount}
        onChange={answerQuestion("employeeCount")}
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
      />
      <span className="trash" onClick={onDelete}>
        <span class="icon is-small">
          <FontAwesomeIcon icon={"trash"} />
        </span>
      </span>
      <br />
    </div>
  );
};

class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      serviceRows: {},
    };
    this.answerQuestion = this.answerQuestion.bind(this);
    this.addServiceRow = this.addServiceRow.bind(this);
    this.deleteServiceRow = this.deleteServiceRow.bind(this);
  }

  componentWillMount() {
    const servicesState = this.props.state.answers["services"];
    if (R.isNil(servicesState) || R.isEmpty(servicesState)) {
      const serviceRows = R.assoc(
        short.generate(),
        { employeeCount: 0, serviceType: services[0] },
        {}
      );
      this.setState({ serviceRows });
    } else {
      this.setState({ serviceRows: servicesState });
    }
  }

  answerQuestion(rowId, questionId, event) {
    const { serviceRows } = this.state;
    const answer = event.target.value;
    const updatedRows = R.assocPath([rowId, questionId], answer, serviceRows);
    this.setState({ serviceRows: updatedRows });
    this.props.answerQuestion("services", updatedRows);
  }

  addServiceRow() {
    const { serviceRows } = this.state;
    const updatedRows = R.assoc(
      short.generate(),
      { employeeCount: 0, serviceType: services[0] },
      serviceRows
    );
    this.setState({ serviceRows: updatedRows });
  }

  deleteServiceRow(rowId) {
    const { serviceRows } = this.state;
    const updatedRows = R.dissoc(rowId, serviceRows);
    this.setState({ serviceRows: updatedRows });
  }

  render() {
    const { serviceRows } = this.state;
    const curriedAnswerQuestion = R.curry((rowId, qId, answer) =>
      this.answerQuestion(rowId, qId, answer)
    );
    const services = this.props.state.answers["services"];
    const isDisabled = R.isNil(services) || R.isEmpty(services);

    return (
      <Layout showProgress={true} step={1}>
        <div className="container center">
          <div className="user-option-box">
            <Banner
              title="Services"
              subtitle="Select the services you would like performed on your employees"
            />
            <div className="rows user-option-row">
              <div className="row services-container">
                <div className="container text-is-centered">
                  <div className="rows">
                    {R.keys(serviceRows).map((rowId) => (
                      <ServiceRow
                        answerQuestion={curriedAnswerQuestion(rowId)}
                        onDelete={() => this.deleteServiceRow(rowId)}
                        {...R.prop(rowId, serviceRows)}
                      />
                    ))}
                  </div>
                </div>
                <button
                  className="button is-small is-white service-button"
                  onClick={this.addServiceRow}
                >
                  Add Service
                </button>
              </div>
              <br />
              <br />
              <br />
              <div class="row">
                <div className="container text-is-centered">
                  <br />
                  <div className="buttons">
                    <AniLink fade duration={0.8} to={"/book-now"}>
                      <button className="btn btn-custom w-lg">Back</button>
                    </AniLink>
                    <AniLink
                      fade
                      duration={0.8}
                      to={isDisabled ? "/services" : "/date"}
                    >
                      <button
                        className="btn btn-custom w-lg btn-red"
                        disabled={isDisabled}
                      >
                        <span>Next</span>
                      </button>
                    </AniLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  answerQuestion: (qId, v) => dispatch(answerQuestion(qId, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
