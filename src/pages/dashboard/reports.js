import React from "react";
import Layout from "../../components/layouts/dashboard";
import config from "../../config";
import { connect } from "react-redux";
import { assoc, pipe, omit, mergeAll } from "ramda";
import ServiceTable from "../../components/servicesTable";

class IndexPage extends React.Component {
  render() {
    return (
      <Layout location={this.props.location}>
        {/* <div className="report-searchbars">
                    <div className="quick">
                        <SearchBar type="quick"/>
                    </div>
                    <div className="buttons">
                        <SearchBar type="buttons"/>
                    </div>
                </div> */}

        <br />
        <ServiceTable data={this.props.appointments} />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  const appointments = state.dashboard.appointments;
  let { services } = config;
  services = Object.keys(services);
  const simpleAppointments = appointments.map((ap) => {
    const { user } = ap;
    return pipe(
      assoc("customer", user.name),
      assoc("idNumber", user.idNo),
      assoc("occupation", user.occupation),
      omit(["approved", "user"])
    )(ap);
  });
  const awesomeApps = simpleAppointments.map((appointment) => {
    let appointmentServices = services.map((service) => {
      const people = appointment.employees.filter(
        (e) => e.appointmentType === service
      );
      return assoc(service, people.length, {});
    });
    appointmentServices = mergeAll(appointmentServices);
    return pipe(
      assoc("services", appointmentServices),
      omit(["employees"])
    )(appointment);
  });
  return {
    appointments: awesomeApps,
  };
};

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
