import React from "react";
import Layout from "../../components/layouts/dashboard";
import ServiceCount from "../../components/serviceCount";
import config from "../../config";
import { isEmpty, values } from "ramda";
import SearchBar from "../../components/searchBar";
class IndexPage extends React.Component {
  constructor() {
    super();
    this.filterServices = this.filterServices.bind(this);
    this.toggleUpdate = this.toggleUpdate.bind(this);
    this.state = {
      services: [],
      updated: false,
    };
  }

  toggleUpdate() {
    const { updated } = this.state;
    this.setState({ updated: !updated });
  }

  filterServices(services) {
    if (isEmpty(services)) {
      this.setState({ services: config.services });
    } else {
      this.setState({ services });
    }
  }

  componentWillMount() {
    const { services } = config;
    this.setState({ services });
  }

  render() {
    const { services } = this.state;
    return (
      <Layout
        location={this.props.location}
        onRefresh={() => this.toggleUpdate()}
      >
        <br />
        <br />
        <SearchBar
          type="long"
          onFilter={this.filterServices}
          searchKeys={["title"]}
          data={values(config.services)}
        />
        <br />
        <div className="service-counters">
          {values(services).map((service) => {
            return (
              <ServiceCount
                name={service.title}
                id={service.id}
                info={service.info}
                noCount
                showInfo
              />
            );
          })}
        </div>
      </Layout>
    );
  }
}

export default IndexPage;
