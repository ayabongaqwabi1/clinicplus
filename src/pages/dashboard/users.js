import React from "react";
import { connect } from "react-redux";
import { omit, pipe, filter, map } from "ramda";
import TABLE_DATA_ACTIONS from "../../config/tableDataActions";
import Layout from "../../components/layouts/dashboard";
import TabbedTable from "../../components/tabbedTable";
import SearchBar from "../../components/searchBar";
import "./style.scss";

const getTabs = (staff) => {
  const cleanstaff = staff.map((c) => omit(["_id", "appointment"], c));
  const current = pipe(
    filter((c) => c.isDeleted === false),
    map((c) => omit(["isDeleted"], c))
  )(cleanstaff);
  const deleted = pipe(
    filter((c) => c.isDeleted !== false),
    map((c) => omit(["isDeleted"], c))
  )(cleanstaff);
  const tabs = [
    { title: "current", data: current },
    { title: "deleted", data: deleted },
  ];
  return tabs;
};

class IndexPage extends React.Component {
  constructor() {
    super();
    this.filterStaff = this.filterStaff.bind(this);
    this.toggleUpdate = this.toggleUpdate.bind(this);
    this.state = {
      tabs: [],
      updated: false,
    };
  }

  toggleUpdate() {
    const { updated } = this.state;
    this.setState({ updated: !updated });
  }

  filterStaff(staff) {
    const tabs = getTabs(staff);
    this.setState({ tabs });
  }

  componentWillMount() {
    const tabs = getTabs(this.props.staff);
    this.setState({ tabs });
  }

  componentDidUpdate() {
    const tabs = getTabs(this.props.staff);
    if (!this.state.updated === true) {
      this.setState({ tabs, updated: true });
    }
  }

  render() {
    const { EDIT, DELETE } = TABLE_DATA_ACTIONS;
    const actions = [EDIT, DELETE];
    const { tabs } = this.state;
    return (
      <Layout
        location={this.props.location}
        onRefresh={() => this.toggleUpdate()}
      >
        <br />
        <SearchBar
          type="long"
          onFilter={this.filterStaff}
          searchKeys={["name", "email", "id", "type"]}
          data={this.props.staff}
        />
        <br />
        <TabbedTable
          tabs={tabs}
          actions={actions}
          type="user"
          appendedAction={() => this.toggleUpdate()}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    staff: state.dashboard.staff,
  };
};

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
