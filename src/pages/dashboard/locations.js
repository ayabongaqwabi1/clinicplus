import React from "react";
import Layout from "../../components/layouts/dashboard";
import { connect } from "react-redux";
import { assoc, pipe, omit, toLower } from "ramda";
import TABLE_DATA_ACTIONS from "../../config/tableDataActions";
import TabbedTable from "../../components/tabbedTable";
import SearchBar from "../../components/searchBar";
import moment from "moment";

export const getTableData = (apps, locations) => {
  const appointments = apps.map((ap) => {
    const { user, employees } = ap;
    return pipe(
      assoc("user", user.name),
      assoc("employees", employees.length)
    )(ap);
  });
  const tabs = locations.map((clinic) => {
    const clinicAps = appointments
      .filter((ap) => {
        return toLower(ap.location) === toLower(clinic);
      })
      .filter((ap) => {
        return ap.isDeleted === false;
      })
      .map((ap) => {
        return pipe(
          assoc("date", moment(ap.date, "DD/MM/YYYY").format("DD/MM/YYYY")),
          omit([
            "_id",
            "approved",
            "company",
            "services",
            "ndaUrl",
            "isDeleted",
            "deletedBy",
          ])
        )(ap);
      });
    return { title: clinic, data: clinicAps };
  });
  return { tabs, appointments };
};
class IndexPage extends React.Component {
  constructor() {
    super();
    this.filterAppointments = this.filterAppointments.bind(this);
    this.toggleUpdate = this.toggleUpdate.bind(this);
    this.state = {
      tabs: [],
    };
  }

  componentWillMount() {
    const stateApps = this.props.state.dashboard.appointments;
    const locations = this.props.state.location;
    const { tabs } = getTableData(stateApps, locations);
    this.setState({ tabs, appointments: stateApps });
  }

  toggleUpdate() {
    const { updated } = this.state;
    this.setState({ updated: !updated });
  }

  filterAppointments(id, appointments) {
    const locations = this.props.state.location;
    const { tabs } = getTableData(appointments, locations);
    this.setState({ tabs });
  }

  componentDidUpdate() {
    const stateApps = this.props.state.dashboard.appointments;
    const locations = this.props.state.location;
    const { tabs } = getTableData(stateApps, locations);
    if (!this.state.updated === true) {
      this.setState({ tabs, updated: true, appointments: stateApps });
    }
  }

  render() {
    const { DELETE, INFO } = TABLE_DATA_ACTIONS;
    const actions = [INFO, DELETE];
    const { tabs, appointments } = this.state;
    return (
      <Layout
        location={this.props.location}
        onRefresh={() => this.toggleUpdate()}
      >
        <br />
        <SearchBar
          type="buttons"
          onFilter={this.filterAppointments}
          appointments={appointments}
        />
        <br />
        <TabbedTable
          tabs={tabs}
          actions={actions}
          appendedAction={() => {}}
          type="appointment"
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({ state });

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
