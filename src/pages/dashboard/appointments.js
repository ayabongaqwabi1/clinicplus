import React from "react";
import { connect } from "react-redux";
import { assoc, pipe, omit } from "ramda";
import TABLE_DATA_ACTIONS from "../../config/tableDataActions";
import Layout from "../../components/layouts/dashboard";
import TabbedTable from "../../components/tabbedTable";
import SearchBar from "../../components/searchBar";
import "./style.scss";
import moment from "moment";

const flags = {
  0: "Pending",
  1: "Approved",
  2: "Declined",
};

export const getTabs = (appointments) => {
  const simpleAppointments = appointments.map((ap) => {
    const { user, employees, date, company } = ap;
    
    return pipe(
      assoc("user", user.name),
      assoc("employees", employees.length),
      assoc("company", company.name),
      (item) => {
        const { approved } = item;
        const setText = (flagNumber) => flags[flagNumber];
        return assoc(
          "status",
          { type: Number, text: setText(approved.status) },
          item
        );
      },
      assoc("date", moment(date, "DD/MM/YYYY").format("DD/MM/YYYY")),
      omit(["_id", "services", "approved", "ndaUrl", "isDeleted"])
    )(ap);
  });
  const tabs = [
    {
      title: "appointments",
      data: simpleAppointments.filter((a) => !a.isDeleted),
    },
  ];
  return tabs;
};

class IndexPage extends React.Component {
  constructor() {
    super();
    this.filterAppointments = this.filterAppointments.bind(this);
    this.toggleUpdate = this.toggleUpdate.bind(this);
    this.state = {
      tabs: [],
      updated: false,
    };
  }

  toggleUpdate() {
    const { updated } = this.state;
    this.setState({ updated: !updated });
  }

  filterAppointments(appointments) {
    const tabs = getTabs(appointments);
    this.setState({ tabs });
  }

  componentWillMount() {
    const tabs = getTabs(this.props.state.dashboard.appointments);
    this.setState({ tabs });
  }

  componentDidUpdate() {
    const tabs = getTabs(this.props.state.dashboard.appointments);
    if (!this.state.updated === true) {
      this.setState({ tabs, updated: true });
    }
  }

  render() {
    const { EDIT, DELETE, INFO } = TABLE_DATA_ACTIONS;
    const actions = [INFO, EDIT, DELETE];
    return (
      <Layout
        location={this.props.location}
        onRefresh={() => this.toggleUpdate()}
      >
        <br />
        <SearchBar type="quick" onFilter={this.filterAppointments} />
        <TabbedTable
          tabs={this.state.tabs}
          actions={actions}
          type="appointment"
          appendedAction={() => this.toggleUpdate()}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({ state });

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
