import React from "react";
import Layout from "../../components/layouts/dashboard";
import ServiceCount from "../../components/serviceCount";
import { connect } from "react-redux";
import { omit, values, flatten } from "ramda";
import SearchBar from "../../components/searchBar";
import TabbedTable from "../../components/tabbedTable";
import { isEmpty } from "lodash";

class IndexPage extends React.Component {
  render() {
    const actions = [];
    const appointments = this.props.appointments.filter((i) => !isEmpty(i));
    const employeeCount = flatten(appointments.map((a) => values(a.employees)))
      .length;
    const UTE = flatten(
      appointments.map((a) => {
        const { id, date, location, approved, employees, isDeleted } = a;
        return employees.map((e) => {
          const { name, medicalServices } = e;
          return {
            appointmentId: id,
            appointmentDate: date,
            location,
            medicalServices,
            employee: name,
            approved,
            isDeleted,
          }
        });
      })
    );
    const upcomingApps = UTE.filter((a) => parseInt(a.approved.status, 10) === 1 && a.isDeleted === false);
    const pendingApps = UTE.filter((a) => parseInt(a.approved.status, 10) === 0 && a.isDeleted === false);
    const declinedApps = UTE.filter((a) => parseInt(a.approved.status, 10) === 2 && a.isDeleted === false);
    const terminatedApps = UTE.filter((a) => a.isDeleted === true);
    const tabs = [
      {
        title: "Approved",
        data: upcomingApps.map((ap) => omit(["status", "isDeleted"], ap)),
      },
      { title: "Pending", data: pendingApps },
      { title: "Rejected", data: declinedApps },
      { title: "Terminated", data: terminatedApps },
    ];
    return (
      <Layout location={this.props.location}>
        <div className="report-searchbars">
          <div className="buttons">
            {/* <SearchBar type="buttons" /> */}
          </div>
        </div>
        <br />

        <div className="service-counters">
          <ServiceCount
            count={this.props.appointments.length}
            name={"Appointments"}
          />
          <ServiceCount count={`${UTE.length}H`} name={"Duration"} />
          <ServiceCount count={employeeCount} name={"Employees"} />
          <ServiceCount count={pendingApps.length} name={"Pending"} />
        </div>
        <br />
        <TabbedTable tabs={tabs} actions={actions} type="employee" isUserDashboardCollapsible />
        <br />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return state.dashboard;
};

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
