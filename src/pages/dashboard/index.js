import React from "react";
import { connect } from "react-redux";
import { assoc, pipe, omit, mergeAll, isNil, keys, equals } from "ramda";
import TABLE_DATA_ACTIONS from "../../config/tableDataActions";
import Layout from "../../components/layouts/dashboard";
import TabbedTable from "../../components/tabbedTable";
import config from "../../config";
import ServiceCount from "../../components/serviceCount";
import "./style.scss";
import SearchBar from "../../components/searchBar";
import fromEntries from "object.fromentries";
import moment from "moment";

export const getDashData = (state) => {
  const appointments = state.dashboard.appointments;
  const simpleAppointments = appointments.map((ap) => {
    const { user, employees, company, date } = ap;
    return pipe(
      assoc("user", user.name),
      assoc("company", company.name),
      assoc("employees", employees.length),
      assoc("date", moment(date, "DD/MM/YYYY").format("DD/MM/YYYY")),
      omit(["_id", "services", "ndaUrl"])
    )(ap);
  });
  const upcomingApps = simpleAppointments.filter(
    (a) => parseInt(a.approved.status, 10) === 1 && !a.isDeleted
  );
  const pendingApps = simpleAppointments.filter(
    (a) => parseInt(a.approved.status, 10) === 0 && !a.isDeleted
  );
  const declinedApps = simpleAppointments.filter(
    (a) => parseInt(a.approved.status, 10) === 2 && !a.isDeleted
  );
  const deletedApps = simpleAppointments.filter((a) => a.isDeleted === true);
  let tabs = [
    {
      title: "upcoming",
      data: upcomingApps.map((ap) =>
        omit(["approved", "isDeleted", "deletedBy", "accountId"], ap)
      ),
    },
    {
      title: "pending",
      data: pendingApps.map((ap) =>
        omit(["approved", "isDeleted", "deletedBy", "accountId"], ap)
      ),
    },
    {
      title: "declined",
      data: declinedApps.map((ap) =>
        omit(["approved", "isDeleted", "deletedBy", "accountId"], ap)
      ),
    },
  ];
  const deleted = {
    title: "deleted",
    data: deletedApps.map((ap) =>
      omit(["approved", "isDeleted", "deletedBy"], ap)
    ),
    isDeleted: true,
  };
  const user = state.auth;
  if (user.type === "super") {
    tabs.push(deleted);
  }
  const { services } = config;
  let _services;
  if (!Object.fromEntries) {
    _services = mergeAll(
      Object.keys(services).map((s) =>
        fromEntries(new Map([[s, { employees: [] }]]))
      )
    );
  } else {
    _services = mergeAll(
      Object.keys(services).map((s) =>
        Object.fromEntries(new Map([[s, { employees: [] }]]))
      )
    );
  }

  appointments.map((appointment) => {
    const { employees } = appointment;
    employees.map((emp) => {
      const { medicalServices } = emp;
      medicalServices.map((appointmentType) => {
        if (isNil(_services[appointmentType])) {
          _services = assoc(appointmentType, { employees: [] }, _services);
          _services[appointmentType].employees.push(emp);
        } else {
          _services[appointmentType].employees.push(emp);
        }
      });
    });
  });
  return {
    tabs,
    employeesByService: _services,
    simpleAppointments,
  };
};

class IndexPage extends React.Component {
  constructor() {
    super();
    this.filterAppointments = this.filterAppointments.bind(this);
    this.toggleUpdate = this.toggleUpdate.bind(this);
    this.state = {
      tabs: [],
      updated: false,
    };
  }

  toggleUpdate() {
    const { updated } = this.state;
    this.setState({ updated: !updated });
  }

  filterAppointments(id, appointments) {
    const upcomingApps = appointments.filter((a) => a.approved.status === 1);
    const pendingApps = appointments.filter((a) => a.approved.status === 0);
    const declinedApps = appointments.filter((a) => a.approved.status === 2);
    const tabs = [
      {
        title: "upcoming",
        data: upcomingApps.map((ap) => omit(["approved", "deletedBy"], ap)),
      },
      {
        title: "pending",
        data: pendingApps.map((ap) => omit(["approved", "deletedBy"], ap)),
      },
      {
        title: "declined",
        data: declinedApps.map((ap) => omit(["approved", "deletedBy"], ap)),
      },
    ];
    this.setState({ tabs });
  }

  componentWillMount() {
    const { tabs, employeesByService, simpleAppointments } = getDashData(
      this.props.state
    );
    this.setState({ tabs, employeesByService, simpleAppointments });
  }

  componentDidUpdate() {
    const { tabs, employeesByService, simpleAppointments } = getDashData(
      this.props.state
    );
    if (!this.state.updated === true) {
      this.setState({
        tabs,
        employeesByService,
        simpleAppointments,
        updated: true,
      });
    }
  }

  render() {
    const { EDIT, DELETE, INFO } = TABLE_DATA_ACTIONS;
    const actions = [INFO, EDIT, DELETE];
    const { employeesByService, tabs, simpleAppointments } = this.state;
    const { services } = config;
    return (
      <Layout onRefresh={() => this.toggleUpdate()}>
        <br />
        <SearchBar
          type="buttons"
          onFilter={this.filterAppointments}
          appointments={simpleAppointments}
        />
        <br />
        <div className="service-counters">
          {keys(employeesByService).map((key) => {
            const { employees } = employeesByService[key];
            return (
              <ServiceCount
                count={employees.length}
                name={services[key].title}
                key={services[key].title}
              />
            );
          })}
        </div>
        <TabbedTable
          tabs={tabs}
          actions={actions}
          appendedAction={() => this.toggleUpdate()}
          type="appointment"
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({ state });

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
