import React from "react";
import { connect } from "react-redux";
import { omit, pipe, filter, map } from "ramda";
import TABLE_DATA_ACTIONS from "../../config/tableDataActions";
import Layout from "../../components/layouts/dashboard";
import TabbedTable from "../../components/tabbedTable";
import SearchBar from "../../components/searchBar";
import "./style.scss";

const getTabs = (customers) => {
  const cleanCustomers = customers.map((c) => omit(["password", "_id"], c));
  const current = pipe(
    filter((c) => c.isDeleted === false),
    map((c) => omit(["isDeleted"], c))
  )(cleanCustomers);
  const deleted = pipe(
    filter((c) => c.isDeleted !== false),
    map((c) => omit(["isDeleted"], c))
  )(cleanCustomers);
  const tabs = [
    { title: "current", data: current },
    { title: "deleted", data: deleted, isDeleted: true, },
  ];
  return tabs;
};

class IndexPage extends React.Component {
  constructor() {
    super();
    this.filterCustomers = this.filterCustomers.bind(this);
    this.toggleUpdate = this.toggleUpdate.bind(this);
    this.state = {
      tabs: [],
      updated: false,
    };
  }

  toggleUpdate() {
    const { updated } = this.state;
    this.setState({ updated: !updated });
  }

  filterCustomers(customers) {
    const tabs = getTabs(customers);
    this.setState({ tabs });
  }

  componentWillMount() {
    const tabs = getTabs(this.props.customers);
    this.setState({ tabs });
  }

  componentDidUpdate() {
    const tabs = getTabs(this.props.customers);
    if (!this.state.updated === true) {
      this.setState({ tabs, updated: true });
    }
  }

  render() {
    const { EDIT, DELETE, INFO } = TABLE_DATA_ACTIONS;
    const actions = [INFO, EDIT, DELETE];
    const { tabs } = this.state;
    return (
      <Layout
        location={this.props.location}
        onRefresh={() => this.toggleUpdate()}
      >
        <br />
        <SearchBar
          type="long"
          onFilter={this.filterCustomers}
          searchKeys={["fullname", "email", "id", "appointment"]}
          data={this.props.customers}
        />
        <br />
        <TabbedTable
          tabs={tabs}
          actions={actions}
          type="customers"
          appendedAction={() => this.toggleUpdate()}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customers: state.dashboard.customers,
  };
};

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
