import React from "react";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import Layout from "../components/layout";
import Banner from "../components/banner";
import { connect } from "react-redux";
import { answerQuestion } from "../state/actions/answers";
import { isEmpty, omit, toPairs } from "ramda";
import config from "../config";
import {
  saveAnswersToUserDashboard,
  resetAnswerState,
} from "../state/actions/userDashboard";
import { navigate } from "gatsby";
import saveConfirmation from "../functions/confirmation";
class IndexPage extends React.Component {
  constructor() {
    super();
    this.saveToUserDashboard = this.saveToUserDashboard.bind(this);
  }

  saveToUserDashboard() {
    saveConfirmation(this.props.state.answers);
    const { dispatch, state } = this.props;
    dispatch(saveAnswersToUserDashboard(state.answers));
    dispatch(resetAnswerState());
    navigate("/login");
  }

  render() {
    const { questions } = config;
    const answers = toPairs(
      omit(
        ["appointmentDate", "employees", "services", "ndaUrl"],
        this.props.state.answers
      )
    );
    return (
      <Layout showProgress={false} step={1}>
        <div className="container center">
          <div className="user-option-box">
            <Banner
              title="Confirmation"
              subtitle="Please confirm the details below"
            />

            {!isEmpty(answers) && (
              <div className="details">
                <h1> Booking details</h1>
                {answers.map((pair) => (
                  <div>
                    <p>
                      <strong>{questions[pair[0]].label}: </strong>{" "}
                      {pair[1].value}{" "}
                    </p>
                  </div>
                ))}
              </div>
            )}

            <div className="rows employees-table">
              <div class="row">
                <div className="container text-is-centered">
                  <br />
                  <div className="buttons">
                    <AniLink fade duration={0.8} to={"/employees"}>
                      <button className="btn btn-custom w-lg">Go Back</button>
                    </AniLink>

                    <button
                      className="btn btn-custom w-lg btn-red"
                      onClick={() => this.saveToUserDashboard()}
                    >
                      <span>Done</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  answerQuestion: (qId, v) => dispatch(answerQuestion(qId, v)),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
