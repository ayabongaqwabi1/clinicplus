import React from "react";
import Layout from "../components/layout";
import medic from "../images/illustration.png";
import AniLink from "gatsby-plugin-transition-link/AniLink";
class IndexPage extends React.Component {
  render() {
    return (
      <Layout isHomePage>
        <div className="container center">
          <article className="media center main-page-article">
            <div className="media-content" style={{ width: "35vw" }}>
              <div className="content">
                <h1 className="is-uppercase is-size-1 brand-text-dark-red">
                  Occupational Health
                </h1>
                <p className="subtitle brand-text-color-grey is-size-4">
                  ClinicPlus offers comprehensive Occupational Health Management
                  and Consulting service to mines and industries. Our goal is to
                  help our clients manage their occupational health and safety
                  risks.
                </p>
                <div className="buttons">
                  <AniLink paintDrip duration={0.8} hex="#d9133ac4" to="/login">
                    <button className="btn btn-custom w-lg">My Bookings</button>
                  </AniLink>
                  <AniLink
                    paintDrip
                    duration={0.8}
                    hex="#d9133ac4"
                    to="/registration"
                  >
                    <button className="btn btn-custom w-lg">Book Now</button>
                  </AniLink>
                </div>
              </div>
            </div>
            <figure className="is-left">
              <span className="vector">
                <img src={medic} alt="gatsby-logo" style={{ width: "50vw" }} />
              </span>
            </figure>
          </article>
        </div>
        <p
          style={{
            textAlign: "center",
            position: "fixed",
            bottom: "10px",
            left: "40vw",
          }}
        >
          Built and run by{" "}
          <a href="https://facebook.com/midastouchsa">Midas Touch</a> in
          partnership with Tecla Digital
        </p>
      </Layout>
    );
  }
}

export default IndexPage;
