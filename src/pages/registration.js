import React from "react";
import Layout from "../components/layout";
import { Form } from "../functions/form";
import { isBrowser } from "react-device-detect";

class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = { activeForm: " " };
    this.changeForm = this.changeForm.bind(this);
  }
  componentWillMount() {
    this.setState({ activeForm: "registration" });
  }

  changeForm() {
    const { activeForm } = this.state;
    if (activeForm === "registration") {
      this.setState({ activeForm: "login" });
    } else {
      this.setState({ activeForm: "registration" });
    }
  }

  render() {
    const registrationForm = <Form formId="registration" key="registration" />;
    const loginForm = <Form formId="login" key="login" />;
    const isReg = this.state.activeForm === "registration";
    const activeFormComponent = isReg ? registrationForm : loginForm;
    const possesion = !isReg ? `don't have` : "have";
    const formLinkAlt = isReg ? "Login" : "Register";
    return (
      <Layout hideNav={!isBrowser}>
        <div className="container center">
          <div class="user-option-box force-transparency login">
            <div class="columns option-box-content">
              <div class="form-page-container">
                <h1> Get Started </h1>
                <p>
                  Save Time With Simple Online Medical Assesment Scheduling
                  Software
                </p>
                {activeFormComponent}
                {/* <p>
                  <small>
                    If you {possesion} an account you can{" "}
                    <a onClick={this.changeForm}>{formLinkAlt}</a>
                  </small>
                </p> */}
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default IndexPage;
