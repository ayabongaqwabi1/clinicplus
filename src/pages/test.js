import React from "react";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import Layout from "../components/layout";
import "react-dates/initialize"; // Make sure to import the default stylesheet
import "react-dates/lib/css/_datepicker.css";
import { DayPickerSingleDateController } from "react-dates";
import Banner from "../components/banner";
import { connect } from "react-redux";
import { answerQuestion } from "../state/actions/answers";
import moment from 'moment'
import { isNil, isEmpty } from "ramda";
class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      focused: false,
    };
    this.prevBtn = React.createRef();
    this.nextBtn = React.createRef();
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
  }

  componentWillMount() {
    const appointmentDate = this.props.state.answers["appointmentDate"];
    this.setState({ focused: appointmentDate, selected: appointmentDate });
  }

  onDateChage(date) {
    this.setState({ selected: date });
    this.props.answerQuestion("appointmentDate", { value: date });
  }

  onFocusChange(focused) {
    this.setState({ focused });
  }

  render() {
    const isDisabled =
      isEmpty(this.state.selected) || isNil(this.state.selected);
    return (
      <Layout showProgress={true} step={2}>
        <div className="container center">
          <div className="date-picker-box user-option-box">
            <Banner title="Date" subtitle="Date when Medical must be done" />
            <div className="rows user-option-row">
              <div className="row">
                <DayPickerSingleDateController
                  numberOfMonths={1}
                  isOutsideRange={(day) => {
                    
                    const today = moment();
                    console.log("\n---")
                    console.log(day.format("DD MMM YYYY"))
                    console.log("diff",today.diff(day, 'days'))
                    return today.diff(day, 'days') > 0;
                  }}
                  hideKeyboardShortcutsPanel={true}
                  onOutsideClick={(e) => {}}
                  onPrevMonthClick={(e) => {}}
                  onNextMonthClick={(e) => {}}
                  onClick={(e) => {}}
                  date={this.state.selected} // momentPropTypes.momentObj or null
                  onDateChange={this.onDateChage} // PropTypes.func.isRequired
                  focused={this.state.focused} // PropTypes.bool
                  onFocusChange={this.onFocusChange}
                />
              </div>
              <div class="row">
                <div className="container text-is-centered">
                  <br />
                  <div className="buttons">
                    <AniLink fade duration={0.8} to={"/registration"}>
                      <button className="btn btn-custom w-lg">Back</button>
                    </AniLink>
                    <AniLink fade to="/forms/company">
                      <button
                        className="btn btn-custom w-lg btn-red"
                        disabled={isDisabled}
                      >
                        <span>Next</span>
                      </button>
                    </AniLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  answerQuestion: (qId, v) => dispatch(answerQuestion(qId, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
