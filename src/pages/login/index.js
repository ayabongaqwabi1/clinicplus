import React from "react";
import Layout from "../../components/layout";
import { Form } from "../../functions/form";
import {
  saveAppointment,
  hydrateAppointments,
} from "../../state/actions/appointment";
import { saveCustomers, saveStaff, saveAccounts } from "../../state/actions/startup";
import { saveUser } from "../../state/actions/auth";
import axios from "axios";
import "./style.scss";
import { connect } from "react-redux";
import PageButton from "../../components/buttons/pageButton";
import { navigate } from "gatsby";
import loading from "../../images/loading2.gif";
import { GET_SERVER } from "../../config/server";
import { isNil, isEmpty } from "ramda";
import ClinicPlusLogo from "../../components/nav/dashboard/logo";
import jwt from "json-web-token";
import { GATSBY_ACCESS_TOKEN_SECRET } from "../../config/access";
import Cookies from "js-cookie";
class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      isLoading: false,
      loginMessage: "",
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.answer = this.answer.bind(this);
  }

  answer(id) {
    return (e) => {
      const value = e.target.value;
      let state = this.state;
      state[id] = value;
      this.setState(state);
    };
  }

  onSubmit() {
    const { dispatch } = this.props;
    const { email, password } = this.state;
    const login = async () => {
      try {
        this.setState({ isLoading: true });
        const response = await axios.post(`${GET_SERVER()}/api/auth/login`, {
          email,
          password,
        });

        const data = jwt.decode(
          GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
          response.data
        ).value;
        const user = data.user;

        Cookies.set("clinicplus_user", user);

        if (isEmpty(user) || isNil(user)) {
          this.setState({
            loginMessage: "Error user not found",
            isLoading: false,
          });
        } else if (
          user.type === "super" ||
          user.type === "doctor" ||
          user.type === "admin"
        ) {
          const { customers, appointments, users, accounts } = data;
          dispatch(saveAccounts(accounts));
          dispatch(saveCustomers(customers));
          dispatch(saveStaff(users));
          dispatch(hydrateAppointments(appointments));
          dispatch(saveUser(user));
          navigate("/dashboard");
        } else {
          const { customer, appointments, user, account } = data;
          dispatch(hydrateAppointments(appointments));
          dispatch(saveAccounts([account]));
          dispatch(saveCustomers([customer]));
          dispatch(saveUser(user));
          navigate("/dashboard/user-dashboard");
        }
      } catch (err) {
        // Handle Error Here
        this.setState({
          loginMessage: !isNil(err.response) ? err.response.data.msg : !isNil(err.message) ? err.message : err,
          isLoading: false,
        });
      }
    };

    login();
  }

  render() {
    const { isLoading } = this.state;
    return (
      <Layout isHomePage hideNav>
        <div className="container center loginPage">
          <article className="media center main-page-article">
            <div className="not-logged-in">
              <div className="login-logo">
                <ClinicPlusLogo />
              </div>
              <div className="login-info">
                <h1 className="is-size-3 brand-text-dark-red">Login</h1>
                <p>To access your bookings please fill in the details below</p>
                <p> Email </p>
                <input
                  disabled={isLoading}
                  type="text"
                  onChange={this.answer("email")}
                  value={this.state.email}
                />
                <p> Password </p>
                <input
                  disabled={isLoading}
                  type="password"
                  onChange={this.answer("password")}
                  value={this.state.password}
                />
                <br />
                <PageButton
                  disabled={isLoading}
                  onClick={this.onSubmit}
                  color="orange"
                >
                  {" "}
                  Login{" "}
                </PageButton>

                <div className="login-loading">
                  {isLoading && <img src={loading} alt="loading" />}
                </div>
                <p style={{ textAlign: "center", color: "red" }}>
                  {this.state.loginMessage}
                </p>
              </div>
            </div>
          </article>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
