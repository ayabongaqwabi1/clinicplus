import React from "react";
import { generateFormPage } from "../../functions/form";

const COMPANY_FORM = "company";
class CompanyPage extends React.Component {
  render() {
    return <div>{generateFormPage(COMPANY_FORM)}</div>;
  }
}

export default CompanyPage;
