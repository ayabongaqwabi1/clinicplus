import React from "react";
import { generateFormPage } from "../../functions/form";
import { answerQuestion } from '../../state/actions/answers';
import { connect } from "react-redux";
const PREBOOKING_FORM = "prebooking";

function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result.toUpperCase();
}
class PrebookingPage extends React.Component {
  componentWillMount(){
    const dispatch = this.props.dispatch;
     const orderNumber = makeid(8);
     dispatch(answerQuestion("orderNumber", {value: orderNumber, isValid:true} ))
    
  }

  render() {
    return <div>
      {generateFormPage(PREBOOKING_FORM, true)}
      
      </div>;
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(PrebookingPage);
