import forms from "./forms";
import questions from "./questions";

const dashPath = "/dashboard";
const toDashPath = (p) => `${dashPath}/${p}`;

const userDashboardNavLinks = [
  {
    title: "dashboard",
    icon: "dash",
    href: toDashPath("user-dashboard"),
  },
  // {
  //   title: "email notifications",
  //   icon: "email",
  //   href: "",
  // },
  // {
  //   title: "settings",
  //   icon: "settings",
  //   href: toDashPath("settings"),
  // },
];

const dashboardNavLinks = [
  {
    title: "dashboard",
    icon: "dash",
    href: toDashPath(""),
  },
  {
    title: "appointments",
    icon: "bell",
    href: toDashPath("appointments"),
  },
  {
    title: "customers",
    icon: "addUser",
    href: toDashPath("customers"),
  },
  {
    title: "Users",
    icon: "users",
    href: toDashPath("users"),
  },
  {
    title: "services",
    icon: "services",
    href: toDashPath("services"),
  },
  {
    title: "reports",
    icon: "dash",
    href: toDashPath("reports"),
  },
  {
    title: "locations",
    icon: "location",
    href: toDashPath("locations"),
  },
  {
    title: "email notifications",
    icon: "email",
    href: toDashPath(""),
  },
  {
    title: "settings",
    icon: "settings",
    href: toDashPath(""),
  },
];

const docNavLinks = [
  {
    title: "dashboard",
    icon: "dash",
    href: toDashPath(""),
  },
  {
    title: "appointments",
    icon: "bell",
    href: toDashPath("appointments"),
  },
  {
    title: "customers",
    icon: "addUser",
    href: toDashPath("customers"),
  },
  {
    title: "services",
    icon: "services",
    href: toDashPath("services"),
  },
  {
    title: "reports",
    icon: "dash",
    href: toDashPath("reports"),
  },
  {
    title: "locations",
    icon: "location",
    href: toDashPath("locations"),
  },
  {
    title: "email notifications",
    icon: "email",
    href: toDashPath(""),
  },
  {
    title: "settings",
    icon: "settings",
    href: toDashPath(""),
  },
];

const services = {
  "medical-examination": {
    title: "Medical Examination",
    id: "medical-examination",
    info: `Medical assessment means a visual and physical inspection of the consumer, noting deviations from the norm, and a statement of the consumer's mental and physical condition that can be amendable to or resolved by appropriate actions of the provider.`,
  },
  "x-ray": {
    title: "X - Rays",
    id: "x-ray",
    info: `An X-ray is a quick, painless test that produces images of the structures inside your body — particularly your bones. X-ray beams pass through your body, and they are absorbed in different amounts depending on the density of the material they pass through`,
  },
  "6-in-1-drug-test": {
    title: "6 in 1 Drug Test",
    id: "6-in-1-drug-test",
    info: `Our 6-test panel drug test uses urine to check for the presence of drug abuse. In comparison to our 10-panel drug test, this test covers the most popular illicit drugs, also known as “street drugs” which are rarely prescribed.`,
  },
  cannabis: {
    title: "Canbabis/Dagga",
    id: "cannabis",
    info: `Cannabis drug testing describes various drug test methodologies for the use of cannabis in medicine, sport, and law. Cannabis use is highly detectable and can be detected by urinalysis, hair analysis, as well as saliva tests for days or weeks.`,
  },
  induction: {
    title: "Induction",
    id: "induction",
    info: `In human resource development, induction training is a form of introduction for new employees in order to enable them to do their work in a new profession or job role within an organisation. Training can be systematic or unsystematic training. Induction training is systematic training`,
  },
  "pregnancy-test": {
    title: "Pregnancy Test",
    id: "pregnancy-test",
    info: "saldkjlsd",
  },
  "sugar-test": {
    title: "Sugar Test",
    id: "sugar-test",
    info: "saldkjlsd",
  },
  clearance: {
    title: "Clearance",
    id: "clearance",
    info: "saldkjlsd",
  },
  "vienna-test": {
    title: "Dover / Vienna Test",
    id: "vienna-test",
    info: "saldkjlsd",
  },
  hiv: {
    title: "HIV",
    id: "hiv",
    info: "saldkjlsd",
  },
  "fire-training": {
    title: "Fire Awareness Training",
    id: "fire-awareness-training",
    info: "saldkjlsd",
  },
  "first-aid-training": {
    title: "First Aid Training",
    id: "first-aid-training",
    info: "saldkjlsd",
  },
  "covid-19-rapid-test": {
    title: "Covid-19 Rapid Test",
    id: "covid-19-rapid-test",
    info: "saldkjlsd",
  },
  "covid-19-screen-questionnaire": {
    title: "Covid-19 Screen Questionnaire",
    id: "covid-19-screen-questionnaire",
    info: "saldkjlsd",
  },
};
export default {
  forms,
  questions,
  services,
  dashboardNavLinks,
  userDashboardNavLinks,
  docNavLinks,
};
