export const DEV_SERVER = "http://localhost:3067";
export const PRODUCTION_SERVER = process.env.GATSBY_DATABASE_URL;

export const IS_DEV = () => process.env.NODE_ENV === 'dev';

export const GET_SERVER = () => IS_DEV() ? DEV_SERVER : PRODUCTION_SERVER;