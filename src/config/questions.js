export default {
  name: {
    label: "Name",
    type: "text",
    validator: "free-text",
  },
  selectedClinic: {
    label: "Selected Clinic",
    type: "text",
    validator: "free-text",
  },
  surname: {
    label: "Surname",
    type: "text",
    validator: "free-text",
  },
  email: {
    label: "Email",
    type: "text",
    validator: "email",
  },
  invoiceEmail: {
    label: "Invoice Email",
    type: "text",
    validator: "email",
  },
  bookingsEmail: {
    label: "Bookings Email",
    type: "text",
    validator: "email",
  },
  billingEmail: {
    label: "Statement Email",
    type: "text",
    validator: "email",
  },
  signupPassword: {
    label: "Create Password",
    type: "text",
    validator: "free-text",
  },
  signinPassword: {
    label: "Password",
    type: "password",
    validator: "free-text",
  },
  registrationName: {
    label: "Registration Name",
    type: "text",
    validator: "free-text",
  },
  tradingName: {
    label: "Trading Name",
    type: "text",
    validator: "free-text",
  },
  billingCompany: {
    label: "Company Responsible for Account",
    type: "text",
    validator: "free-text",
  },
  medicalsCompany: {
    label: "Company Requesting Medicals",
    type: "text",
    validator: "free-text",
  },
  orderNumber: {
    label: "Order Number",
    type: "text",
    validator: "free-text",
  },
  companyContactPerson: {
    label: "Contact Person",
    type: "text",
    validator: "free-text",
  },
  contactNumber: {
    label: "Contact Number",
    type: "text",
    validator: "free-text",
  },
  registrationNumber: {
    label: "Registration Number",
    type: "text",
    validator: "free-text",
  },
  vat: {
    label: "VAT Registration",
    type: "text",
    validator: "free-text",
  },
  postalAddress: {
    label: "Postal Address",
    type: "text",
    validator: "free-text",
    multiline: true,
    rows: 2,
  },
  physicalAddress: {
    label: "Physical Address",
    type: "text",
    validator: "free-text",
    multiline: true,
    rows: 2,
  },
  employeeIdeNumber: {
    label: "Registration Number",
    type: "text",
    validator: "free-text",
  },
  employeeIdNumber: {
    label: "Id Number or Passport",
    type: "text",
    validator: "free-text",
  },
  employeeInitialsAndSurname: {
    label: "Employee Name and Surname",
    type: "text",
    validator: "free-text",
  },
  employeeOccupation: {
    label: "Occupation",
    type: "text",
    validator: "free-text",
  },
  employeeMedicalService: {
    label: "Select tests to be done",
    type: "checkbox-group",
    required: true,
    error: "",
    component: "fieldset",
    options: [
      "medical-examination",
      "x-ray",
      "6-in-1-drug-test",
      "cannabis",
      "induction",
      "pregnancy-test",
      "sugar-test",
      "clearance",
      "vienna-test",
      "fire-training",
      "first-aid-training",
      "hiv",
      "covid-19-rapid-test",
      "covid-19-screen-questionnaire"
    ],
  },
};
