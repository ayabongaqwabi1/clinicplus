
const performAction = func => func();

const TABLE_DATA_ACTIONS = {
    EDIT: {
        id: "EDIT",
        icon: 'pencil',
        performAction,
    },
    DELETE: {
        id: "DELETE",
        icon: 'trash',
        performAction,
    },
    INFO: {
        id: "INFO",
        icon: 'info',
        performAction,
    },
}

export default TABLE_DATA_ACTIONS;