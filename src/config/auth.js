export default {
  edit: {
    admin: ["super", "admin"],
    doctor: ["super", "admin"],
    customer: ["super", "admin"],
    super: ["super"],
  },
};
