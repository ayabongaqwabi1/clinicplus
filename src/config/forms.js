import nearly from '../images/nearly.png';
import nearlyMobile from '../images/nearly_mobile.png';
import {
    isBrowser,
  } from "react-device-detect";
export default {
    'registration': {
        title: "Register",
        description: "If you are a new user please fill in the form below",
        questions: ["name", "surname", "email", "signupPassword"],
        next: "/book-now",
        button: {
            label: "Register",
        }
    },
    'login': {
        title: "Sign in",
        description: "If you are an existing user you can sign in here",
        questions: [ "email", "signinPassword"],
        next: "/book-now",
        button: {
            label: "Sign in",
        }
    },
    'company': {
        title: "Company Details",
        description: "Please enter the relevant info about your company",
        questions: [ "registrationName", "registrationNumber", "tradingName", "vat", "physicalAddress", "postalAddress", "invoiceEmail", "bookingsEmail", "billingEmail"],
        next: "/forms/prebooking",
        button: {
            label: "Submit",
            splash: !isBrowser ? nearlyMobile : nearly,
        }
    },
    'prebooking': {
        title: "Pre Booking",
        description: "Please enter the relevant pre booking info about your request",
        questions: [ "billingCompany", "medicalsCompany", "orderNumber", "companyContactPerson", "contactNumber"],
        next: "/employees",
        button: {
            label: "Submit",
        }
    },
    'employee': {
        title: "Employee Details",
        description: "",
        questions: [ "employeeInitialsAndSurname", "employeeIdNumber", "employeeOccupation", "employeeMedicalService"],
    }

}