import { isEmpty, isNil } from 'lodash';
import { last } from 'ramda';
import React  from 'react';

const flags= {
    0: "Pending",
    1: "Approved",
    2: "Declined"
};
class Collapse extends React.Component{
    constructor(){
        super();
        this.state = {
            open: false,
        }
        this.setOpen = this.setOpen.bind(this);
    }

    setOpen(i){
        this.setState({open: i})
    }

    render(){
        const toggle = () => {
            if(this.state.open){
                this.setOpen(false)
            }
            else{
                this.setOpen(true);
            }
        }
        const { medicalServices, employee, location, appointmentDate, appointmentId, approved} = this.props.data;
        const audit = last(approved.audit)
        return(
        <div className="collapse-container">
            <div onClick={() => toggle()} className={this.state.open  ? "collapsible open" : "collapsible closed"}>
                <div className="collapse-title"><h5>{this.props.index+1}. {" "} {employee}</h5></div>
                    
                    <span className={this.state.open ? "collapse-par open" : "collapse-par closed" }> <strong>Appointment is part of booking with ID: </strong>{appointmentId}</span>

                    <span className={this.state.open ? "collapse-par open" : "collapse-par closed" }> <strong>Appointment Date: </strong>{appointmentDate}</span>
                
                
                    <span className={this.state.open ? "collapse-par open" : "collapse-par closed" }> <strong>Location:</strong> {location}</span>
                
                    <span className={this.state.open ? "collapse-par open" : "collapse-par closed" }>
                        <strong>Services:</strong>
                        <ol>
                            {medicalServices.map(i=> <li>{i}</li>)}
                        </ol>
                    </span>
                    {!isEmpty(approved.audit) &&
                        <div className={this.state.open ? "collapse-par open tracking" : "collapse-par closed" }>
                            <h2>Tracking</h2>
                            {approved.audit.map(a => {
                                return(
                                    <div>
                                        <p className="approved-status">Changed from {flags[a.from]} to {flags[a.to]}</p>
                                        <p className="reason-title">Reason</p>
                                        <p className="reason">{a.reason}</p>
                                    </div>
                                )
                            })}
                        </div>
                    }
            </div>       
        </div>
        )
    }
}

export default Collapse;
