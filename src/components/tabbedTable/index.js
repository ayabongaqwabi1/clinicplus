import React from 'react';
import { keys, assoc, append, mergeAll } from 'ramda';
import './style.scss';
import { head } from 'lodash';
import DialogContext from '../layouts/context';
import Icon from '../buttonIcon';
import { isEmpty } from 'ramda';
import TABLE_DATA_ACTIONS from '../../config/tableDataActions';
import Collapsible from './collapse';

const _capitalize = (str, lower = false) =>
  (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, (match) =>
    match.toUpperCase()
  );
const capitalize = (str) =>
  _capitalize(str.replace(/([a-z0-9])([A-Z])/g, '$1 $2'));

const createTableIdFromTitle = (text) => {
  return text.split(' ').join('').toLowerCase();
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const RoundTable = ({ keys, data, visible, actions, action, tableType, appendedAction, isDeleted, isUserDashboardCollapsible }) => {
  const tableClass = visible ? 'show-table' : 'hide-table';
  const collapsibleClass = visible ? 'show-collapsible' : 'hide-collapsible';
  const { INFO } = TABLE_DATA_ACTIONS;
  return (
    <React.Fragment>
    {!isUserDashboardCollapsible && 
      <table className={tableClass}>
      {!isDeleted &&
        <React.Fragment>
          <thead>
            <tr>
              {keys.map((k) => <th key={`${k}-${getRandomInt(0, 10000)}`}>{capitalize(k)}</th>)}
              <th>{isEmpty(data) ? "No data" : "Actions"}</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => {
              return (
                <tr key={`rtable-i-${getRandomInt(0, 10000)}`}>
                  {keys.map((k) => {
                    if (item[k].type == Number) {
                      return (
                        <td key={`${item[k].text}-${getRandomInt(0, 10000)}`}>
                          <span className={`${item[k].text}`}>{item[k].text}</span>
                        </td>
                      );
                    }
                    return <td key={`${item[k]}--${getRandomInt(0, 10000)}`}>{item[k]}</td>;
                  })}
                  <td>
                    {actions.map((act) => {
                      return (<span key={`${act.id}-${getRandomInt(0, 10000)}`}>
                        <Icon
                          name={act.icon}
                          alt={act.id}
                          onClick={() => {
                            action(act.id, tableType, item, appendedAction)
                            if(appendedAction){
                              appendedAction()
                            }
                          }}
                        />
                      </span>)
                    })}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </React.Fragment>
      }
      {isDeleted &&
        <React.Fragment>
          <thead>
            <tr>
              {keys.map((k) => <th key={`${k}-${getRandomInt(0, 10000)}`}>{capitalize(k)}</th>)}
              <th>{isEmpty(data) ? "No data" : "Actions"}</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => {
              return (
                <tr key={`rtable-i-${getRandomInt(0, 10000)}`}>
                  {keys.map((k) => {
                    if (item[k].type == Number) {
                      return (
                        <td key={`${item[k].text}-${getRandomInt(0, 10000)}`}>
                          <span className={`${item[k].text}`}>{item[k].text}</span>
                        </td>
                      );
                    }
                    return <td key={`${item[k]}--${getRandomInt(0, 10000)}`}>{item[k]}</td>;
                  })}
                  <td>
                    <span key={`${INFO.id}-${getRandomInt(0, 10000)}`}>
                        <Icon
                          name={INFO.icon}
                          alt={INFO.id}
                          onClick={() => {
                            action(INFO.id, tableType, item, appendedAction)
                            if(appendedAction){
                              appendedAction()
                            }
                          }}
                        />
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </React.Fragment>
      }
    </table>
    }
    {isUserDashboardCollapsible &&
      <div className={collapsibleClass}>
        {isEmpty(data) && <h1 style={{width: "100%", textAlign:"center", padding:"10px"}}>No data</h1>}
        {data.map((item, index) => {
              return (
                <Collapsible data={item} index={index}/>
              );
        })}
      </div>
    }
    </React.Fragment>
  );
};

const ContextRoundTable = (props) => {
  return (
    <DialogContext.Consumer>
      {(context) => {
        return <RoundTable {...props} action={context.openDialog} />;
      }}
    </DialogContext.Consumer>
  );
};

class IndexPage extends React.Component {
  constructor() {
    super();
    this.viewTable = this.viewTable.bind(this);
    this.state = {};
  }

  componentWillMount() {
    const { tabs } = this.props;
    const firstTableId = createTableIdFromTitle(head(tabs).title);
    let tableState = {};
    tableState[firstTableId] = { visible: true };
    const tables = tabs
      .filter((t) => createTableIdFromTitle(t.title) !== firstTableId)
      .map((t) => {
        const id = createTableIdFromTitle(t.title);
        return assoc(id, { visible: false }, {});
      });
    this.setState(mergeAll([tableState, mergeAll(tables)]));
  }

  viewTable(id) {
    const { tabs } = this.props;
    let tableState = {};
    tableState[id] = { visible: true };
    const tables = tabs
      .filter((t) => createTableIdFromTitle(t.title) !== id)
      .map((t) => {
        const id = createTableIdFromTitle(t.title);
        return assoc(id, { visible: false }, {});
      });
    this.setState(mergeAll([tableState, mergeAll(tables)]));
  }

  render() {
    
    const { tabs, actions, type, appendedAction, isUserDashboardCollapsible } = this.props;
    const tabTitles = tabs.map((tab) => tab.title);
    const mappedTitles = tabTitles.map((title) => {
      const tableId = createTableIdFromTitle(title);
      const pClass = this.state[tableId].visible ? 'selected' : '';
      return (
        <p
          className={pClass}
          id={tableId}
          onClick={() => this.viewTable(tableId)}
          key={`tableId-${getRandomInt(0, 10000)}`}
        >
          <strong>{title}</strong>
        </p>
      );
    });
    return (
      <div className="round-table">
        <div className="tab-titles">
          {tabTitles.length > 1 ? mappedTitles : <p />}
        </div>
        {tabs.map((tab) => {
          const tableId = createTableIdFromTitle(tab.title);
          return (
            <ContextRoundTable
              visible={this.state[tableId].visible}
              keys={keys(tab.data[0])}
              actions={actions}
              data={tab.data}
              tableType={type}
              key={`type-${getRandomInt(0, 10000)}`}
              appendedAction={appendedAction}
              isDeleted={tab.isDeleted}
              isUserDashboardCollapsible={isUserDashboardCollapsible}
            />
          );
        })}
      </div>
    );
  }
}

export default IndexPage;
