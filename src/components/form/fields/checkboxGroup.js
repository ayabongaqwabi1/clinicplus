import React from "react";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import * as R from "ramda";

export default class extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = {};
  }

  componentWillMount() {
    const options = this.props.options;
    const getChecked = (option) => {
      const selecteds = this.props.selected;
      return R.isNil(selecteds)
        ? false
        : !R.isEmpty(selecteds.filter((s) => s === option));
    };
    const newState = R.mergeAll(
      options.map((option) =>
        R.assoc(option, { checked: getChecked(option), id: option }, {})
      )
    );
    this.setState(newState);
  }

  handleChange(e) {
    const name = e.target.name;
    const currentState = this.state;
    const newState = R.assoc(
      name,
      { checked: e.target.checked, id: name },
      currentState
    );
    this.setState(newState);
    const selecteds = R.values(newState)
      .filter((i) => i.checked === true)
      .map((i) => i.id);
    this.props.onChange(R.assoc("selected", selecteds, this.props));
  }

  render() {
    const { options } = this.props;
    const segments = R.splitEvery(3, options);
    return (
      <FormControl component="fieldset">
        <FormLabel component="legend">{this.props.label}</FormLabel>
        {segments.map((seg) => {
          return (
            <FormGroup row>
              {seg.map((option) => {
                const checked = this.props.selected
                  ? !R.isEmpty(this.props.selected.filter((i) => i === option))
                  : false;
                return (
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={checked}
                        onChange={this.handleChange}
                        name={option}
                      />
                    }
                    label={option}
                  />
                );
              })}
            </FormGroup>
          );
        })}
      </FormControl>
    );
  }
}
