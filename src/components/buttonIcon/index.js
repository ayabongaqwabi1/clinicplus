import React, { Component } from 'react';
import upload from '../../images/icons/upload.svg';
import download from '../../images/icons/download.svg';
import plus from '../../images/icons/add.svg';
import plusWhite from '../../images/icons/add-white.svg';
import services from '../../images/icons/services.svg';
import calendar from '../../images/icons/calendar.svg';
import settings from '../../images/icons/settings.svg';
import location from '../../images/icons/location.svg';
import users from '../../images/icons/users.svg';
import addUser from '../../images/icons/add-user.svg';
import bell from '../../images/icons/bell.svg';
import dash from '../../images/icons/dash.svg';
import selectDate from '../../images/icons/date.svg';
import email from '../../images/icons/email.svg';
import trash from '../../images/icons/trash.svg'
import pencil from '../../images/icons/edit.svg';
import info from '../../images/icons/help.svg';
import file from '../../images/icons/file.svg';
import search from '../../images/icons/search.svg';

const icons ={
    trash, 
    pencil,
    info,
    upload,
    download,
    plus,
    plusWhite,
    services,
    calendar,
    settings,
    location,
    users,
    addUser,
    bell,
    dash,
    selectDate,
    email,
    file,
    search,
}

class Icon extends Component {
    render(){
        const { name } = this.props;
        return <img className="btn-icon" src={icons[name]} alt={name} {...this.props} />
    }
}

export default Icon;