import React, { Component } from 'react';
import './style.scss';
import Drop from '../dropdown/index.js';
import PageButton from '../buttons/pageButton';
import InputSearch from '../inputSearch';
import { connect } from 'react-redux';
import { 
    getTodaysAppointments,
    getAppointmentsOnThisWeek,
    getAppointmentsOnLastWeek,
    getAppointmentsOnThisMonth,
    getAppointmentsOnThisYear,
    getTomorrowsAppointments
  } from '../../functions/appointmentFilter'
  import config from '../../config';
import { isEmpty, keys, trim } from 'ramda';
import Fuse from 'fuse.js';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment'


class _QuickSearch extends React.Component{
    constructor(){
        super();
        this.state = {
            customer: '',
            service: '',
            approved: '',
            date: new Date(),
        }
        this.set = this.set.bind(this);
        this.setDate = this.setDate.bind(this);
        this.search = this.search.bind(this);
    }

    set(e, id){
        const val = e.target.value;
        let state = this.state;
        state[id] = val;
        this.setState(state)
    }

    setDate(date){
        this.setState({ date })
    }

    search(){
        const appointments = this.props.state.dashboard.appointments;
        const options = {
            includeScore: true,
            keys: ['user.name', 'employees.medicalServices','location', 'approved.statusString', 'date']
          }
          
        const fuse = new Fuse(appointments, options)
        const { approved, customer, service, date, location } = this.state;
        const searchStr = `${location} ${approved} ${customer} ${service} ${date !== null ? moment(date).format("DD/MM/YYYY") : ''}`
        
        const result = fuse.search(searchStr)
        const resultAppointments = result.map(r => r.item)
        this.props.onFilter(isEmpty(trim(searchStr)) ? appointments : resultAppointments);
    }

    render(){
        const customers = this.props.state.dashboard.customers.map( c => ({key: c.name, value: c.name }))
        const locations = this.props.state.dashboard.locations.map( l => ({key: l, value: l }))
        const services = keys(config.services).map(s => ({key: s, value: config.services[s].title }))
        const status =[
            {
                key: "pending",
                value: "pending",
            },
            {
                key: "approved" ,
                value: "approved",
            },
            {
                key: "declined",
                value: "declined",
            }
        ]
        return (
            <div className="quicksearch-container">
                <PageButton onClick={this.search}>Quick Search</PageButton>
                <DatePicker 
                    selected={this.state.date} 
                    onChange={date => this.setDate(date)}
                    isClearable
                    placeholderText="Date"
                />
                <Drop title="Services" data={services} onChange={(e)=> this.set(e, 'service')}/>
                <Drop title="Customer" data={customers} onChange={(e)=> this.set(e, 'customer')} />
                <Drop title="Location" data={locations} onChange={(e)=> this.set(e, 'location')}/>
                <Drop title="Status" data={status} onChange={(e)=> this.set(e, 'approved')}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({ state })

const mapDispatchToProps = dispatch => ({ dispatch })

const QuickSearch = connect(mapStateToProps, mapDispatchToProps)(_QuickSearch);



class _LongSearch extends React.Component{
    constructor(){
        super();
        this.state = {
            query:''
        }
        this.set = this.set.bind(this);
        this.search = this.search.bind(this);
    }

    set(e, id){
        const val = e.target.value;
        let state = this.state;
        state[id] = val;
        this.setState(state)
    }

    search(){
        const items= this.props.data;
        const options = {
            includeScore: true,
            keys: this.props.searchKeys
          }
          
        const fuse = new Fuse(items, options)
        const { query} = this.state;
   
        const searchStr = `${query}`
        
        const result = fuse.search(searchStr)
        const resultData = result.map(r => r.item)
        if(isEmpty(searchStr)){
            this.props.onFilter(items);
        }
        else{
            this.props.onFilter(resultData);
        }
    }

    render(){
        return (
            <div className="longsearch-container">
                <InputSearch value={this.state.query} onChange={(e)=> {this.set(e, 'query'); this.search();}}/>
            </div>
        )
    }
}

const mapLongSearchStateToProps = state => ({ state })

const mapLongSearchDispatchToProps = dispatch => ({ dispatch })

const LongSearch = connect(mapLongSearchStateToProps, mapLongSearchDispatchToProps)(_LongSearch);



const ButtonBar = (props) => {
    const { onFilter, appointments } = props;

    const save = (id, data) =>{
        onFilter(id, data)
    }

    return (
        <div className="quicksearch-container">
            <PageButton 
                size="small" 
                onClick={()=> save("today", getTodaysAppointments(appointments))}
            >
                Today
            </PageButton>
            <PageButton 
                size="small" 
                onClick={()=> save("tommorow", getTomorrowsAppointments(appointments))}
            >
                Tommorow
            </PageButton>
            <PageButton 
                size="small" 
                onClick={()=> save("thisWeek", getAppointmentsOnThisWeek(appointments))}
            >
                This Week
            </PageButton>
            <PageButton 
                size="small" 
                onClick={()=> save("lastWeek", getAppointmentsOnLastWeek(appointments))}
            >
                Last Week
            </PageButton>
            <PageButton 
                size="small" 
                onClick={()=> save("thisMonth", getAppointmentsOnThisMonth(appointments))}
            >
                This Month
            </PageButton>
            <PageButton 
                size="small" 
                onClick={()=> save("thisYear", getAppointmentsOnThisYear(appointments))}
            >
                This Year
            </PageButton>
            <PageButton color="orange">Custom</PageButton>
            
        </div>
    )
}

class SearchBar extends Component {
    render(){
        const { type } = this.props;
        switch(type){
            case "buttons":
                return <ButtonBar {...this.props} />
                break;
            case "quick":
                return <QuickSearch {...this.props} />;
                break;
            case "long":
                return <LongSearch {...this.props}/>;
                break;
            default:
                return <QuickSearch  {...this.props}/>;
                break;
        }
    }
}

export default SearchBar;