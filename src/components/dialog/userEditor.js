import React from "react";
import { editUser } from "../../state/actions/user";
import { connect } from "react-redux";
import { pipe, filter, head, mergeAll } from "ramda";
import PageButton from "../buttons/pageButton";
import { saveUserEditToDb } from "../../functions/db";
import authConfig from "../../config/auth";

class UserEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
    };
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const { user } = this.props;
    const { _id, id, email, name, type } = user;
    this.setState({ _id, id, email, name, type });
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    let state = this.state;
    state[id] = value;
    state["done"] = null;
    state["errors"] = [];
    this.setState(state);
  }

  submit() {
    let state = this.state;
    state["done"] = false;
    state["errors"] = [];
    this.setState(state);
    const { dispatch } = this.props;
    const { _id, id, email, name, type } = this.state;
    const user = { _id, id, email, name, type, isDeleted: false };

    this.setState({ done: false });
    dispatch(editUser(user));

    saveUserEditToDb(user)
      .then((data) => this.setState({ done: true }))
      .catch((err) => {
        console.log(err);
        console.log(err.msg || err.response.data.msg || err.message);
        this.setState({
          done: true,
          errors: [
            {
              msg:
                err.msg ||
                err.response.data ||
                err.response.data.msg ||
                err.message,
            },
          ],
        });
      });

    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    let userTypes = ["doctor", "admin"];
    if (this.props.state.auth.type === "super") {
      userTypes.push("super");
    }
    const userType = this.state.type;
    const isAuthorizedToEdit = (type) => {
      const loggedInUserType = this.props.state.auth.type;
      const userAuth = authConfig["edit"][type];
      return userAuth.includes(loggedInUserType);
    };
    return (
      <div>
        <p className="label"> Full Name </p>
        <input
          type="text"
          onChange={this.changeInput("name")}
          value={this.state.name}
        />

        <p className="label"> Email </p>
        <input
          type="text"
          onChange={this.changeInput("email")}
          value={this.state.email}
        />

        <p className="label"> User type </p>
        <select onChange={this.changeInput("type")} value={this.state.type}>
          {userTypes.map((i) => {
            return (
              <option value={i} selected={i === this.state.types}>
                {i}
              </option>
            );
          })}
        </select>
        <br />
        {userType === "customer" && (
          <p className="danger-zone">
            <strong>Not Authorized</strong>
            <br />
            Please use the customers page to edit a customer
          </p>
        )}
        {userType !== " customer" && (
          <React.Fragment>
            {!isAuthorizedToEdit(userType) && (
              <p className="danger-zone">
                <strong>Danger zone</strong>
                <br />
                You don't have permission to Edit this user. Please contact{" "}
                <a href="https://facebook.com/midastouchsa">Tech Support.</a>
              </p>
            )}
            {isAuthorizedToEdit(userType) && (
              <React.Fragment>
                <br />
                <br />
                {userType !== "customer" && (
                  <PageButton color="orange" onClick={this.submit}>
                    Submit{" "}
                  </PageButton>
                )}
              </React.Fragment>
            )}
          </React.Fragment>
        )}
        <br />
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done!</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          return <p className="error">{err.msg}</p>;
        })}
        <br />
        <br />
      </div>
    );
  }
}
const getUser = (state, id) =>
  pipe(
    filter((user) => user.id === id),
    head
  )(state.dashboard.staff);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const cId = ownProps.data.id;
  const user = getUser(stateProps.state, cId);
  return mergeAll([stateProps, dispatchProps, ownProps, { user }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(UserEditor);
