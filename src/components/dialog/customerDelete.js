import React from "react";
import { connect } from "react-redux";
import { pipe, filter, mergeAll, head, assoc } from "ramda";
import PageButton from "../buttons/pageButton";
import { saveCustomerEditToDb, saveAppointmentEditToDb } from "../../functions/db";
import { deleteAppointment } from "../../state/actions/appointment";
class CustomerDelete extends React.Component {
  constructor() {
    super();
    this.state = {
      date: { focused: false },
      errors: [],
    };
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const { customer } = this.props;
    const { _id, id, name, surname, email, accountId, contactNumber } = customer;
    this.setState({ _id, id, name, surname, email, accountId, contactNumber });
  }

  submit() {
    const { _id, id, name, surname, email, accountId, contactNumber } = this.state;
    const customer = { _id, id, name, surname, email, accountId, contactNumber, isDeleted: true };

    this.setState({ done: false });
    saveCustomerEditToDb(customer)
      .then(() => {
        this.props.appointments.map(app => {
          const deletedApp = assoc("isDeleted", true, app);
          this.props.dispatch(deleteAppointment(deletedApp))
          saveAppointmentEditToDb(deletedApp)
        })
        this.setState({ done: true })
      })
      .catch((err) => {
        console.log(err);
        this.setState({ done: true, errors: [{ msg: err.message }] });
      });
    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
    if (this.props.close) {
      this.props.close();
    }
  }

  render() {
    return (
      <div>
        <p className="label2"> Name </p>
        <p className="info-par2">{this.state.name}</p>

        <p className="label2"> Surname </p>
        <p className="info-par2">{this.state.surname}</p>

        <p className="label2"> Email </p>
        <p className="info-par2">{this.state.email}</p>

        <p className="label2"> Contact Number </p>
        <p className="info-par2">{this.state.contactNumber} </p>

        <br />
        <br />

        <p className="danger-zone">
          <strong>Danger zone</strong>
          <br />
          You are about to delete this Customer! 
          <br />
          This will also delete all
          apppointments by this customer.
        </p>
        <br />
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done!.</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        <PageButton color="orange" onClick={this.submit}>
          Delete{" "}
        </PageButton>
      </div>
    );
  }
}

const getCustomer = (state, id) =>
  pipe(
    filter((user) => user.id === id),
    head
  )(state.dashboard.customers);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const cId = ownProps.data.id;
  const data = getCustomer(stateProps.state, cId);
  const account = stateProps.state.dashboard.accounts.filter(acc => acc.id === ownProps.data.accountId)[0]
  const appointments = stateProps.state.dashboard.appointments.filter(app => app.accountId === account.id)

  return mergeAll([stateProps, dispatchProps, ownProps, { account, appointments, customer:data }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CustomerDelete);
