import React from "react";
import moment from "moment";
import { deleteAppointment } from "../../state/actions/appointment";
import { connect } from "react-redux";
import { pipe, filter, head, mergeAll } from "ramda";
import PageButton from "../buttons/pageButton";
import { saveAppointmentEditToDb } from "../../functions/db";

class AppointmentDeletor extends React.Component {
  constructor() {
    super();
    this.state = {
      date: { focused: false },
      errors: [],
    };
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const { appointment } = this.props;
    const {
      _id,
      user,
      company,
      ndaUrl,
      approved,
      location,
      employees,
      id,
    } = appointment;
    const appointmentDate = moment(this.props.appointment.date, "DD/MM/YYYY");
    this.setState({
      _id,
      company,
      ndaUrl,
      id,
      approved,
      date: { focused: appointmentDate, selected: appointmentDate },
      user,
      location,
      employees,
    });
  }

  onDateChage(date) {
    this.setState({ date: { selected: date } });
    //this.props.answerQuestion("appointmentDate", { value: date });
  }

  onFocusChange(focused) {
    this.setState({ date: { focused } });
  }

  submit() {
    const { dispatch } = this.props;
    const {
      _id,
      user,
      company,
      ndaUrl,
      approved,
      location,
      employees,
      id,
    } = this.state;
    const deletedBy = this.props.state.auth;
    const appointment = {
      _id,
      user,
      company,
      ndaUrl,
      date: this.props.appointment.date,
      approved,
      location,
      employees,
      id,
      approved,
      isDeleted: true,
      deletedBy,
    };
    dispatch(deleteAppointment(appointment));
    saveAppointmentEditToDb(appointment);
    this.props.appendedAction();
    this.props.close();
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    return (
      <div>
        <p className="label"> Name </p>
        <p className="info-par">{this.state.user.name}</p>

        <p className="label"> Email </p>
        <p className="info-par">{this.state.user.email}</p>

        <p className="label"> Date </p>
        <p className="info-par">
          {this.state.date.selected.format("DD MMMM YYYY")}
        </p>

        <p className="label"> Company Name </p>
        <p className="info-par">{this.state.company.name} </p>

        <p className="label"> Location </p>
        <p className="info-par">{this.state.location} </p>

        <p className="label"> Employees </p>
        <ol>
          {this.state.employees.map((emp) => {
            return (
              <li>
                <small>{emp.name}</small>
              </li>
            );
          })}
        </ol>
        <br />
        <p className="danger-zone">
          <strong>Danger zone</strong>
          <br />
          You are about to delete this appointment!
        </p>
        <PageButton color="orange" onClick={this.submit}>
          Delete{" "}
        </PageButton>
      </div>
    );
  }
}

const getAppointmentById = (state, id) =>
  pipe(
    filter((ap) => ap.id === id),
    head
  )(state.dashboard.appointments);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const appId = ownProps.data.id;
  const appointment = getAppointmentById(stateProps.state, appId);
  return mergeAll([stateProps, dispatchProps, ownProps, { appointment }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AppointmentDeletor);
