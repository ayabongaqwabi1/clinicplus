import React from 'react';
import moment from "moment";
import { connect } from "react-redux";
import { pipe, filter, head, mergeAll, includes, assoc, isEmpty, isNil, last, replace} from 'ramda';
import { saveAppointmentEditToDb } from "../../functions/db";
import Axios from 'axios';
import fileDownload from 'js-file-download';

function download(url) {
  const filename = last(url.split('/'))
  Axios.get(replace("http", "https", url), {
    responseType: 'blob',
  }).then(res => {
    fileDownload(res.data, filename);
  });
}

const flags= {
    0: "Pending",
    1: "Approved",
    2: "Declined"
};

const flagColors= {
    0: "orange",
    1: "green",
    2: "tomato"
};

const getAuditText = num => flags[num]
const getFlagColor = num => flagColors[num]

class AppointmentInfo extends React.Component {
    constructor() {
        super();
        this.state = {
          date: { focused: false },
        };
        this.onDateChage = this.onDateChage.bind(this);
        this.onFocusChange = this.onFocusChange.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.save = this.save.bind(this);
        this.restore = this.restore.bind(this);
    }

    componentWillMount() {
        const{ appointment } = this.props;
        const { user, company, location, employees, id, approved, ndaUrl, deletedBy } = appointment;
        const today = moment(new Date());
        const appointmentDate = moment(this.props.appointment.date, "DD/MM/YYYY");
        this.setState({ id, approved, date: { focused: appointmentDate, selected: appointmentDate },
             user, company, location, employees, ndaUrl, deletedBy });
    }

    onDateChage(date) {
        this.setState({date: { selected: date }});
    }

    onFocusChange(focused) {
        this.setState({ date: { focused }});
    }
    
    changeInput(qId){
        return (e)=>{
            this.save(qId, e.target.value)
        }
    }

    save(id, value){
        const userKeys = ["name", "email","occupation", "idNo"]
        const isUserKey = (i) => includes(i, userKeys);
        if(isUserKey(id)){
            const { user } = this.state;
            const newUser = assoc(id, value, user)
            this.setState({ user: newUser})
        }
        else{
            let state = this.state;
            state[id] = value;
            this.setState(state)
        }
    }

    restore(){
        const { appointment } = this.props;
        const restoredAppointment = assoc("isDeleted", false, appointment)
        saveAppointmentEditToDb(restoredAppointment);
        this.props.appendedAction();
        this.props.close();
        if (this.props.refresh) {
        this.props.refresh();
        }
    }


    render(){
        return(
            <div> 
                {this.state.deletedBy &&
                    <div className="deletedby">
                        <p className="label2"> Deleted by </p>
                        <p className="info-par2">
                            {this.state.deletedBy.name}
                            <br />
                            {this.state.deletedBy.email}
                        </p>
                        <p className="label2"><strong>Status Audit</strong></p>
                        {!isEmpty(this.state.approved) && 
                            <p className="info-par2">
                                {this.state.approved.audit.map(
                                    (audit) => (
                                        <div>
                                            <p>
                                                <strong> 
                                                    Changed Status from 
                                                    <b className={getFlagColor(audit.from)}>
                                                        &nbsp; {getAuditText(audit.from)}
                                                    </b> to 
                                                    <b className={getFlagColor(audit.to)}>
                                                        &nbsp; {getAuditText(audit.to)}
                                                    </b>
                                                </strong></p>
                                            <p className="info-par2"><small><i>{audit.reason}</i></small></p>
                                        </div>
                                    )
                                )}
                            </p>
                        }
                    </div>
                }
                <p className="label2"> Name </p>
                <p className="info-par2">{this.state.user.name}</p>

                <p className="label2"> Email </p>
                <p className="info-par2">{this.state.user.email}</p>

                <p className="label2"> Date </p>
                <p className="info-par2">{this.state.date.selected.format("DD MMMM YYYY")}</p>
                
                <p className="label2"> Company Name </p>
                <p className="info-par2">{this.state.company.name} </p>

                <p className="label2"> Billing Email </p>
                {(isNil(this.props.account.email) || isEmpty(this.props.account.email)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.email) || !isEmpty(this.props.account.email)) && <p className="info-par2"> {this.props.account.email} </p>} 

                <p className="label2"> Invoice Email </p>
                {(isNil(this.props.account.invoiceEmail) || isEmpty(this.props.account.invoiceEmail)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.invoiceEmail) || !isEmpty(this.props.account.invoiceEmail)) && <p className="info-par2"> {this.props.account.invoiceEmail} </p>} 

                <p className="label2"> Registration Name </p>
                {(isNil(this.props.account.registrationName) || isEmpty(this.props.account.registrationName)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.registrationName) || !isEmpty(this.props.account.registrationName)) && <p className="info-par2"> {this.props.account.registrationName} </p>} 


                <p className="label2"> Registration Number</p>
                {(isNil(this.props.account.registrationNumber) || isEmpty(this.props.account.registrationNumber)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.registrationNumber) || !isEmpty(this.props.account.registrationNumber)) && <p className="info-par2"> {this.props.account.registrationNumber} </p>} 


                <p className="label2"> Trading Name</p>
                {(isNil(this.props.account.tradingName) || isEmpty(this.props.account.tradingName)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.tradingName) || !isEmpty(this.props.account.tradingName)) && <p className="info-par2"> {this.props.account.tradingName} </p>} 
                
                <p className="label2"> VAT </p>
                {(isNil(this.props.account.vat) || isEmpty(this.props.account.vat)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.vat) || !isEmpty(this.props.account.vat)) && <p className="info-par2"> {this.props.account.vat} </p>} 

                <p className="label2"> Physical Address</p>
                {(isNil(this.props.account.physicalAddress) || isEmpty(this.props.account.physicalAddress)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.physicalAddress) || !isEmpty(this.props.account.physicalAddress)) && <p className="info-par2"> {this.props.account.physicalAddress} </p>} 

                <p className="label2"> Postal Address</p>
                {(isNil(this.props.account.postalAddress) || isEmpty(this.props.account.postalAddress)) && <p className="info-par2"><span className="error">Customer did not specify</span></p>} 
                {(!isNil(this.props.account.postalAddress) || !isEmpty(this.props.account.postalAddress)) && <p className="info-par2"> {this.props.account.postalAddress} </p>} 

                <p className="label2"> Location </p>
                <p className="info-par2">{this.state.location} </p>

                { this.state.ndaUrl &&
                    <div>
                    <p className="label2"> Signed Non Disclosure Agreement </p>
                    <p className="info-par2"><a onClick={()=> download(this.state.ndaUrl)}> Download</a></p> 
                    </div>
                }
                
                <p className="label2"> Employees </p>
                <p className="info-par2">
                    <ol>
                        {this.state.employees.map( emp => {
                            return (<li><small>{emp.name}</small></li>)
                        })}
                    </ol>
                </p>
                <br />
                {this.props.customer.isDeleted &&
                    <div>
                        <small>In order to restore this appointment please restore customer {this.props.customer.id} in the customers tab.</small>
                    </div>
                }
                {!this.props.customer.isDeleted &&
                    <div>
                        <a onClick={this.restore}>Restore Appointment</a>
                    </div>
                }
            </div>
        )
    
    }
}
const getAppointmentById = (state, id) => 
    pipe(
        filter(ap => ap.id === id),
        head
    )(state.dashboard.appointments)

const getAccountById = (state, id) => 
    pipe(
        filter(ac => ac.id === id),
        head
    )(state.dashboard.accounts)

const mapStateToProps = (state) => ({
    state,
  });
  
const mapDispatchToProps = (dispatch) => ({
    dispatch
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const appId = ownProps.data.id;
    const appointment = getAppointmentById(stateProps.state, appId)
    const account = getAccountById(stateProps.state, appointment.accountId)
    const customer = stateProps.state.dashboard.customers.filter(customer=> customer.accountId === account.id)[0]
    return mergeAll([stateProps, dispatchProps, ownProps, { appointment, account, customer }])
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(AppointmentInfo);
