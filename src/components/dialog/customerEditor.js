import React from "react";
import { editCustomer } from "../../state/actions/customer";
import { connect } from "react-redux";
import { head, filter, pipe, mergeAll } from "ramda";
import PageButton from "../buttons/pageButton";
import { saveCustomerEditToDb } from "../../functions/db";

class CustomerEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
    };
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const { data } = this.props;
    const { _id, id, name, surname, email, accountId, contactNumber } = data;
    this.setState({ _id, id, name, surname, email, accountId, contactNumber });
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    let state = this.state;
    state[id] = value;
    state["done"] = null;
    state["errors"] = [];
    this.setState(state);
  }

  submit() {
    const { dispatch } = this.props;
    const { _id, id, name, surname, email, accountId, contactNumber } = this.state;
    const customer = {
      _id,
      id,
      name,
      surname,
      email,
      contactNumber,
      isDeleted: false,
      accountId
    };
    this.setState({ done: false });
    saveCustomerEditToDb(customer)
      .then((data) => this.setState({ done: true }))
      .catch((err) => {
        console.log(err);
        this.setState({ done: true, errors: [{ msg: err.message }] });
      });
    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    const appointmentIds = this.props.state.dashboard.appointments.map(
      (ap) => ap.id
    );
    return (
      <div>
        <p className="label"> Name </p>
        <input
          type="text"
          onChange={this.changeInput("name")}
          value={this.state.name}
        />

        <p className="label"> Surname </p>
        <input
          type="text"
          onChange={this.changeInput("surname")}
          value={this.state.surname}
        />

        <p className="label"> Email </p>
        <input
          type="text"
          onChange={this.changeInput("email")}
          value={this.state.email}
        />

        <p className="label"> Contact Number </p>
        <input
          type="text"
          onChange={this.changeInput("contactNumber")}
          value={this.state.contactNumber}
        />

        <br />
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done!.</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          console.log(err);
          return <p className="error">{err.msg}</p>;
        })}
        <br />
        <br />
        <PageButton color="orange" onClick={this.submit}>
          Submit{" "}
        </PageButton>
      </div>
    );
  }
}

const getCustomer = (state, id) =>
  pipe(
    filter((user) => user.id === id),
    head
  )(state.dashboard.customers);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const cId = ownProps.data.id;
  const data = getCustomer(stateProps.state, cId);
  return mergeAll([stateProps, dispatchProps, ownProps, { data }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CustomerEditor);
