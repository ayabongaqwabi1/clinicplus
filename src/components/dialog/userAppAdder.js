import React from "react";
import { DayPickerSingleDateController } from "react-dates";
import { addAppointment } from "../../state/actions/appointment";
import { connect } from "react-redux";
import moment from "moment";
import {
  mergeAll,
  includes,
  assoc,
  append,
  isEmpty,
} from "ramda";
import PageButton from "../buttons/pageButton";
import SimpleEmployeeRow from "../SimpleEmployeeRow";
import { addAppointmentToUser } from "../../functions/db";
import "react-dates/initialize";
import "./appEditor.scss";
import axios from 'axios';
import loading from "../../images/loading2.gif";

function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result.toUpperCase();
}

const flags = {
  0: "Pending",
  1: "Approved",
  2: "Declined",
};

const flagColors = {
  0: "orange",
  1: "green",
  2: "tomato",
};

const getAuditText = (num) => flags[num];
const getFlagColor = (num) => flagColors[num];
class AppointmentEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
      id: "",
      approvalAudit: [],
      approved: 0,
      currentApprovalStatus: 0,
      date: { focused: "", selected: "" },
      user: {
        name: "",
        surname: "",
        email: "",
      },
      companyName: "",
      selectedClinic: "",
      employees: [],
      reason: "",
      registrationName: "",
      registrationNumber: "",
      tradingnName: "",
      vat: "",
      physicalAddress: "",
      postalAddress: "",
      invoiceEmail: "",
      bookingsEmail: "",
      billingEmail: "",
      billingCompany: "",
      medicalsCompany: "",
      orderNumber: "",
      companyContactPerson: "",
      contactNumber: "",
      ndaUrl: "",
      closedState: true,
    };
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.addChangeStatusReason = this.addChangeStatusReason.bind(this);
    this.saveReasonStatusChange = this.saveReasonStatusChange.bind(this);
    this.saveStatusChange = this.saveStatusChange.bind(this);
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
    this.deleteEmployee = this.deleteEmployee.bind(this);
    this.editEmployee = this.editEmployee.bind(this);
    this.addNewEmployee = this.addNewEmployee.bind(this);
    this.resetState = this.resetState.bind(this);
    this.onFileChange = this.onFileChange.bind(this);
    this.onFileUpload = this.onFileUpload.bind(this);
  }

  componentWillMount() {
    const { dummy, customer } = this.props;
    const orderNumber = makeid(8);
    const { name: medicalsCompany, email: bookingsEmail, companyContactPerson,} = dummy.company;
    this.setState({
       orderNumber, 
       closedState: false, 
       medicalsCompany, 
       companyContactPerson, 
       bookingsEmail, 
       selectedClinic: this.props.state.location[0],
       contactNumber: customer.contactNumber});
    this.resetState(false);
  }

  onFileChange = event => { 
    this.setState({ selectedFile: event.target.files[0] }); 
  }; 

  onFileUpload = () => { 
    return ()=>{ 
      this.setState({isUploading: true})
      const url = "https://api.cloudinary.com/v1_1/clinic-plus/raw/upload";
      const formData = new FormData(); 
      formData.append( 
        "file", 
        this.state.selectedFile, 
        this.state.selectedFile.name 
      ); 
      formData.append("upload_preset", "pwdsm6sz");
      axios({
        method: "POST",
        data: formData,
        headers: {'Content-Type': 'multipart/form-data' },
        url
      })
        .then((response) => {
          this.setState({isUploading: false, ndaUrl: response.data.url})
        })
        .then((data) => {
          
        });
    }
  }; 

  resetState(isClosed) {
    if (isClosed && !this.state.closedState) {
      this.setState({
        id: "",
        approvalAudit: [],
        approved: 0,
        currentApprovalStatus: 0,
        date: { focused: "", selected: "" },
        user: {
          name: "",
          surname: "",
          email: "",
        },
        selectedClinic: "",
        employees: [],
        bookingsEmail: "",
        medicalsCompany: "",
        orderNumber: "",
        companyContactPerson: "",
        ndaUrl: "",
        closedState: true,
      });
    }
  }

  addNewEmployee() {
    const employee = {
      id: "",
      name: "new employee",
      occupation: "",
      medicalServices: [],
    };
    const employees = append(employee, this.state.employees);
    this.setState({ employees });
  }

  addChangeStatusReason(e) {
    const reason = e.target.value;
    const statusChangeErrors = this.state.errors.filter(
      (err) => err.type === "statusChange"
    );
    const hasNoStatusChangeErrs = isEmpty(statusChangeErrors);
    const errors = hasNoStatusChangeErrs
      ? append(
          { msg: "Please save status change audit", type: "statusChange" },
          this.state.errors
        )
      : this.state.errors;
    const statusReasonChangeError = isEmpty(reason)
      ? "Please specify reason why you made this change"
      : null;
    this.setState({ reason, errors, statusReasonChangeError });
  }

  saveStatusChange(newValue) {
    if (newValue !== this.state.currentApprovalStatus) {
      const statusChangeErrors = this.state.errors.filter(
        (err) => err.type === "statusChange"
      );
      const hasNoStatusChangeErrs = isEmpty(statusChangeErrors);
      let errors = hasNoStatusChangeErrs
        ? append(
            { msg: "Please save status change audit", type: "statusChange" },
            this.state.errors
          )
        : this.state.errors;
      this.save("approved", newValue);
      const statusReasonChangeError =
        "Please specify reason why you made this change";
      this.setState({ errors, statusReasonChangeError, closedState: false });
    } else {
      const nonStatusChangeErrors = this.state.errors.filter(
        (err) => err.type !== "statusChange"
      );
      this.save("approved", this.state.currentApprovalStatus);
      const statusReasonChangeError = null;
      this.setState({
        errors: nonStatusChangeErrors,
        statusReasonChangeError,
        closedState: false,
      });
    }
  }

  saveReasonStatusChange() {
    const reason = this.state.reason;
    const approvalAudit = append(
      {
        from: this.state.currentApprovalStatus,
        to: this.state.approved,
        reason,
      },
      this.state.approvalAudit
    );
    const errors = this.state.errors;
    this.setState({
      approvalAudit,
      currentApprovalStatus: this.state.approved,
      errors: errors.filter((err) => err.type !== "statusChange"),
      closedState: false,
    });
  }

  onDateChage(date) {
    let state = this.state;
    state["date"]["selected"] = date;
    state["date"]["focused"] = date;
    this.setState(state);
  }

  onFocusChange(focused) {
    // do nothing
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    const userKeys = ["name", "email", "surname", "occupation", "idNo"];
    const isUserKey = (i) => includes(i, userKeys);
    if (isUserKey(id)) {
      const { user } = this.state;
      const newUser = assoc(id, value, user);
      this.setState({ user: newUser, closedState: false, done: null });
    } else {
      let state = this.state;
      state[id] = value;
      state["closedState"] = false;
      state["done"] = null;
      this.setState(state);
    }
  }

  deleteEmployee(employee) {
    const employees = this.state.employees.filter((e) => e.id !== employee.id);
    this.setState({ employees });
  }

  editEmployee(employee) {
    const employees = this.state.employees.filter((e) => e.id !== employee.id && e.name !== "new employee");
    const { id, name, occupation, medicalServices, jobfileUrl } = employee;
    const cleanEmployee = { id, name, occupation, medicalServices, jobfileUrl };
    this.setState({ employees: append(cleanEmployee, employees) });
  }

  submit() {
    const { dispatch, account } = this.props;
    const {
      date,
      selectedClinic,
      employees,
      bookingsEmail,
      medicalsCompany,
      orderNumber,
      companyContactPerson,
      contactNumber,
      ndaUrl,
    } = this.state;

    const strDate = date.selected.toISOString();
    const customer = assoc('contactNumber', contactNumber, this.props.customer);

    const company = {
      name: medicalsCompany,
      email: bookingsEmail,
      companyContactPerson,
    };

    const appointment = {
      user: customer,
      id: orderNumber,
      date: moment(new Date(strDate)).format("DD/MM/YYYY"),
      location: selectedClinic,
      company,
      employees,
      ndaUrl,
      approved: {
        status: 0,
        statusString: "pending",
        audit: [],
      },
      isDeleted: false,
      accountId: account.id
    };

    dispatch(addAppointment(appointment));
    
    this.setState({ done: false });
    addAppointmentToUser({ appointment })
      .then((data) => this.setState({ done: true }))
      .catch((err) => this.setState({ done: true, errors: [err] }));
    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
  }

  render() {
    if (this.props.isClosed) {
      this.resetState(this.props.isClosed);
    }
    return (
      <div>
        <br />
        <p className="label"> Email </p>
        <input
          type="text"
          onChange={this.changeInput("bookingsEmail")}
          value={this.state.bookingsEmail}
        />
        <p className="label"> Contact Number </p>
        <input
          type="text"
          onChange={this.changeInput("contactNumber")}
          value={this.state.contactNumber}
        />
        <p className="label"> Company requesting medicals </p>
        <input
          type="text"
          onChange={this.changeInput("medicalsCompany")}
          value={this.state.medicalsCompany}
        />
        <p className="label"> Company contact person </p>
        <input
          type="text"
          onChange={this.changeInput("companyContactPerson")}
          value={this.state.companyContactPerson}
        />
        <p className="label"> Date </p>
        <DayPickerSingleDateController
          numberOfMonths={1}
          hideKeyboardShortcutsPanel={true}
          onOutsideClick={(e) => {}}
          onPrevMonthClick={(e) => {}}
          onNextMonthClick={(e) => {}}
          onClick={(e) => {}}
          date={this.state.date.selected} // momentPropTypes.momentObj or null
          onDateChange={this.onDateChage} // PropTypes.func.isRequired
          focused={this.state.date.focused} // PropTypes.bool
          onFocusChange={this.onFocusChange}
          id="edit-date"
          key={this.props.data.user}
          isOutsideRange={(day) => {
            const today = moment();
            return today.diff(day, 'days') > 0;
          }}
        />

        <p className="label"> Order Number </p>
        <input
          type="text"
          onChange={this.changeInput("orderNumber")}
          value={this.state.orderNumber}
        />

        <p className="label"> Location </p>
        <select
          onChange={this.changeInput("selectedClinic")}
          value={this.state.selectedClinic}
        >
          {this.props.state.location.map((i) => {
            return (
              <option value={i} selected={i === this.state.selectedClinic}>
                {i}
              </option>
            );
          })}
        </select>
         
        <div>
          <p className="label">NDA</p>
          <input type="file" onChange={this.onFileChange} /> 
          <br />
          <br />
          <PageButton onClick={this.onFileUpload("jobfileUrl")}> 
              Upload! 
          </PageButton>
          {this.state.isUploading === true && <div className="login-loading">Uploading <img style={{width: "40px", marginTop:"-8px"}}src={loading} alt="loading"/></div>}
          {this.state.isUploading === false && <div className="login-loading">Done Uploading!</div>}
        </div>

        <p className="label"> Employees </p>
        <button onClick={this.addNewEmployee}> Add Employee </button>
        <ol>
          {this.state.employees.map((employee) => {
            return (
              <SimpleEmployeeRow
                delete={this.deleteEmployee}
                edit={this.editEmployee}
                {...employee}
              />
            );
          })}
        </ol>
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          return <p className="error">{err.msg}</p>;
        })}
        <PageButton color="orange" onClick={this.submit}>
          Submit
        </PageButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const dummy = stateProps.state.dashboard.appointments[0];
  const account = stateProps.state.dashboard.accounts[0];
  const customer = stateProps.state.dashboard.customers[0];
  console.log(dummy)
  return mergeAll([stateProps, dispatchProps, ownProps, { customer, account, dummy }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AppointmentEditor);
