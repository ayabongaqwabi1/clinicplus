import React from "react";
import { connect } from "react-redux";
import { CSVLink } from "react-csv";

const headers = [
  { label: "Id", key: "id" },
  { label: "Fullname", key: "fullname" },
  { label: "Email", key: "email" },
  { label: "AppointmentID", key: "appointment" },
];

class CustomerExporter extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
    };
  }

  render() {
    const csvReport = {
      data: this.props.state.dashboard.customers,
      headers: headers,
      filename: "Clinicplus_customers.csv",
    };
    return (
      <div>
        <p className="label">Export data to CSV </p>
        <CSVLink {...csvReport}>Download</CSVLink>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerExporter);
