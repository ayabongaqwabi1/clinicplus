import React from "react";
import { deleteUser } from "../../state/actions/user";
import { connect } from "react-redux";
import { pipe, filter, head, mergeAll } from "ramda";
import PageButton from "../buttons/pageButton";
import { saveUserEditToDb } from "../../functions/db";
import authConfig from "../../config/auth";

class userDelete extends React.Component {
  constructor() {
    super();
    this.state = {
      date: { focused: false },
      errors: [],
    };
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const { user } = this.props;
    const { _id, id, email, name, type } = user;
    this.setState({ _id, id, email, name, type });
  }

  submit() {
    const { dispatch } = this.props;
    const { _id, id, email, name, type } = this.state;
    const user = { _id, id, email, name, type, isDeleted: true };

    dispatch(deleteUser(id));
    saveUserEditToDb(user)
      .then((data) => this.setState({ done: true }))
      .catch((err) => {
        console.log(err);
        console.log(err.msg || err.response.data.msg || err.message);
        this.setState({
          done: true,
          errors: [
            {
              msg:
                err.msg ||
                err.response.data ||
                err.response.data.msg ||
                err.message,
            },
          ],
        });
      });

    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    const userType = this.state.type;
    const isAuthorizedToEdit = (type) => {
      const loggedInUserType = this.props.state.auth.type;
      const userAuth = authConfig["edit"][type];
      return userAuth.includes(loggedInUserType);
    };
    return (
      <div>
        <p className="label"> Name </p>
        <p className="info-par">{this.state.name}</p>

        <p className="label"> Email </p>
        <p className="info-par">{this.state.email}</p>

        <p className="label"> Type </p>
        <p className="info-par">{this.state.type}</p>

        <br />
        <br />

        {userType === "customer" && (
          <p className="danger-zone">
            <strong>Not Authorized</strong>
            <br />
            Please use the customers page to delete a customer
          </p>
        )}
        {userType !== " customer" && (
          <React.Fragment>
            {!isAuthorizedToEdit(userType) && (
              <p className="danger-zone">
                <strong>Danger zone</strong>
                <br />
                You don't have permission to Delete this user. Please contact{" "}
                <a href="https://facebook.com/midastouchsa">Tech Support.</a>
              </p>
            )}
            {isAuthorizedToEdit(userType) && (
              <React.Fragment>
                <br />
                <br />
                {userType !== "customer" && (
                  <PageButton color="orange" onClick={this.submit}>
                    Submit{" "}
                  </PageButton>
                )}
              </React.Fragment>
            )}
          </React.Fragment>
        )}
        <br />
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done! </h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          console.log("err", err);
          return err.msg.error ? (
            <p className="error">
              {
                <span>
                  {err.msg.error}, you can contact Tech support on this{" "}
                  <a href="https://facebook.com/midastouchsa">link</a>{" "}
                </span>
              }
            </p>
          ) : (
            <p className="error">{<span>{err.msg}</span>}</p>
          );
        })}
        <br />
        <br />
      </div>
    );
  }
}
const getUser = (state, id) =>
  pipe(
    filter((user) => user.id === id),
    head
  )(state.dashboard.staff);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const userId = ownProps.data.id;
  const user = getUser(stateProps.state, userId);
  return mergeAll([stateProps, dispatchProps, ownProps, { user }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(userDelete);
