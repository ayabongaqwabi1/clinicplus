import React from "react";
import { addCustomer } from "../../state/actions/customer";
import { connect } from "react-redux";
import PageButton from "../buttons/pageButton";
import { addCustomerToDb } from "../../functions/db";

class CustomerAdder extends React.Component {
  constructor() {
    super();
    this.state = {
      date: { focused: false },
      errors: [],
    };
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    this.setState({
      email: "",
      fullname: "",
      appointmentId: "",
    });
  }

  onDateChage(date) {
    this.setState({ date: { selected: date } });
  }

  onFocusChange(focused) {
    this.setState({ date: { focused } });
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    let state = this.state;
    state[id] = value;
    state["done"] = null;
    state["errors"] = [];
    this.setState(state);
  }

  submit() {
    const { dispatch } = this.props;
    const { fullname, email, appointmentId } = this.state;
    const customer = { fullname, email, appointmentId };
    this.setState({ done: false });
    addCustomerToDb(customer)
      .then((data) => this.setState({ done: true }))
      .catch((err) => {
        console.log(err);
        this.setState({ done: true, errors: [{ msg: err.message }] });
      });
    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    const appointmentIds = this.props.state.dashboard.appointments.map(
      (ap) => ap.id
    );
    return (
      <div>
        <p className="label"> Full Name </p>
        <input
          type="text"
          onChange={this.changeInput("fullname")}
          value={this.state.fullname}
        />

        <p className="label"> Email </p>
        <input
          type="text"
          onChange={this.changeInput("email")}
          value={this.state.email}
        />

        <p className="label"> Appointment </p>
        <select
          onChange={this.changeInput("appointmentId")}
          value={this.state.appointmentId}
        >
          <option value={""} selected={"" === this.state.appointmentId}>
            None
          </option>
          {appointmentIds.map((i) => {
            return (
              <option value={i} selected={i === this.state.appointmentId}>
                {i}
              </option>
            );
          })}
        </select>
        <br />
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done!.</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          console.log(err);
          return <p className="error">{err.msg}</p>;
        })}
        <br />
        <br />
        <PageButton color="orange" onClick={this.submit}>
          Submit{" "}
        </PageButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerAdder);
