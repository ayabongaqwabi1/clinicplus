import React from "react";
import { DayPickerSingleDateController } from "react-dates";
import moment from "moment";
import { editAppointment } from "../../state/actions/appointment";
import { connect } from "react-redux";
import {
  pipe,
  filter,
  head,
  mergeAll,
  includes,
  assoc,
  append,
  isEmpty,
  isNil,
} from "ramda";
import PageButton from "../buttons/pageButton";
import { SegmentedControl } from "segmented-control";
import SimpleEmployeeRow from "../SimpleEmployeeRow";
import { saveAppointmentEditToDb } from "../../functions/db";
import "react-dates/initialize";
import "./appEditor.scss";

const getSelected = (stateVal, item) => stateVal === item;

const flags = {
  0: "Pending",
  1: "Approved",
  2: "Declined",
};

const flagColors = {
  0: "orange",
  1: "green",
  2: "tomato",
};

const getAuditText = (num) => flags[num];
const getFlagColor = (num) => flagColors[num];
class AppointmentEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      date: { focused: false },
      errors: [],
    };
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.addChangeStatusReason = this.addChangeStatusReason.bind(this);
    this.saveReasonStatusChange = this.saveReasonStatusChange.bind(this);
    this.saveStatusChange = this.saveStatusChange.bind(this);
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
    this.deleteEmployee = this.deleteEmployee.bind(this);
    this.editEmployee = this.editEmployee.bind(this);
  }

  componentWillMount() {
    const { appointment } = this.props;
    const { user, company, location, employees, id, approved } = appointment;
    const appointmentDate = moment(this.props.appointment.date, "DD/MM/YYYY");
    this.setState({
      id,
      approvalAudit: approved.audit,
      approved: parseInt(approved.status, 10),
      currentApprovalStatus: approved.status,
      date: { focused: appointmentDate, selected: appointmentDate },
      user,
      companyName: company.name,
      location,
      employees,
      reason: "",
    });
  }

  addChangeStatusReason(e) {
    const reason = e.target.value;
    const statusChangeErrors = this.state.errors.filter(
      (err) => err.type === "statusChange"
    );
    const hasNoStatusChangeErrs = isEmpty(statusChangeErrors);
    const errors = hasNoStatusChangeErrs
      ? append(
          { msg: "Please save status change audit", type: "statusChange" },
          this.state.errors
        )
      : this.state.errors;
    const statusReasonChangeError = isEmpty(reason)
      ? "Pleas specify reason why you made this change"
      : null;
    this.setState({ reason, errors, statusReasonChangeError });
    
  }

  saveStatusChange(newValue) {
    if (newValue !== this.state.currentApprovalStatus) {
      const statusChangeErrors = this.state.errors.filter(
        (err) => err.type === "statusChange"
      );
      const hasNoStatusChangeErrs = isEmpty(statusChangeErrors);
      let errors = hasNoStatusChangeErrs
        ? append(
            { msg: "Please save status change audit", type: "statusChange" },
            this.state.errors
          )
        : this.state.errors;
      this.save("approved", newValue);
      const statusReasonChangeError =
        "Please specify reason why you made this change";
      this.setState({ errors, statusReasonChangeError });
    } else {
      const nonStatusChangeErrors = this.state.errors.filter(
        (err) => err.type !== "statusChange"
      );
      this.save("approved", this.state.currentApprovalStatus);
      const statusReasonChangeError = null;
      this.setState({ errors: nonStatusChangeErrors, statusReasonChangeError });
    }
  }

  saveReasonStatusChange() {
    const reason = this.state.reason;
    const approvalAudit = append(
      {
        from: this.state.currentApprovalStatus,
        to: this.state.approved,
        reason,
      },
      this.state.approvalAudit
    );
    const errors = this.state.errors;
    this.setState({
      approvalAudit,
      currentApprovalStatus: this.state.approved,
      errors: errors.filter((err) => err.type !== "statusChange"),
    });
  }

  onDateChage(date) {
    let state = this.state;
    state["date"]["selected"] = date;
    state["date"]["focused"] = date;
    this.setState(state);
  }

  onFocusChange(focused) {
    // do nothing
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    const userKeys = ["name", "surname", "email", "occupation", "idNo"];
    const isUserKey = (i) => includes(i, userKeys);
    if (isUserKey(id)) {
      const { user } = this.state;
      const newUser = assoc(id, value, user);
      this.setState({ user: newUser });
    } else {
      let state = this.state;
      state[id] = value;
      this.setState(state);
    }
  }

  deleteEmployee(employee) {
    const employees = this.state.employees.filter((e) => e.id !== employee.id);
    this.setState({ employees });
  }

  editEmployee(employee) {
    const employees = this.state.employees.filter((e) => e.id !== employee.id);
    const { id, name, occupation, medicalServices, jobfileUrl } = employee;
    const cleanEmployee = { id, name, occupation, medicalServices, jobfileUrl };
    this.setState({ employees: append(cleanEmployee, employees) });
  }

  submit() {
    const { dispatch } = this.props;
    const {
      user,
      employees,
      id,
      location,
      companyName,
      date,
      approved,
      approvalAudit,
    } = this.state;
    const strDate = date.selected.format("DD/MM/YYYY");
    const { _id, company } = this.props.appointment;
    const flags = {
      0: "Pending",
      1: "Approved",
      2: "Declined",
    };
    const appointment = {
      _id,
      user,
      id,
      date: strDate,
      location,
      company: assoc("name", companyName, company),
      employees,
      approved: {
        status: approved,
        audit: approvalAudit,
        statusString: flags[approved],
      },
    };
    dispatch(editAppointment(appointment));
    saveAppointmentEditToDb(appointment);
    this.props.appendedAction();
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    return (
      <div>
        <p className="label"> Name </p>
        <input
          type="text"
          onChange={this.changeInput("name")}
          value={this.state.user.name}
        />

        <p className="label"> Email </p>
        <input
          type="text"
          onChange={this.changeInput("email")}
          value={this.state.user.email}
        />

        <div className="status">
          <p className="label"> Status </p>
          <SegmentedControl
            name="oneDisabled"
            options={[
              {
                label: "Approved",
                value: 1,
                default: getSelected(this.state.approved, 1),
              },
              {
                label: "Pending",
                value: 0,
                default: getSelected(this.state.approved, 0),
              },
              {
                label: "Declined",
                value: 2,
                default: getSelected(this.state.approved, 2),
              },
            ]}
            setValue={(newValue) => this.saveStatusChange(newValue)}
            style={{ width: 400, color: getFlagColor(this.state.approved) }} // purple400
          />
          {!isEmpty(this.state.approvalAudit) && (
            <div>
              <p className="label"> Audit </p>
              <div>
                {this.state.approvalAudit.map((audit) => (
                  <div>
                    <p>
                      <strong>
                        Changed Status from
                        <b className={getFlagColor(audit.from)}>
                          &nbsp; {getAuditText(audit.from)}
                        </b>{" "}
                        to
                        <b className={getFlagColor(audit.to)}>
                          &nbsp; {getAuditText(audit.to)}
                        </b>
                      </strong>
                    </p>
                    <p>
                      <small>
                        <i>{audit.reason}</i>
                      </small>
                    </p>
                  </div>
                ))}
              </div>
            </div>
          )}
          {this.state.currentApprovalStatus !== this.state.approved && (
            <diV>
              <p className="label"> Reason for changing Status </p>
              <textarea
                rows="5"
                onChange={this.addChangeStatusReason}
              ></textarea>
              {!isNil(this.state.statusReasonChangeError) && (
                <p className="error">{this.state.statusReasonChangeError}</p>
              )}
              <PageButton
                color="orange"
                onClick={this.saveReasonStatusChange}
                disabled={!isNil(this.state.statusReasonChangeError)}
              >
                Save
              </PageButton>
            </diV>
          )}
        </div>

        <p className="label"> Date </p>
        <DayPickerSingleDateController
          numberOfMonths={1}
          hideKeyboardShortcutsPanel={true}
          onOutsideClick={(e) => {}}
          onPrevMonthClick={(e) => {}}
          onNextMonthClick={(e) => {}}
          onClick={(e) => {}}
          date={this.state.date.selected} // momentPropTypes.momentObj or null
          onDateChange={this.onDateChage} // PropTypes.func.isRequired
          focused={this.state.date.focused} // PropTypes.bool
          onFocusChange={this.onFocusChange}
          id="edit-date"
          key={this.props.data.user}
          isOutsideRange={(day) => {
            const today = moment();
            return today.diff(day, 'days') > 0;
          }}
        />

        <p className="label"> Company Name </p>
        <input
          type="text"
          onChange={this.changeInput("companyName")}
          value={this.state.companyName}
        />

        <p className="label"> Location </p>
        <select
          onChange={this.changeInput("location")}
          value={this.state.location}
        >
          {this.props.state.location.map((i) => {
            return (
              <option value={i} selected={i === this.state.location}>
                {i}
              </option>
            );
          })}
        </select>

        <p className="label"> Employees </p>
        <ol>
          {this.state.employees.map((employee) => {
            return (
              <SimpleEmployeeRow
                delete={this.deleteEmployee}
                edit={this.editEmployee}
                {...employee}
              />
            );
          })}
        </ol>
        <br />
        {this.state.errors.map((err) => {
          return <p className="error">{err.msg}</p>;
        })}
        <PageButton
          color="orange"
          onClick={this.submit}
          disabled={this.state.errors.length > 0}
        >
          Submit
        </PageButton>
      </div>
    );
  }
}
const getAppointmentById = (state, id) =>
  pipe(
    filter((ap) => ap.id === id),
    head
  )(state.dashboard.appointments);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const appId = ownProps.data.id;
  const appointment = getAppointmentById(stateProps.state, appId);
  return mergeAll([stateProps, dispatchProps, ownProps, { appointment }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AppointmentEditor);
