import React from "react";
import { addUser } from "../../state/actions/user";
import { connect } from "react-redux";
import { isEmpty, head, assoc, update } from "ramda";
import PageButton from "../buttons/pageButton";
import { addUserToDb } from "../../functions/db";

class UserAdder extends React.Component {
  constructor() {
    super();
    this.state = {
      date: { focused: false },
      errors: [],
    };
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    this.setState({
      email: "",
      name: "",
      type: "doctor",
      password: "",
      password2: "",
    });
  }

  onDateChage(date) {
    this.setState({ date: { selected: date } });
  }

  onFocusChange(focused) {
    this.setState({ date: { focused } });
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    let state = this.state;
    state[id] = value;
    state["done"] = null;
    state["errors"] = [];
    this.setState(state);
  }

  submit() {
    const { dispatch } = this.props;
    const { name, email, type, password, password2 } = this.state;
    if (password === password2) {
      const user = { name, email, type, password };
      dispatch(addUser(user));
      this.setState({ done: false });
      addUserToDb(user)
        .then((data) => this.setState({ done: true }))
        .catch((err) => {
          console.log(err);
          
          console.log(err.msg || err.response.data.msg || err.message);
          this.setState({
            done: true,
            errors: [
              {
                msg:
                  err.msg ||
                  err.response.data ||
                  err.response.data.msg ||
                  err.message,
              },
            ],
          });
        });
      if (this.props.appendedAction) {
        this.props.appendedAction();
      }
      if (this.props.refresh) {
        this.props.refresh();
      }
    } else {
      this.setState({
        errors: [{ msg: "Passwords don't match, please try again" }],
        password: "",
        password2: "",
      });
    }
  }

  render() {
    let userTypes = ["doctor", "admin"];
    if (this.props.state.auth.type === "super") {
      userTypes.push("super");
    }

    return (
      <div>
        <p className="label"> Full Name </p>
        <input
          type="text"
          onChange={this.changeInput("name")}
          value={this.state.name}
        />

        <p className="label"> Email </p>
        <input
          type="text"
          onChange={this.changeInput("email")}
          value={this.state.email}
        />

        <p className="label"> Password </p>
        <input
          type="password"
          onChange={this.changeInput("password")}
          value={this.state.password}
        />

        <p className="label"> Repeat Password </p>
        <input
          type="password"
          onChange={this.changeInput("password2")}
          value={this.state.password2}
        />

        <p className="label"> User type </p>
        <select onChange={this.changeInput("type")} value={this.state.type}>
          {userTypes.map((i) => {
            return (
              <option value={i} selected={i === this.state.types}>
                {i}
              </option>
            );
          })}
        </select>
        <br />
        <br />
        {this.state.done === false && (
          <div className="notification grey">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done!</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          console.log(err);
          return <p className="error">{err.msg}</p>;
        })}
        <br />
        <br />
        <PageButton color="orange" onClick={this.submit}>
          Submit{" "}
        </PageButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(UserAdder);
