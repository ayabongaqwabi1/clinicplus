import React from "react";
import { editCustomer } from "../../state/actions/customer";
import { connect } from "react-redux";
import { head, filter, pipe, mergeAll } from "ramda";
import PageButton from "../buttons/pageButton";
import { saveCustomerEditToDb, saveAccountEditToDb } from "../../functions/db";

class CustomerEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
    };
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillMount() {
    const { customer, account } = this.props;
    const { _id : customer_Id, id: customerId, email: customerEmail, name: customerName, surname: customerSurname } = customer;
    const { 
      _id: account_Id, 
      id: accountId,
      email,
      invoiceEmail,
      isDeleted,
      name,
      physicalAddress,
      postalAddress,
      registrationName,
      registrationNumber,
      tradingName,
      vat } = account;
    
    this.setState({
      accountId,
      customerId,
      customer_Id,
      customerEmail,
      customerName,
      customerSurname,
      account_Id,
      email,
      invoiceEmail,
      isDeleted,
      name,
      physicalAddress,
      postalAddress,
      registrationName,
      registrationNumber,
      tradingName,
      vat });
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    let state = this.state;
    state[id] = value;
    state["done"] = null;
    state["errors"] = [];
    this.setState(state);
  }

  submit() {
    const { dispatch } = this.props;
    const {
      customerId,
      customer_Id,
      customerEmail,
      customerName,
      customerSurname,
      account_Id,
      accountId,
      email,
      invoiceEmail,
      isDeleted,
      name,
      physicalAddress,
      postalAddress,
      registrationName,
      registrationNumber,
      tradingName,
      vat } = this.state;

    const account = {
       id: accountId,
      _id: account_Id,
      email,
      invoiceEmail,
      isDeleted,
      name,
      physicalAddress,
      postalAddress,
      registrationName,
      registrationNumber,
      tradingName,
      vat
    };

    const customer = {
      _id: customer_Id,
      id: customerId,
      name: customerName,
      surname: customerSurname,
      email: customerEmail,
      accountId,
      isDeleted: false,
    };

    this.setState({ done: false });
    saveCustomerEditToDb(customer)
      .then(() => {
        saveAccountEditToDb(account)
        .then(() => this.setState({ done: true }))
        .catch((err) => {
          console.log(err);
          this.setState({ done: true, errors: [{ msg: err.message }] });
        });
      })
      .catch((err) => {
        console.log(err);
        this.setState({ done: true, errors: [{ msg: err.message }] });
      });
    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
  }

  render() {
    return (
      <div>
        <h2> Details about you </h2>

        <section>
          <p className="label"> Name </p>
          <input
            type="text"
            onChange={this.changeInput("customerName")}
            value={this.state.customerName}
          />
          <p className="label"> Surname </p>
          <input
            type="text"
            onChange={this.changeInput("customerSurname")}
            value={this.state.customerSurname}
          />

          <p className="label"> Contact Email </p>
          <small>Please note this is not your login email</small>
          <input
            type="text"
            onChange={this.changeInput("customerEmail")}
            value={this.state.customerEmail}
          />
        </section>

        <h2> Details about your company </h2>
        <section>
          <p className="label"> Registration Name </p>
          <input
            type="text"
            onChange={this.changeInput("registrationName")}
            value={this.state.registrationName}
          />

          <p className="label"> Trading Name </p>
          <input
            type="text"
            onChange={this.changeInput("tradingName")}
            value={this.state.tradingName}
          />

          <p className="label"> Registration Number </p>
          <input
            type="text"
            onChange={this.changeInput("registrationNumber")}
            value={this.state.registrationNumber}
          />
          
          <p className="label"> VAT </p>
          <input
            type="text"
            onChange={this.changeInput("vat")}
            value={this.state.vat}
          />

          <p className="label"> Billing Email </p>
          <input
            type="text"
            onChange={this.changeInput("customerEmail")}
            value={this.state.customerEmail}
          />

          <p className="label"> Physical Address </p>
          <input
            type="text"
            onChange={this.changeInput("physicalAddress")}
            value={this.state.physicalAddress}
          />

          <p className="label"> Postal Address </p>
          <input
            type="text"
            onChange={this.changeInput("postalAddress")}
            value={this.state.postalAddress}
          />

        </section>
        
        <br />
        <br />
        <PageButton color="orange" onClick={this.submit}>
          Submit{" "}
        </PageButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const account = stateProps.state.dashboard.accounts[0];
  const customer = stateProps.state.dashboard.customers[0];
  return mergeAll([stateProps, dispatchProps, ownProps, { customer, account }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CustomerEditor);
