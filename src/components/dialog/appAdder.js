import React from "react";
import { DayPickerSingleDateController } from "react-dates";
import { addAppointment } from "../../state/actions/appointment";
import { connect } from "react-redux";
import moment from "moment";
import {
  pipe,
  filter,
  head,
  mergeAll,
  includes,
  assoc,
  append,
  isEmpty,
  isNil,
} from "ramda";
import PageButton from "../buttons/pageButton";
import { SegmentedControl } from "segmented-control";
import SimpleEmployeeRow from "../SimpleEmployeeRow";
import { addAppointmentToDb } from "../../functions/db";
import "react-dates/initialize";
import "./appEditor.scss";

const getSelected = (stateVal, item) => stateVal === item;

const flags = {
  0: "Pending",
  1: "Approved",
  2: "Declined",
};

const flagColors = {
  0: "orange",
  1: "green",
  2: "tomato",
};

const getAuditText = (num) => flags[num];
const getFlagColor = (num) => flagColors[num];
class AppointmentEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
      id: "",
      approvalAudit: [],
      approved: 0,
      currentApprovalStatus: 0,
      date: { focused: "", selected: "" },
      user: {
        name: "",
        surname: "",
        email: "",
      },
      signupPassword:"",
      companyName: "",
      selectedClinic: "",
      employees: [],
      reason: "",
      registrationName: "",
      registrationNumber: "",
      tradingnName: "",
      vat: "",
      physicalAddress: "",
      postalAddress: "",
      invoiceEmail: "",
      bookingsEmail: "",
      billingEmail: "",
      billingCompany: "",
      medicalsCompany: "",
      orderNumber: "",
      companyContactPerson: "",
      contactNumber: "",
      ndaUrl: "",
      closedState: true,
    };
    this.onDateChage = this.onDateChage.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.addChangeStatusReason = this.addChangeStatusReason.bind(this);
    this.saveReasonStatusChange = this.saveReasonStatusChange.bind(this);
    this.saveStatusChange = this.saveStatusChange.bind(this);
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.submit = this.submit.bind(this);
    this.deleteEmployee = this.deleteEmployee.bind(this);
    this.editEmployee = this.editEmployee.bind(this);
    this.addNewEmployee = this.addNewEmployee.bind(this);
    this.resetState = this.resetState.bind(this);
  }

  componentWillMount() {
    this.setState({ closedState: false });
    this.resetState(false);
  }

  resetState(isClosed) {
    if (isClosed && !this.state.closedState) {
      this.setState({
        id: "",
        approvalAudit: [],
        approved: 0,
        currentApprovalStatus: 0,
        date: { focused: "", selected: "" },
        user: {
          name: "",
          surname: "",
          email: "",
        },
        companyName: "",
        signupPassword: "",
        selectedClinic: "",
        employees: [],
        reason: "",
        registrationName: "",
        registrationNumber: "",
        tradingnName: "",
        vat: "",
        physicalAddress: "",
        postalAddress: "",
        invoiceEmail: "",
        bookingsEmail: "",
        billingEmail: "",
        billingCompany: "",
        medicalsCompany: "",
        orderNumber: "",
        companyContactPerson: "",
        contactNumber: "",
        ndaUrl: "",
        closedState: true,
      });
    }
  }

  addNewEmployee() {
    const employee = {
      id: "",
      name: "new employee",
      occupation: "",
      medicalServices: [],
    };
    const employees = append(employee, this.state.employees);
    this.setState({ employees });
  }

  addChangeStatusReason(e) {
    const reason = e.target.value;
    const statusChangeErrors = this.state.errors.filter(
      (err) => err.type === "statusChange"
    );
    const hasNoStatusChangeErrs = isEmpty(statusChangeErrors);
    const errors = hasNoStatusChangeErrs
      ? append(
          { msg: "Please save status change audit", type: "statusChange" },
          this.state.errors
        )
      : this.state.errors;
    const statusReasonChangeError = isEmpty(reason)
      ? "Please specify reason why you made this change"
      : null;
    this.setState({ reason, errors, statusReasonChangeError });
  }

  saveStatusChange(newValue) {
    if (newValue !== this.state.currentApprovalStatus) {
      const statusChangeErrors = this.state.errors.filter(
        (err) => err.type === "statusChange"
      );
      const hasNoStatusChangeErrs = isEmpty(statusChangeErrors);
      let errors = hasNoStatusChangeErrs
        ? append(
            { msg: "Please save status change audit", type: "statusChange" },
            this.state.errors
          )
        : this.state.errors;
      this.save("approved", newValue);
      const statusReasonChangeError =
        "Please specify reason why you made this change";
      this.setState({ errors, statusReasonChangeError, closedState: false });
    } else {
      const nonStatusChangeErrors = this.state.errors.filter(
        (err) => err.type !== "statusChange"
      );
      this.save("approved", this.state.currentApprovalStatus);
      const statusReasonChangeError = null;
      this.setState({
        errors: nonStatusChangeErrors,
        statusReasonChangeError,
        closedState: false,
      });
    }
  }

  saveReasonStatusChange() {
    const reason = this.state.reason;
    const approvalAudit = append(
      {
        from: this.state.currentApprovalStatus,
        to: this.state.approved,
        reason,
      },
      this.state.approvalAudit
    );
    const errors = this.state.errors;
    this.setState({
      approvalAudit,
      currentApprovalStatus: this.state.approved,
      errors: errors.filter((err) => err.type !== "statusChange"),
      closedState: false,
    });
  }

  onDateChage(date) {
    let state = this.state;
    state["date"]["selected"] = date;
    state["date"]["focused"] = date;
    this.setState(state);
  }

  onFocusChange(focused) {
    // do nothing
  }

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    const userKeys = ["name", "email", "surname", "occupation", "idNo"];
    const isUserKey = (i) => includes(i, userKeys);
    if (isUserKey(id)) {
      const { user } = this.state;
      const newUser = assoc(id, value, user);
      this.setState({ user: newUser, closedState: false, done: null });
    } else {
      let state = this.state;
      state[id] = value;
      state["closedState"] = false;
      state["done"] = null;
      this.setState(state);
    }
  }

  deleteEmployee(employee) {
    const employees = this.state.employees.filter((e) => e.id !== employee.id);
    this.setState({ employees });
  }

  editEmployee(employee) {
    const employees = this.state.employees.filter((e) => e.id !== employee.id);
    const { id, name, occupation, medicalServices, jobfileUrl } = employee;
    const cleanEmployee = { id, name, occupation, medicalServices, jobfileUrl };
    this.setState({ employees: append(cleanEmployee, employees) });
  }

  submit() {
    const { dispatch } = this.props;
    const {
      id,
      approvalAudit,
      approved,
      date,
      user,
      companyName,
      selectedClinic,
      location,
      employees,
      reason,
      registrationName,
      registrationNumber,
      tradingnName,
      vat,
      physicalAddress,
      postalAddress,
      invoiceEmail,
      bookingsEmail,
      billingEmail,
      billingCompany,
      medicalsCompany,
      orderNumber,
      companyContactPerson,
      contactNumber,
      ndaUrl,
    } = this.state;
    const strDate = date.selected.toISOString();
    const flags = {
      0: "Pending",
      1: "Approved",
      2: "Declined",
    };
    const company = {
      name: medicalsCompany,
      email: bookingsEmail,
      companyContactPerson,
    };
    const appointment = {
      user,
      id: orderNumber,
      date: moment(new Date(strDate)).format("DD/MM/YYYY"),
      location: selectedClinic,
      company,
      employees,
      ndaUrl,
      approved: {
        status: approved,
        audit: approvalAudit,
        statusString: flags[approved],
      },
      isDeleted: false,
    };
    dispatch(addAppointment(appointment));
    const { name, email, surname } = this.state.user;
    const ap = {
      id,
      name,
      surname,
      email,
      approvalAudit,
      approved,
      selectedClinic,
      appointmentDate: strDate,
      user,
      companyName,
      location,
      employees,
      reason,
      registrationName,
      registrationNumber,
      tradingnName,
      vat,
      physicalAddress,
      postalAddress,
      invoiceEmail,
      bookingsEmail,
      billingEmail,
      billingCompany,
      medicalsCompany,
      orderNumber,
      companyContactPerson,
      contactNumber,
      ndaUrl,
      isDeleted: false,
    };
    this.setState({ done: false });
    addAppointmentToDb(ap)
      .then((data) => this.setState({ done: true }))
      .catch((err) => this.setState({ done: true, errors: [err] }));
    if (this.props.appendedAction) {
      this.props.appendedAction();
      this.props.refresh();
    }
  }

  render() {
    if (this.props.isClosed) {
      this.resetState(this.props.isClosed);
    }
    return (
      <div>
        <br />
        <h2> User </h2>
        <section>
          <p className="label"> Name </p>
          <input
            type="text"
            onChange={this.changeInput("name")}
            value={this.state.user.name}
          />

          <p className="label"> Surname </p>
          <input
            type="text"
            onChange={this.changeInput("surname")}
            value={this.state.user.surname}
          />

          <p className="label"> Email </p>
          <input
            type="text"
            onChange={this.changeInput("email")}
            value={this.state.user.email}
          />
          <p className="label"> Password </p>
          <input
            type="text"
            onChange={this.changeInput("signupPassword")}
            value={this.state.signupPassword}
          />
        </section>
        <br />
        <h2> Company </h2>
        <section>
          <p className="label"> Registration Name </p>
          <input
            type="text"
            onChange={this.changeInput("registrationName")}
            value={this.state.registrationName}
          />

          <p className="label"> Registration Number </p>
          <input
            type="text"
            onChange={this.changeInput("registrationNumber")}
            value={this.state.user.registrationNumber}
          />

          <p className="label"> Trading Name </p>
          <input
            type="text"
            onChange={this.changeInput("tradingName")}
            value={this.state.tradingName}
          />

          <p className="label"> Vat Registration </p>
          <input
            type="text"
            onChange={this.changeInput("vat")}
            value={this.state.vat}
          />

          <p className="label"> Physical Addres </p>
          <textarea
            onChange={this.changeInput("physicalAddress")}
            value={this.state.physicalAddress}
          />

          <p className="label"> Postal Addres </p>
          <textarea
            onChange={this.changeInput("postalAddress")}
            value={this.state.postalAddress}
          />
        </section>

        <h2> Booking </h2>
        <section>
          <p className="label"> Billing Email </p>
          <input
            type="text"
            onChange={this.changeInput("billingEmail")}
            value={this.state.billingEmail}
          />

          <p className="label"> Invoice Email </p>
          <input
            type="text"
            onChange={this.changeInput("invoiceEmail")}
            value={this.state.invoiceEmail}
          />

          <p className="label"> Bookings Email </p>
          <input
            type="text"
            onChange={this.changeInput("bookingsEmail")}
            value={this.state.bookingsEmail}
          />

          <p className="label"> Company to be billed </p>
          <input
            type="text"
            onChange={this.changeInput("billingCompany")}
            value={this.state.billingCompany}
          />

          <p className="label"> Company requesting medicals </p>
          <input
            type="text"
            onChange={this.changeInput("medicalsCompany")}
            value={this.state.medicalsCompany}
          />

          <p className="label"> Company contact person </p>
          <input
            type="text"
            onChange={this.changeInput("companyContactPerson")}
            value={this.state.companyContactPerson}
          />

          <p className="label"> Contact Number </p>
          <input
            type="text"
            onChange={this.changeInput("contactNumber")}
            value={this.state.contactNumber}
          />
        </section>
        <div className="status">
          <p className="label"> Status </p>
          <SegmentedControl
            name="oneDisabled"
            options={[
              {
                label: "Approved",
                value: 1,
                default: getSelected(this.state.approved, 1),
              },
              {
                label: "Pending",
                value: 0,
                default: getSelected(this.state.approved, 0),
              },
              {
                label: "Declined",
                value: 2,
                default: getSelected(this.state.approved, 2),
              },
            ]}
            setValue={(newValue) => this.saveStatusChange(newValue)}
            style={{ width: 400, color: getFlagColor(this.state.approved) }} // purple400
          />
          {!isEmpty(this.state.approvalAudit) && (
            <div>
              <p className="label"> Audit </p>
              <div>
                {this.state.approvalAudit.map((audit) => (
                  <div>
                    <p>
                      <strong>
                        Changed Status from
                        <b className={getFlagColor(audit.from)}>
                          &nbsp; {getAuditText(audit.from)}
                        </b>{" "}
                        to
                        <b className={getFlagColor(audit.to)}>
                          &nbsp; {getAuditText(audit.to)}
                        </b>
                      </strong>
                    </p>
                    <p>
                      <small>
                        <i>{audit.reason}</i>
                      </small>
                    </p>
                  </div>
                ))}
              </div>
            </div>
          )}
          {this.state.currentApprovalStatus !== this.state.approved && (
            <diV>
              <p className="label"> Reason for changing Status </p>
              <textarea
                rows="5"
                onChange={this.addChangeStatusReason}
              ></textarea>
              {!isNil(this.state.statusReasonChangeError) && (
                <p className="error">{this.state.statusReasonChangeError}</p>
              )}
              <PageButton
                color="orange"
                onClick={this.saveReasonStatusChange}
                disabled={!isNil(this.state.statusReasonChangeError)}
              >
                Save
              </PageButton>
            </diV>
          )}
        </div>

        <p className="label"> Date </p>
        <DayPickerSingleDateController
          numberOfMonths={1}
          hideKeyboardShortcutsPanel={true}
          onOutsideClick={(e) => {}}
          onPrevMonthClick={(e) => {}}
          onNextMonthClick={(e) => {}}
          onClick={(e) => {}}
          date={this.state.date.selected} // momentPropTypes.momentObj or null
          onDateChange={this.onDateChage} // PropTypes.func.isRequired
          focused={this.state.date.focused} // PropTypes.bool
          onFocusChange={this.onFocusChange}
          id="edit-date"
          key={this.props.data.user}
          isOutsideRange={(day) => {
            const today = moment();
            return today.diff(day, 'days') > 0;
          }}
        />

        <p className="label"> Order Number </p>
        <input
          type="text"
          onChange={this.changeInput("orderNumber")}
          value={this.state.orderNumber}
        />

        <p className="label"> Location </p>
        <select
          onChange={this.changeInput("selectedClinic")}
          value={this.state.selectedClinic}
        >
          {this.props.state.location.map((i) => {
            return (
              <option value={i} selected={i === this.state.selectedClinic}>
                {i}
              </option>
            );
          })}
        </select>

        <p className="label"> Employees </p>
        <button onClick={this.addNewEmployee}> Add Employee </button>
        <ol>
          {this.state.employees.map((employee) => {
            return (
              <SimpleEmployeeRow
                delete={this.deleteEmployee}
                edit={this.editEmployee}
                {...employee}
              />
            );
          })}
        </ol>
        <br />
        {this.state.done === false && (
          <div className="notification">
            <h2>Saving...</h2>
          </div>
        )}
        {this.state.done && (
          <div className="notification">
            <h2>Done</h2>
            <small>Please refresh</small>
          </div>
        )}
        <br />
        {this.state.errors.map((err) => {
          return <p className="error">{err.msg}</p>;
        })}
        <PageButton color="orange" onClick={this.submit}>
          Submit
        </PageButton>
      </div>
    );
  }
}
const getAppointmentById = (state, id) =>
  pipe(
    filter((ap) => ap.id === id),
    head
  )(state.dashboard.appointments);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const appId = ownProps.data.id;
  const appointment = getAppointmentById(stateProps.state, appId);
  
  return mergeAll([stateProps, dispatchProps, ownProps, { appointment }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AppointmentEditor);
