import React from "react";
import { connect } from "react-redux";
import { pipe, filter, mergeAll, assoc } from "ramda";
import { saveCustomerEditToDb, saveAppointmentEditToDb } from "../../functions/db";
class CustomerInfo extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.restore = this.restore.bind(this);
  }

  componentWillMount() {
    const { customer } = this.props;
    const { _id, id, email, name , surname, contactNumber, accountId} = customer;
    this.setState({ _id, id, email, name, surname, contactNumber, accountId });
  }

  restore() {
    const { _id, id, name, surname, email, accountId, contactNumber } = this.state;
    const customer = { _id, id, name, surname, email, accountId, contactNumber, isDeleted: false };

    this.setState({ done: false });
    saveCustomerEditToDb(customer)
      .then(() => {
        this.props.appointments.map(app => {
          const deletedApp = assoc("isDeleted", false, app);
          saveAppointmentEditToDb(deletedApp)
        })
        this.setState({ done: true })
      })
      .catch((err) => {
        console.log(err);
        this.setState({ done: true, errors: [{ msg: err.message }] });
      });
    if (this.props.appendedAction) {
      this.props.appendedAction();
    }
    if (this.props.refresh) {
      this.props.refresh();
    }
    if (this.props.close) {
      this.props.close();
    }
  }

  render() {
    return (
      <div>
        <p className="label2"> Name </p>
        <p className="info-par2">{this.state.name}</p>

        <p className="label2"> Surname </p>
        <p className="info-par2">{this.state.surname}</p>

        <p className="label2"> Email </p>
        <p className="info-par2">{this.state.email}</p>

        <p className="label2"> Contact Number </p>
        <p className="info-par2">{this.state.contactNumber}</p>

        <p className="label2"> Customer Id </p>
        <p className="info-par2">{this.state.id} </p>

        <h2>Appointments</h2>

        {this.props.appointments.map(app => {
          return (
            <div>
              <p className="label2"> {app.id} </p>
              <p className="info-par2">{app.employees.length} Employees </p>
            </div>
          )
        })}

        <h2> Billing Details</h2>
        <p className="label2"> Billing Email </p>
        <p className="info-par2">{this.props.account.email} </p>

        <p className="label2"> Invoice Email </p>
        <p className="info-par2">{this.props.account.invoiceEmail} </p>

        <p className="label2"> Company </p>
        <p className="info-par2">{this.props.account.tradingName} </p>

        <p className="label2"> Company VAT </p>
        <p className="info-par2">{this.props.account.vat} </p>
        <br />
        <br />
        {this.props.customer.isDeleted && 
          <div>
            <a onClick={this.restore}>Restore Customer</a>
            <br />
            <small>This will also restore all appointments by this customer</small>
          </div>
        }
      </div>
    );
  }
}
const getAppointmentsByCustomer = (state, id) =>
  pipe(filter((ap) => ap.user.idNo === id))(state.dashboard.appointments);

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const cId = ownProps.data.id;
  const account = stateProps.state.dashboard.accounts.filter(acc => acc.id === ownProps.data.accountId)[0]
  const appointments = stateProps.state.dashboard.appointments.filter(app => app.accountId === account.id)
  const customer = stateProps.state.dashboard.customers.filter(customer=> customer.id === cId)[0]
  return mergeAll([stateProps, dispatchProps, ownProps, { account, appointments, customer }]);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CustomerInfo);
