import React from "react";
import { connect } from "react-redux";
import "./style.scss";
import { isNil } from "ramda";
import AppointmentEditor from "./appEditor";
import AppointmentAdder from "./appAdder";
import AppointmentInfo from "./appInfo";
import AppointmentDeleter from "./appDelete";
import AppointmentExporter from "./appointmentExporter";

import PageButton from "../buttons/pageButton";

import CustomerEditor from "./customerEditor";
import CustomerAdder from "./customerAdder";
import CustomerDelete from "./customerDelete";
import CustomerInfo from "./customerInfo";
import CustomerExporter from "./customerExporter";

import ServiceInfo from "./serviceInfo";

import UserAdder from "./userAdder";
import UserEditor from "./userEditor";
import UserDelete from "./userDelete";

import AccountEditor from "./accountEditor";
import UserAppAdder from "./userAppAdder";

const _capitalize = (str, lower = false) =>
  (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, (match) =>
    match.toUpperCase()
  );
const capitalize = (str) =>
  _capitalize(str.replace(/([a-z0-9])([A-Z])/g, "$1 $2"));

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

const Dialog = ({ type, close, show, children }) => {
  const styleClass = show ? "show" : "hide";
  const diagClass = `dialog ${styleClass}`;
  return (
    <div key={type} className={diagClass}>
      <PageButton onClick={() => close()}>close</PageButton>
      <br />
      {children}
      <PageButton onClick={() => close()}>close</PageButton>
    </div>
  );
};

const creators = {
  add: {
    appointment: AppointmentAdder,
    "user_appointment": UserAppAdder,
    customer: CustomerAdder,
    user: UserAdder,
  },
  export: {
    appointments: AppointmentExporter,
    customers: CustomerExporter,
  },
  EDIT: {
    appointment: AppointmentEditor,
    customers: CustomerEditor,
    user: UserEditor,
    account: AccountEditor
  },
  INFO: {
    appointment: AppointmentInfo,
    customers: CustomerInfo,
    service: ServiceInfo,
  },
  DELETE: {
    appointment: AppointmentDeleter,
    customers: CustomerDelete,
    user: UserDelete,
  },
};

const TaskDialog = (props) => {
  const action = creators[props.task];
  const ContentCreator = action[props.type];
  return (
    <Dialog {...props}>
      <br />
      <br />
      <h1 style={{ fontSize: "28px" }}>
        {" "}
        {capitalize(props.task.toLowerCase())} {capitalize(props.type)}
      </h1>
      <br />
      <ContentCreator
        {...props}
        key={`${props.task}-for-${props.type}-${props.data.id}-${props.data._id}-${getRandomInt(0, 10000)}`}
        isClosed={!props.show}
      />
      <br />
      <br />
    </Dialog>
  );
};

class DashboardLayout extends React.Component {
  render() {
    return (
      <div>
        {this.props.task !== "default" && <TaskDialog {...this.props} />}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ state });
const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(DashboardLayout);
