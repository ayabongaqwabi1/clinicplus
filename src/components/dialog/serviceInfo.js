import React from 'react';
import { DayPickerSingleDateController } from "react-dates";
import moment from "moment";
import { deleteAppointment } from "../../state/actions/appointment";
import { deleteCustomer } from "../../state/actions/customer";
import { connect } from "react-redux";
import { pipe, filter, head, mergeAll, includes, assoc } from 'ramda';
import PageButton from '../buttons/pageButton';

class ServiceInfo extends React.Component {
    constructor() {
        super();
        this.state = { };
    }

    componentWillMount() {
        const { data } = this.props
        const { info, id, name } = data;
        this.setState({ info, id, name });
    }

    render(){
        const { appointments } = this.props;
        return(
            <div> 
                <p className="info-par">{this.state.name}</p>
                <br />
                <p className="info-par">{this.state.info}</p>
                <br />
                <p>We have <strong>{appointments.length}</strong> {appointments.length > 1 ? "appointments" : "appointment"} for this service </p>
                {appointments.map(appointment => (
                    <div className="greyish">
                        <p>
                            <span className="data-label">Employee </span>
                            <span className="data">{appointment.employee.name}</span>
                        </p>
                        <p>
                            <span className="data-label">Company </span>
                            <span className="data">{appointment.companyName}</span>
                        </p>
                        <p>
                            <span className="data-label">Date </span>
                            <span className="data">{appointment.date}</span>
                        </p>
                        <p>
                            <span className="data-label">Location </span>
                            <span className="data">{appointment.location}</span>
                        </p>
                    </div>
                ))}
            </div>
        )
    
    }
}
const getAppointmentsByService = (state, id) => {
    const { appointments } = state.dashboard;
    let employees = []
    appointments.map(appointment => {
        appointment.employees.map( employee => {
            const { companyName, location, date } = appointment;
            if(includes(id, employee.medicalServices)){
                const serviceEmployee = {
                    employee,
                    companyName,
                    location,
                    date
                }
                employees.push(serviceEmployee)
            }
        })
    })
    return employees;
}
    
const mapStateToProps = (state) => ({
    state,
  });
  
const mapDispatchToProps = (dispatch) => ({
    dispatch
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const sId = ownProps.data.id;
    const appointments = getAppointmentsByService(stateProps.state, sId)
    return mergeAll([stateProps, dispatchProps, ownProps, { appointments }])
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ServiceInfo);
