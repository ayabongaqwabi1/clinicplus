import React from "react";
import { connect } from "react-redux";
import { CSVLink } from "react-csv";
import { assoc, omit, pipe } from "ramda";
import moment from "moment";

const flags = {
  0: "Pending",
  1: "Approved",
  2: "Declined",
};

const headers = [
  { label: "Id", key: "id" },
  { label: "User", key: "user" },
  { label: "Date", key: "date" },
  { label: "Location", key: "location" },
  { label: "Company", key: "company" },
  { label: "Employee Count", key: "employees" },
  { label: "Status", key: "status" },
  { label: "NDA URL", key: "ndaUrl" },
  { label: "Deleted", key: "isDeleted" },
];

class AppointmentExporter extends React.Component {
  constructor() {
    super();
    this.state = {
      errors: [],
    };
  }

  render() {
    const simpleAppointments = this.props.state.dashboard.appointments.map(
      (ap) => {
        const { user, employees, date, company } = ap;
        return pipe(
          assoc("user", user.name),
          assoc("employees", employees.length),
          assoc("company", company.name),
          (item) => {
            const { approved } = item;
            const setText = (flagNumber) => flags[flagNumber];
            return assoc("status", setText(approved.status), item);
          },
          assoc("date", moment(date, "DD/MM/YYYY").format("DD/MM/YYYY")),
          omit(["_id", "services", "approved"])
        )(ap);
      }
    );
    const csvReport = {
      data: simpleAppointments,
      headers: headers,
      filename: "Clinicplus_appointments.csv",
    };
    return (
      <div>
        <p className="label">Export data to CSV </p>
        <CSVLink {...csvReport}>Download</CSVLink>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentExporter);
