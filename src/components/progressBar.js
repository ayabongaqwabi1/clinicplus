import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import DateRangeIcon from "@material-ui/icons/DateRange";
import AddLocationIcon from "@material-ui/icons/AddLocation";
import StepConnector from "@material-ui/core/StepConnector";
import { navigate } from "gatsby";

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)",
    },
  },
  completed: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)",
    },
  },
  line: {
    height: 6,
    border: 0,
    backgroundColor: "#fff",
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: "#fff",
    zIndex: 1,
    color: "#3d4142",
    width: 50,
    height: 50,
    display: "flex",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  active: {
    color: "#fff",
    backgroundImage:
      "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)",
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
  },
  completed: {
    color: "#fff",
    backgroundImage:
      "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)",
  },
});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <AddLocationIcon />,
    2: <GroupAddIcon />,
    3: <DateRangeIcon />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    {
      title: "Clinic",
      to: "/book-now",
    },
    {
      title: "Service",
      to: "/services",
    },
    {
      title: "Date",
      to: "/date",
    },
  ];
}

export default function CustomizedSteppers({ step }) {
  const classes = useStyles();
  const steps = getSteps();
  const handleStep = (link) => {
    navigate(link);
  };

  return (
    <div className={classes.root}>
      <Stepper
        alternativeLabel
        activeStep={step}
        connector={<ColorlibConnector />}
      >
        {steps.map((s) => (
          <Step key={s.title} onClick={() => handleStep(s.to)}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{s.tite}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </div>
  );
}
