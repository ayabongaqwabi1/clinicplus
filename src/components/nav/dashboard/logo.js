import React from 'react';
import logo from '../../../images/logoWhite.svg';

const Logo = () => (
	<div>
		<a
			className="Logo-item"
			href="/"
		>
			<img src={logo} alt="Logo-1" />
		</a>
	</div>
);

export default Logo;
