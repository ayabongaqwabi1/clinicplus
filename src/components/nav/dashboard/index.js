import React from "react";
import { connect } from "react-redux";
import config from "../../../config";
import ClinicPlusLogo from "./logo";
import * as R from "ramda";
import "./style.scss";
import { Link } from "gatsby";

import Icon from "../../buttonIcon";

const NavLink = ({ title, icon, href }) => {
  return (
    <div className="nav-link">
      <div className="nav-link-icon">
        <Icon name={icon} />
      </div>
      <div className="nav-link-title">
        <Link to={href}>{title}</Link>
      </div>
    </div>
  );
};

const renderNavLink = (link) => {
  return <NavLink key={link.title} {...link} />;
};
const linksConfig = {
  customer: R.path(["userDashboardNavLinks"], config),
  admin: R.path(["dashboardNavLinks"], config),
  super: R.path(["dashboardNavLinks"], config),
  doctor: R.path(["docNavLinks"], config),
};

class DashboardNavigation extends React.Component {
  render() {
    const { type } = this.props.state.auth;
    const links = linksConfig[type];
    return (
      <div className="navigation">
        <div className="logo-container">
          <ClinicPlusLogo />
        </div>
        <div className="links-container">
          {links.map((link) => renderNavLink(link))}
        </div>
        <br />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ state });
const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardNavigation);
