import React, { Component } from 'react';
import './style.scss';

const Btn = (props) => {
    return <button disabled={props.disabled} {...props}>{props.children}</button>
}

const OrangeBtn = (props) =>{
    const {size } = props;
    const classes = size ==="small"  ? "small dash-btn orange-btn" : "dash-btn orange-btn"; 
    return <Btn  className={classes} {...props}>{props.children}</Btn>
}

const WhiteBtn = (props) =>{
    const {size } = props;
    const classes = size ==="small"  ? "small dash-btn white-btn" : "dash-btn white-btn"; 
    return <Btn className={classes} {...props}>{props.children}</Btn>
}

class PageButton extends Component {
    render(){
        const { color } = this.props;
        switch(color){
            case "orange":
                return <OrangeBtn {...this.props} />
                break;
            default:
                return <WhiteBtn {...this.props} />
                break;
        }
    }
}

export default PageButton;