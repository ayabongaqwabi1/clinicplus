import { isEmpty, keys, append, includes, last, replace} from "ramda";
import React, { Component } from "react";
import config from "../../config";
import "./style.scss";
import PageButton from "../buttons/pageButton";
import axios from 'axios';
import loading from "../../images/loading2.gif";
import Axios from 'axios';
import fileDownload from 'js-file-download';

function download(url) {
  const filename = last(url.split('/'))
  Axios.get(replace("http", "https", url), {
    responseType: 'blob',
  }).then(res => {
    fileDownload(res.data, filename);
  });
}
class Editor extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      name: "",
      occupation: "",
      medicalServices: [],
      saved: false,
    };
    this.changeInput = this.changeInput.bind(this);
    this.save = this.save.bind(this);
    this.isSelectedService = this.isSelectedService.bind(this);
    this.selectService = this.selectService.bind(this);
    this.saveEmployee = this.saveEmployee.bind(this);
    this.onFileUpload = this.onFileUpload.bind(this);
    this.onFileChange = this.onFileChange.bind(this);
  }

  componentWillMount() {
    const { id, name, occupation, medicalServices, jobfileUrl } = this.props;
    this.setState({ id, name, occupation, medicalServices, jobfileUrl });
  }

  onFileChange = event => { 
    this.setState({ selectedFile: event.target.files[0] }); 
  }; 

  onFileUpload = () => { 
    return ()=>{ 
      this.setState({isUploading: true})
      const url = "https://api.cloudinary.com/v1_1/clinic-plus/raw/upload";
      const formData = new FormData(); 
      formData.append( 
        "file", 
        this.state.selectedFile, 
        this.state.selectedFile.name 
      ); 
      formData.append("upload_preset", "pwdsm6sz");
      axios({
        method: "POST",
        data: formData,
        headers: {'Content-Type': 'multipart/form-data' },
        url
      })
        .then((response) => {
          this.setState({isUploading: false, jobfileUrl: response.data.url})
        })
        .then((data) => {
          
        });
    }
  }; 

  changeInput(qId) {
    return (e) => {
      this.save(qId, e.target.value);
    };
  }

  save(id, value) {
    let state = this.state;
    state[id] = value;
    state["saved"] = "false";
    this.setState(state);
  }

  saveEmployee() {
    console.log(this.state)
    const { id, name, occupation, medicalServices, jobfileUrl }  = this.state;
    const employee = { id, name, occupation, medicalServices, jobfileUrl };
    this.props.edit(employee);
    this.setState({ saved: true });
  }

  selectService(e) {
    const service = e.target.name;
    this.setState({
      medicalServices: append(service, this.state.medicalServices),
      saved: false,
    });
  }

  isSelectedService(service) {
    return includes(service, this.state.medicalServices);
  }

  render() {
    const { services } = config;
    return (
      <div>
        <p className="label"> Name </p>
        <input
          type="text"
          onChange={this.changeInput("name")}
          value={this.state.name}
        />

        <p className="label"> ID Number </p>
        <input
          type="text"
          onChange={this.changeInput("id")}
          value={this.state.id}
        />

        <p className="label"> Occupation </p>
        <input
          type="text"
          onChange={this.changeInput("occupation")}
          value={this.state.occupation}
        />
        {this.state.jobfileUrl && (
          <div>
            <p className="label"> Man Job Spec File </p>
            
            <a onClick={()=> download(this.state.jobfileUrl)}>
              {" "}
              Download
            </a>
            <br />
            <p>Upload new file</p>
            <div>
              <br />
              <input type="file" onChange={this.onFileChange} /> 
              <br />
              <br />
              <PageButton onClick={this.onFileUpload()}> 
                Upload
              </PageButton>
              {this.state.isUploading === true && <div className="login-loading">Uploading <img style={{width: "40px", marginTop:"-8px"}}src={loading} alt="loading"/></div>}
              {this.state.isUploading === false && <div className="login-loading">Done Uploading!</div>}
            </div>
          </div>
        )}
        {!this.state.jobfileUrl && (
            <div>
              <p className="label">Man Job Spec File</p>
              <input type="file" onChange={this.onFileChange} /> 
              <br />
              <br />
              <PageButton onClick={this.onFileUpload("jobfileUrl")}> 
                  Upload! 
              </PageButton>
              {this.state.isUploading === true && <div className="login-loading">Uploading <img style={{width: "40px", marginTop:"-8px"}}src={loading} alt="loading"/></div>}
              {this.state.isUploading === false && <div className="login-loading">Done Uploading!</div>}
            </div>
        )}
        <p className="label"> Medical Services </p>
        {keys(services).map((key) => {
          return (
            <div className="key-service-row" key={key}>
              <label htmlFor={key}>{services[key].title}</label>
              <input
                name={key}
                type="checkbox"
                checked={this.isSelectedService(key)}
                onChange={this.selectService}
              />
            </div>
          );
        })}
        <br />
        {!this.state.saved && (
          <p>
            <i>
              <b>You have not saved your changes yet</b>
            </i>
          </p>
        )}
        {this.state.saved && (
          <p>
            <b>Changes have been saved!</b>
          </p>
        )}
        <PageButton color="white" onClick={this.saveEmployee}>
          Save
        </PageButton>
      </div>
    );
  }
}

class Deleter extends Component {
  constructor() {
    super();
    this.state = {
      done: false,
    };
    this.deleteEmployee = this.deleteEmployee.bind(this);
  }

  deleteEmployee() {
    this.props.delete(this.props);
    this.setState({ done: true });
  }

  render() {
    return (
      <div>
        {this.state.done === false && (
          <div>
            <p> You are about to delete {this.props.name}</p>
            <p>Are you sure?</p>
            <PageButton color="white" onClick={this.deleteEmployee}>
              yes
            </PageButton>
            <PageButton color="white" onClick={this.props.close}>
              no
            </PageButton>
          </div>
        )}
        {this.state.done === true && (
          <div>
            <p> Employee has been deleted </p>
          </div>
        )}
      </div>
    );
  }
}

class SimpleEmployeeRow extends Component {
  constructor() {
    super();
    this.state = {
      activeClass: "",
      modalContent: "",
      modalTitle: "",
    };
    this.toggleActiveClass = this.toggleActiveClass.bind(this);
    this.setModal = this.setModal.bind(this);
  }

  toggleActiveClass() {
    if (isEmpty(this.state.activeClass)) {
      this.setState({ activeClass: "is-active" });
    } else {
      this.setState({ activeClass: "" });
    }
  }

  setModal(type) {
    if (type === "edit") {
      const modalContent = (
        <Editor
          close={this.toggleActiveClass}
          {...this.props}
          key={this.props.id}
        />
      );
      const modalTitle = "Edit Employee";
      this.setState({ modalTitle, modalContent });
    } else {
      const modalContent = (
        <Deleter
          close={this.toggleActiveClass}
          {...this.props}
          key={this.props.id}
        />
      );
      const modalTitle = "Delete Employee";
      this.setState({ modalTitle, modalContent });
    }
    this.toggleActiveClass();
  }

  render() {
    const { name } = this.props;
    const { modalContent, modalTitle } = this.state;
    return (
      <div className="key-employee-row">
        <p> {name} </p>
        <div className="actions">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              this.setModal("edit");
            }}
          >
            edit
          </a>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              this.setModal("delete");
            }}
          >
            delete
          </a>
        </div>

        <div class={`modal ${this.state.activeClass}`}>
          <div class="modal-background"></div>
          <div class="modal-card">
            <header class="modal-card-head">
              <p class="modal-card-title">{modalTitle}</p>
              <button
                class="delete"
                aria-label="close"
                onClick={this.toggleActiveClass}
              ></button>
            </header>
            <section class="modal-card-body">
              <div className="employee-form">
                <div className="questions">{modalContent}</div>
              </div>
            </section>
            <footer class="modal-card-foot">
              <button
                class="button is-warning"
                onClick={this.toggleActiveClass}
              >
                Done
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

export default SimpleEmployeeRow;
