import React from 'react';
import Icon from '../buttonIcon';
import './style.scss';
import { isNil } from 'ramda';
import DialogContext from '../layouts/context';

const ServiceCount = ({count, name, id, info, noCount, showInfo}) => {
    return(
        <DialogContext.Consumer>
            {(context) => {
                const { openDialog } = context;
                return( 
                    <div className="count-container">
                        <div className="icon-container">
                            <Icon name="file" />
                        </div>
                        <div className="text-container">
                            {noCount === false || isNil(noCount) ? <p className="count">{count}</p> :'' }
                            <p className={noCount ? "big name" : "name"}>{name}</p>
                            {showInfo  ?  <p className="info" onClick={() =>openDialog("INFO", "service", { name, id, info })}>Info</p> : ''}
                        </div>
                    </div>
                    )
            }}
        </DialogContext.Consumer>
        
    )
}

export default ServiceCount;