import React from "react";

import "./style.scss";

const Banner = ({ title, subtitle }) => (
  <section className="banner is-light">
    <div className="banner-body">
      <div className="container">
        <h1 className="title">{title}</h1>
        <h2 className="subtitle">{subtitle}</h2>
      </div>
    </div>
  </section>
);

export default Banner;
