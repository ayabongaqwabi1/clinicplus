import React from "react";

import "./style.scss";
import logo from "../images/logo.svg";
import { Link } from "gatsby";

const Navbar = () => (
  <div className="hero-head">
    <nav className="navbar">
      <div className="container">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <img src={logo} alt="Logo-1" />
          </a>
        </div>
        <div id="navbarMenuHeroA" className="navbar-menu">
          <div className="navbar-end">
            <Link to="login">Login</Link>
            <Link to="book-now">Register</Link>
          </div>
        </div>
      </div>
    </nav>
  </div>
);

export default Navbar;
