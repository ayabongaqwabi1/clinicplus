import React from "react";
import { connect } from "react-redux";
import "./style.scss";
import DashboardNavigation from "../nav/dashboard";
import DashHeader from "../headers/dashboard";
import { replace, trim, isEmpty, isNil } from "ramda";
import pageConfigs from "./config.js";
import Dialog from "../dialog";
import PageButton from "../buttons/pageButton";
import Icon from "../buttonIcon";
import DialogContext from "./context";
import * as R from "ramda";
import { dashboardStartup } from "../../state/actions/startup";
import { saveUser } from "../../state/actions/auth";
import { navigate } from "gatsby";
import ClinicPlusLogo from "../nav/dashboard/logo";
import loading from "../../images/loading2.gif";
import { refreshDbData } from "../../functions/db";

import {
  saveAppointment,
  hydrateAppointments,
} from "../../state/actions/appointment";
import { saveCustomers, saveStaff, saveAccounts } from "../../state/actions/startup";
import jwt from "json-web-token";
import { GATSBY_ACCESS_TOKEN_SECRET } from "../../config/access";
import Cookies from "js-cookie";

const exists = (i) => !isNil(i) && !isEmpty(i);

const PageHeader = ({ title }) => (
  <div className="dash-page-action-header">
    <h1>{title}</h1>
  </div>
);

const PageButtons = ({ buttons, openDialog }) => (
  <div className="dash-page-action-buttons">
    {buttons.map((btn) => {
      const { task, type, title, icon, color } = btn;
      return (
        <PageButton
          key={`${task + type}`}
          onClick={() => openDialog(task, type)}
          color={color}
        >
          <Icon name={icon} />
          {title}
        </PageButton>
      );
    })}
  </div>
);

const buildPageActionBar = (config, openDialog) => (
  <div className="page-action-center">
    <PageHeader title={config.title} />
    <PageButtons buttons={config.actionButtons} openDialog={openDialog} />
  </div>
);

class DashboardLayout extends React.Component {
  constructor() {
    super();
    this.openDialog = this.openDialog.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.state = {
      task: "default",
      type: "default",
      shown: false,
      refreshing: null,
    };
  }

  componentWillMount() {
    const { dispatch, state } = this.props;
    if (isNil(state.auth.type) || isEmpty(state.auth.type)) {
      const user = Cookies.getJSON("clinicplus_user");
      if (user) {
        dispatch(saveUser(user));
        this.refreshData(null, user);
      }
      dashboardStartup(state, dispatch);
    } else {
      this.setState({ refreshing: false });
    }
  }

  openDialog(task, type, ...data) {
    const appendedAction = R.last(data);
    const newData = R.omit(["appendedAction"], R.head(data));
    this.setState({ task, type, shown: true, data: newData, appendedAction });
  }

  refreshData(e, userData) {
    if (e) {
      e.preventDefault();
    }
    const { dispatch } = this.props;
    const user =
      isNil(userData) || isEmpty(userData) ? this.props.state.auth : userData;
    this.setState({ refreshing: true });
    refreshDbData(user).then((response) => {
      const data = jwt.decode(
        GATSBY_ACCESS_TOKEN_SECRET || process.env.GATSBY_ACCESS_TOKEN_SECRET,
        response.data
      ).value;
      if (
        user.type === "super" ||
        user.type === "doctor" ||
        user.type === "admin"
      ) {
        const { customers, appointments, users, accounts } = data;
        dispatch(hydrateAppointments(appointments));
        dispatch(saveAccounts(accounts));
        dispatch(saveCustomers(customers));
        dispatch(saveStaff(users));
        this.setState({ refreshing: false });
      } else {
        // must empty user appointment first - to add that in
        const { customer, appointments, account } = data;
        dispatch(hydrateAppointments(appointments));
        dispatch(saveAccounts([account]));
        dispatch(saveCustomers([customer]));
        this.setState({ refreshing: false });
        navigate("/dashboard/user-dashboard");
      }

      const { onRefresh } = this.props;
      if (onRefresh) {
        onRefresh();
      }
    });
  }

  closeDialog() {
    this.setState({ shown: false });
  }

  render() {
    const { location } = this.props;

    let page = location
      ? trim(replace("/dashboard/", "", location.pathname.toString()))
      : "";
    page = isEmpty(page) ? "default" : page;
    page = trim(replace("/", "", page));

    const pageActionConfig = pageConfigs[page];
    const { task, type, data, appendedAction } = this.state;
    const user = this.props.state.auth;
    const children = this.state.refreshing === false ? this.props.children : "";
    return (
      <DialogContext.Provider
        openDialog={this.openDialog}
        value={{ openDialog: this.openDialog }}
      >
        {exists(user.email) && this.state.refreshing === false && (
          <div className="dash-layout-main columns">
            <div className="dash-layout-sidebar column is-one-quarter">
              <DashboardNavigation />
            </div>
            <div className="dash-layout-page column is-three-quarters">
              <DashHeader refresh={this.refreshData} />
              {buildPageActionBar(pageActionConfig, this.openDialog)}
              {children}
              <div className="dialogs">
                <Dialog
                  task={task}
                  type={type}
                  show={this.state.shown}
                  close={() => this.closeDialog()}
                  data={data}
                  appendedAction={appendedAction}
                  refresh={this.refreshData}
                />
              </div>
            </div>
          </div>
        )}
        {exists(user.email) && this.state.refreshing === true && (
          <div className="dash-layout-main columns">
            <div className="dash-layout-sidebar column is-one-quarter">
              <DashboardNavigation />
            </div>
            <div className="dash-layout-page column is-three-quarters">
              <DashHeader refresh={this.refreshData} />
              {buildPageActionBar(pageActionConfig, this.openDialog)}
              <div className="refreshing">
                <h1>Please wait</h1>
                <br />
                <div className="login-loading">
                  <img src={loading} alt="loading" />
                </div>
              </div>
            </div>
          </div>
        )}
        {!exists(user.email) && (
          <div className="not-logged-in">
            <div className="login-logo">
              <ClinicPlusLogo />
            </div>
            <div className="login-info">
              <h1 className="is-uppercase is-size-3 brand-text-dark-red">
                {" "}
                You need to be logged in to view this page{" "}
              </h1>

              <p className="subtitle brand-text-color-grey is-size-5">
                ClinicPlus offers comprehensive Occupational Health Management
                and Consulting service to mines and industries. Our goal is to
                help our clients manage their occupational health and safety
                risks.
              </p>
              <PageButton color="orange" onClick={() => navigate("/book-now")}>
                {" "}
                Sign up{" "}
              </PageButton>
              <br />
              <PageButton onClick={() => navigate("/login")}>
                {" "}
                Login{" "}
              </PageButton>
            </div>
          </div>
        )}
      </DialogContext.Provider>
    );
  }
}

const mapStateToProps = (state) => ({ state });
const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(DashboardLayout);
