const exportCustomersButton = {
  title: "export csv",
  icon: "upload",
  task: "export",
  type: "customers",
  color: "white",
};

const exportAppointmentsButton = {
  title: "export csv",
  icon: "upload",
  task: "export",
  type: "appointments",
  color: "white",
};

const exportLocationsButton = {
  title: "export csv",
  icon: "upload",
  task: "export",
  type: "locations",
  color: "white",
};

const addCustomerButton = {
  title: "add customer",
  icon: "plusWhite",
  task: "add",
  type: "customer",
  color: "orange",
};

const addUserButton = {
  title: "add user",
  icon: "plusWhite",
  task: "add",
  type: "user",
  color: "orange",
};

const addServicesButton = {
  title: "add service",
  icon: "plusWhite",
  task: "add",
  type: "service",
  color: "orange",
};

const addAppointmentButton = {
  title: "add apointment",
  icon: "plusWhite",
  task: "add",
  type: "appointment",
  color: "orange",
};

const addLocationButton = {
  title: "add location",
  icon: "plusWhite",
  task: "add",
  type: "location",
  color: "orange",
};

const userAddAppointmentButton = {
  title: "add apointment",
  icon: "plusWhite",
  task: "add",
  type: "user_appointment",
  color: "orange",
};

const editProfileButton = {
  title: "Edit Profile",
  icon: "addUser",
  task: "EDIT",
  type: "account",
  color: "orange",
};

export default {
  customers: {
    title: "Customers",
    actionButtons: [exportCustomersButton, addCustomerButton],
  },
  services: {
    title: "Services",
    actionButtons: [addServicesButton],
  },
  locations: {
    title: "Location",
    actionButtons: [exportLocationsButton, addLocationButton],
  },
  appointments: {
    title: "Appointments",
    actionButtons: [exportAppointmentsButton, addAppointmentButton],
  },
  users: {
    title: "Users",
    actionButtons: [addUserButton],
  },
  reports: {
    title: "Reports",
    actionButtons: [],
  },
  default: {
    title: "Dashboard",
    actionButtons: [],
  },
  "user-dashboard": {
    title: "Dashboard",
    actionButtons: [editProfileButton, userAddAppointmentButton],
  },
};
