import React from 'react';
import { connect } from 'react-redux';
import './style.scss';
import '../utils/fontawesome';
import Helmet from './helmet';
import Navbar from './navbar';
import ProgressBar from './progressBar';
import { startup } from '../state/actions/startup';
import { Link } from 'gatsby';

const signupLinks = [
	{
		title:"Register",
		href: "/registration"
		,num:1
	},
	{
		title:"Select Clinic",
		href: "/book-now"
		,num:2
	},
	
	{
		title:"Select Date",
		href: "/date"
		,num:3
	},
	{
		title:"Company Details",
		href: "/forms/company"
		,num:4
	},
	{
		title:"Prebooking",
		href: "/forms/prebooking"
		,num:5
	},
	{
		title:"Employee Details",
		href: "/employees"
		,num:6
	},
	{
		title:"Submission of booking",
		href: "/confirmation"
		,num:7
	}

]
const CircleNumber = ({num}) => (<span className="circle-digit">{num}</span>);

const SignupLink = ({ title, href }) => (<div class="link-label"><Link to={href}>{title}</Link></div>)

class Layout extends React.Component {
	componentWillMount(){
		const { loadBookingInteraction, dispatch , state} = this.props;
		if(loadBookingInteraction === true){
			dispatch(startup(dispatch, state));
		}
	}
	render(){
		const {isHomePage, children, hideNav } = this.props
		return  (
			<div>
				<Helmet />
				<section className="bgBlur is-fullheight-with-navbar">
					{!hideNav && <Navbar />}
					
						{!isHomePage &&
							<div className="main-container">
								<div className="signupnav">
								{signupLinks.map( link => 
									<div className="signup-link">
										<CircleNumber num={link.num} />
										<SignupLink {...link} />
									</div>
								)}
								</div>
								<div className="children-container">
									{children}
								</div>
							</div>
						}
						{isHomePage &&
							<div className="home-page-container">
								{children}
							</div>
						}
				</section>
			</div>
		);
	}
}

const mapStateToProps = state => ({ state })
const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
