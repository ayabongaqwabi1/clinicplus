import React from "react";
import { toPairs, mergeDeepWith, add, reduce } from "ramda";
import "./style.scss";

/**
 * Capitalizes first letters of words in string.
 * @param {string} str String to be modified
 * @param {boolean=false} lower Whether all other letters should be lowercased
 * @return {string}
 * @usage
 *   capitalize('fix this string');     // -> 'Fix This String'
 *   capitalize('javaSCrIPT');          // -> 'JavaSCrIPT'
 *   capitalize('javaSCrIPT', true);    // -> 'Javascript'
 */
const _capitalize = (str, lower = false) =>
  (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, (match) =>
    match.toUpperCase()
  );
const capitalize = (str) =>
  _capitalize(str.replace(/([a-z0-9])([A-Z])/g, "$1 $2"));

class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { data } = this.props;
    const mergeDeepAll = reduce(mergeDeepWith(add), {});
    const totals = mergeDeepAll(data.map((i) => i.services));
    const totalPairs = toPairs(totals);
    return (
      <div className="service-data-container">
        <div className="totals">
          <h1>Totals</h1>
          <div className="ap-services">
            {totalPairs.map((sPair) => {
              return (
                <div className="serv">
                  <p>{capitalize(sPair[0])}</p>
                  <p>{sPair[1]}</p>
                </div>
              );
            })}
          </div>
        </div>
        <div className="info">
          {data.map((item) => {
            const appPairs = toPairs(item);
            const servicesPairs = toPairs(item.services);
            return (
              <div className="table-item">
                <table>
                  <tr>
                    {appPairs.map((appPair) => {
                      if (appPair[0] === "services") {
                        return "";
                      }
                      return <th>{appPair[0]}</th>;
                    })}
                  </tr>
                  <tr>
                    {appPairs.map((appPair) => {
                      if (typeof appPair[1] === "object") {
                        return "";
                      }
                      return <td>{appPair[1]}</td>;
                    })}
                  </tr>
                </table>
                <div className="ap-services">
                  {servicesPairs.map((sPair) => {
                    return (
                      <div className="serv">
                        <p>{sPair[0]}</p>
                        <p>{sPair[1]}</p>
                      </div>
                    );
                  })}
                </div>
                <br />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default IndexPage;
