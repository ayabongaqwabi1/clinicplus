import React from 'react';

import './style.scss';

import medic from '../images/medic.png';
import Navbar from './navbar';

const Header = ()=> (
	<section className="hero gradientBg is-fullheight-with-navbar">
		<Navbar />
		<div className="hero-body">
			<div className="container center">
				<article className="media center">
					<figure className="is-left">
						<span className="vector">
							<img src={medic} alt="gatsby-logo" />
						</span>
					</figure>
					<div className="media-content">
						<div className="content">
							<h1 className="is-uppercase is-size-1 brand-text-dark-red">
								Occupational Health 
							</h1>
							<p className="subtitle brand-text-color-grey is-size-4">
								ClinicPlus offers comprehensive Occupational Health Management and Consulting service to mines and industries. 
								Our goal is to help our clients manage their occupational health and safety risks.
							</p>
							<div className="buttons">
								<button className="brand-button-dark-red">Book Now</button>
								<button className="brand-button-dark-red-outline">My Bookings</button>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>
);

export default Header;
