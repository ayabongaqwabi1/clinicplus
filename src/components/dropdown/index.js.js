import React, { Component } from 'react';
import './style.scss';


class Drop extends Component {
    render(){
        const { data, onChange } = this.props
        return (<select onChange={onChange}>
            <option value="" disabled selected>{this.props.title}</option>
            <option value="">none</option>
            {data && data.map(item => {
                return (<option value={item.key}>{item.value}</option>)
            })}
        </select>)
    }
}

export default Drop;