import React from "react";
import { connect } from "react-redux";
import "./style.scss";
import { navigate } from "gatsby";
import reset from "../../state/actions/reset";
import Cookies from "js-cookie";

class DashHeader extends React.Component {
  constructor() {
    super();
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {}

  logout(e) {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(reset());
    Cookies.remove("clinicplus_user");
    navigate("/");
  }

  render() {
    return (
      <div className="dash-page-header">
        {this.props.state.auth.type !== "customer" && (
          <a onClick={this.props.refresh}>Refresh</a>
        )}
        <small>Logged in as </small>
        <p>{this.props.state.auth.name}</p>
        <div className="pop-info notification">
          <p>Email: {this.props.state.auth.email}</p>
          <p>User Type: {this.props.state.auth.type}</p>
        </div>
        <a onClick={this.logout}>Logout</a>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ state });
const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(DashHeader);
