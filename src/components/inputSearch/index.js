import React, { Component } from 'react';
import Icon from '../buttonIcon';
import './style.scss';


class InputSearch extends Component {
    render(){
        return (
            <div className="input-search-container">
                <Icon name="search" />
                <input onChange={(e) => this.props.onChange(e)} value={this.props.value} className="search-box" placeholder="Quick Search" />
            </div>
        )
    }s
}

export default InputSearch;